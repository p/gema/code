#!/usr/local/bin/gema -f

! C/C++ cross-reference utility
!
! Usage:
!	gema -f cxref.pat <name-options> <c-source-files>
! or on Unix, it could be just:
!	cxref <name-options> <c-source-files>
! This reads C or C++ source files and writes to standard output a report of
! which files and definitions use the names of interest.  A typical line
! of output looks like:
!
!	foo	bar.c	frob
!
! which means that the identifier `foo' is referenced within the definition
! of `frob' in file "bar.c".  Usually you will want to filter the
! output with `sort -u' or `uniq'.
!
! A common usage is:
!
!   etags -o TAGS *.c
!   cxref -names TAGS *.c | sort -u > refs.text
!
! This produces a file "refs.text" which is an alphabetical cross reference
! of the uses of each non-local name defined in the files in the current
! directory.  Any number of "-names" options may be used.
!
! Another way to use this is:
!
!   cxref -name foo *.c | uniq
!
! which produces a report of where the single name `foo' is referenced.
! This is rather like `grep', except that it shows the name of the
! referencing function instead of the source line, and it will not report
! appearances within comments or strings.
!
! The option 
!	-omit <identifier>
! may be used after "-names" or "-defs" to remove a particular name from
! the search list. 
!
! The option
!	-defs <file>
! is like -names except that each line of the file is assumed to begin with
! an identifier and the rest of the line is ignored.  Use this for a file
! produced by the companion script "cdefs".
!
! Formatting options:
!	-tabs n,n	Set tab stops at the designated columns (default 26,44)
!	-showpath	show file names as given instead of removing directory
!	-progress	display file names to standard error output
!
!----------------------------------------------------------
!  (The rest of this file is input for the "gema" program.)
!  (Documentation for "gema" is at: "http://gema.sourceforge.net".)

! write to standard output:
@ARGV{-out\n-\n}

! case sensitive:
@set-switch{i;0}
! literal characters:
@set-syntax{L;\-\.\(\)}
! discard unmatched input:
@set-switch{match;1}

! skip comments
\/\/*\n=
\/\*<u>\*\/=

! skip constants
\"<string>\"=
string:\\\n=;\\?=\\?;?=?
\'<char>\'=
char:\\?=\\?@end;?=?@end

! skip pre-processor directives
\#\L\W<J>\I<oneline>=
oneline:\\\n=
oneline:\n=@end

! variable "d" is the name of the current definition.
\B=@set{d;}

! top-level macro call that almost looks like a function definition:
<K3>\J<i>\W(<matchparen>)<space><I0>=@scan-refs{$1$2}

! function definition or declaration
<type-and-name><space><arglist><space><funbody>=\
	@set{d;$1}@scan-refs{$t $3}@scan-refs{$5}
funbody:<fstuff>\{<matchparen>\}=$0@end
funbody:\Cconst\W\;=@end
funbody:\;=@end
funbody:=@fail

! between ")" and "{" could be "const" for C++ or arg types for K&R C.
fstuff:\/\/*\n=\S
fstuff:\/\*<u>\*\/=\S
fstuff:\S=\S
fstuff:<type-and-name>\G\W<more-vars>\;=\I$t\I
fstuff:const\I=
fstuff:\:<space><I><space>(<matchparen>)=$0! base class constructor
fstuff:\,<space><I><space>(<matchparen>)=$0! base class constructor
fstuff:IN =;OUT =
fstuff:<I><fstuff2>=
fstuff:<Y0>=@fail
fstuff:\#else\J<oneline><id><space>(<matchparen>)\W\#endif<oneline>=

! for skipping function pointer arguments:
fstuff2:<I>=$1
fstuff2:\*=\*
fstuff2:(<matchparen>)=$0
fstuff2:\;=\;@end
fstuff2:\S=\S
fstuff2:=@fail

arglist:(<matchparen>)=$1@end
arglist:=@fail

more-vars:,<space><I><space>=
more-vars:,<space>\*<space><I><space>=
more-vars:\[<matchparen>\]<space>=
more-vars:=@end

! variable definition or declaration
<type-and-name><vstuff>=@set{d;$1}@scan-refs{$t $2}
vstuff:\;=@end
vstuff:\S<space>=
vstuff:\=<matchparen>\;=$1@end
vstuff:\[<matchparen>\]=
vstuff:=@fail

! macro definition
\#\L\Wdefine <I> <oneline>=@set{d;$1}@scan-refs{$2}
\#\L\Wdefine <I>(<matchparen>)<oneline>=@set{d;$1}@scan-refs{$3}

! class definition
class <class-name>\W<base-class><space>\{<matchparen>\}<matchparen>\;=\
	@set{d;$1}@scan-refs{$2 $4 $5}
class-name:<I> <I0>=! ignore storage specifier
class-name:<I>\W<optqual>=$1$2@end
class-name:=@fail
class-name:<I>=$1@end;=@fail
base-class:\:\W<class-name>\W=\I$1
base-class:\,\W<class-name>\W=\I$1
base-class:=@end
struct <class-name>\W<base-class><space>\{<matchparen>\}<matchparen>\;=@set{d;$1}@scan-refs{$2 $4 $5}
union <I><space>\{<matchparen>\}<matchparen>\;=@set{d;$1}@scan-refs{$3 $4}
enum <I><space>\{<matchparen>\}<matchparen>\;=@set{d;$1}@scan-refs{$4}

class <I>\;=
struct <I>\;=
union <I>\;=
enum <I>\;=

! type alias
typedef <ref> <I>\;=@report{$1\d$2}
typedef <J>\W<i>\W\{<matchparen>\}<space><I>=@set{d;$5}@scan-refs{$3}
typedef <matchparen>\;=

! return the function or variable name; save the type name in $t
type-and-name:\A=@set{t;}
type-and-name:<I> <space><I0>=@set{t;$1}
type-and-name:<I><space>\*=@set{t;$1\*}
type-and-name:<J><space>\{<matchparen>\}=
type-and-name:\*=
type-and-name:\&=
type-and-name:\S<space>=
type-and-name:\PPROTO\W(\W(=$t@set{t;}@end
type-and-name:<id>=$1@end
type-and-name:=@fail

matchparen:(#)=(#)
matchparen:\{#\}=\{#\}
matchparen:\[#\]=\[#\]
matchparen:\/\/*\n=\S
matchparen:\/\*<u>\*\/=\S
matchparen:\"<string>\"=$0
matchparen:\'<char>\'=$0
matchparen:\d<D>,<D>\n=@fail! for TAGS files
matchparen:\d<G>^A<D>,<D>\n=@fail! for TAGS files

space:\/\/*\n=
space:\/\*<u>\*\/=
space:\S=
space:=@end

! skip reserved words
\Icase\I=
\Ifor\I=
\Iif\I=
\Iswitch\I=
\;=
\,=

! report references
@set{t1;26}@set{t2;44}
ARGV:-tabs <D>,<D> =@set{t1;$1}@set{t2;$2}
showfile:=@file
ARGV:-showpath =@define{showfile\:\=\@inpath}
report:<u>\d<u>=$1 @tab{@var{t1}}@showfile{} @tab{@var{t2}}$2\n
<ref>=@report{$1\d$d}

ref:=@fail

scan-refs:\S=
scan-refs:\Ichar\I=
scan-refs:\Iint\I=
scan-refs:\Ivoid\I=
scan-refs:<ref>=@report{$1\d$d}
scan-refs:<I>=
scan-refs:\"<string>\"=
scan-refs:\'<char>\'=
scan-refs:\/\/*\n=
scan-refs:\/\*<u>\*\/=
scan-refs:\#<j>\I=
scan-refs:?=

! command-line options:
ARGV:-find <id>\n=@define{ref\:\\I@quote{$1}\\I\=\$0\@end}
ARGV:-name <id>\n=@define{ref\:\\I@quote{$1}\\I\=\$0\@end}
ARGV:-tags <G>\n=@tags{@read{$1}}
ARGV:-names TAGS\n=@tags{@read{TAGS}}
ARGV:-names <G>\n=@names{@read{$1}}
ARGV:-defs <G>\n=@defnames{@read{$1}}
ARGV:-omit <id>\n=@undefine{ref\:\\I@quote{$1}\\I}
ARGV:-progress\n=@define{\\B\=\@set\{d\;\}\@err\{\@showfile\{\}...\\n\}}

! ignore file names in TAGS file:
names:\f\n<F>.<F>,<D>\N=
! ignore numbers in TAGS file:
names:\d<D>,<D>\N=
names:^A<D>,<D>\N=

names:\/\/*\n=
names:\/\*<u>\*\/=
names:\"<string>\"=
names:\n=;\L\S=
names:<D>\I=
names:\L<id>=@define{ref\:\\I@quote{$1}\\I\=\$0\@end}
names:?=

tags:int\I=;char\I=;long\I=;unsigned\I=;void\I=;extern\I=;static\I=;const\I=
tags:class\I=;struct\I=;union\I=;enum\I=;typedef\I=;\#define =
tags:switch\I=
tags:if\I=;short\I=
tags::names

! ignore constructor to avoid unwanted ref to class name.
defnames:\N<I>\:\:$1\L *\n=

defnames:<I>\:\:<I3><i>\L *\n=\
	@define{ref\:\\I$1\\\:\\\:$2$3\\I\=\$0\@end}\
	@define{ref\:\\I$2$3\\I\=\$0\@end}
defnames:<id>\L <G> <F>\.<I>\W\n=\
	@define{ref\:\\I@quote{$1}\\I\=\$0\t$3.$4\t\@end}
defnames:<id>\L *\n=@define{ref\:\\I@quote{$1}\\I\=\$0\@end}
defnames:<I><G>*\n=
defnames::names

id:operator <I><stars>=$0@end
id:operator\I\W<opchars>=operator $1@end
id:<I><optqual>=$1$2@end
id:\~<I><optqual>=$0@end
id:for\I=@fail
id:return\I=@fail
id:while\I=@fail
id:do\I=@fail
id:=@fail

stars:\W\*\W=\*
stars:=@end

optqual:\:\:\W<id>=$0
optqual:\W\<\L<match-angle>\>=$0
optqual:=@end

opchars:\==\=;\-=\-;\+=\+;\<=\<;\>=\>;\/=\/;\!=\!;\~=\~
opchars:\%=\%;\^=\^;\&=\&;\|=\|;\*=\*
opchars:\[\W\]=$0
opchars:=@terminate

match-angle:\<#\>=$0
match-angle::matchparen

extern\W"C"\W\{=
extern =
static =
__declspec\W(<matchparen>)=

! skip whitespace
\S=
! skip other identifiers
<I>=


