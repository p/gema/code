#!/usr/local/bin/gema -f

! Filter a Java source file for HTML display with syntax coloring.
!----------------------------------------------------------
!  (The rest of this file is input for the "gema" program.)
!  (Documentation for "gema" is at: "http://gema.sourceforge.net/".)

! write to standard output:
@ARGV{-out\n-\n}

! case sensitive:
@set-switch{i;0}
! literal characters:
@set-syntax{L;\-\.\(\)}

@set{c;\@}

ARGV:-title *\n=@set{title;$1}@set{heading;$1}

\B\P<emacsparams>=\
	\<html\>\<head\>\n\<title\>@var{title;@file}\<\/title\>\n\
	\<\/head\>\<body\>\n\
	\<h1\>@var{heading;@inpath}\<\/h1\>\n\<pre\>\n
\E=\N\<\/pre\>\n\<\/body\>\<\/html\>\n

@set{t;@left{8;}}
emacsparams:\L*-\*-*tab-width\:\W<D>*-\*-=@set{t;@left{$3;}}@end
emacsparams:=@end
\N\t<moretab>=$t$1
moretab:\t=$t;=@end

color:*,<U>=\<font color\=$1\>$2\<\/font\>
color:<I>,\Z=@end
color:=

tohtml:\<=\&lt\;
tohtml:\>=\&gt\;
tohtml:\&=\&amp\;
tohtml:\N\t<moretab>=$t$1

! comments
\/\/\L<comment>\P\n=@color{green,\/\/$1}
\/\*<comment>\*\/=@color{green,$0}
comment:\Ihttp\:\/\/<urlhost>=\<a href\="$0"\>$0\<\/a\>
comment:\Iftp\:\/\/<urlhost>=\<a href\="$0"\>$0\<\/a\>
comment:"<j2>tp\:\/\/<P>"="\<a href\=$0\>$1tp\:\/\/$2\<\/a\>"
comment:\<=\&lt\;
comment:\>=\&gt\;
comment:\&=\&amp\;
comment:\N\t<moretab>=$t$1

numprefix:\L<S>=$1
numprefix:\#=$0
numprefix:\Cnum<w>=$0
numprefix:=@end

urlhost:\P. =@terminate;<J>=$1;\/=\/;\:=\:;\#=\#;\.<F0>=\.;<F1>=$1;=@terminate
urlhost:\?=\?;\&=\&;\%=\%
urlhost:\P\, =@terminate;\P\; =@terminate

! constants
\"<string>\"=@color{blue,$0}
string:\\\n=$0
string:\\?=\\@tohtml{?}
string:\<=\&lt\;
string:\>=\&gt\;
string:\&=\&amp\;
\'<char>\'=@color{blue,$0}
char:\\?=\\@tohtml{?}@end;?=@tohtml{?}@end

! skip whitespace
\s\L<s>=$0
<S1>=$1

deftag:\W<I>\W,*=\<a name\="$1"\>$2\<\/a\>
deftag:*,*=$2

package<reqspace><id><space>\;=@color{magenta,package}$1@color{red,$2}$3\;
import<reqspace><id><space>\;=$0

! function
! don't put name anchor on static declaration
static<reqspace><type><space><I><space><arglist><space>\;=\
	static$1$2$3@color{red,$4}$5$6$7\;
! constructor
public\s<I><space><arglist><fstuff><funbody>=@deftag{$1,public}\s@color{red,$1}$2$3$4$5
private\s<I><space><arglist><fstuff><funbody>=@deftag{$1,private}\s@color{red,$1}$2$3$4$5
! general case function:
<typed-name><space><arglist><fstuff><funbody>=$0
!! function with implicit return type or constructor method
!\n<I><iopt><space><arglist><fstuff><funbody>=\
!	\n@color{red,@deftag{$1,$1$2}}$3$4$5$6
!iopt:<space>\.<space><id>=$0@end
!iopt:=@end

!type:const<S>=$0
!type:unsigned<S>int\I<s><typemod>=$0@end
type:<I><s><typemod>=$0@end
type:=@fail

funbody:\{<skip-body>\}=$0@end
funbody:\;=\;@end
funbody:=@fail
skip-body:\}=\}@end;\)=$0@end;\]=$0@end
skip-body:\n\}=$0@end
skip-body:(#)=$0
skip-body:\{#\}=$0
skip-body:\[#\]=$0
skip-body:\/\/<comment>\n=@color{green,\/\/$1}\n
skip-body:\/\*<comment>\*\/=@color{green,$0}
skip-body:\"<string>\"=@color{blue,$0}
skip-body:\'<char>\'=@color{blue,$0}
skip-body::tohtml

! between ")" and "{":
fstuff:throws\I<space><id-list>=$0
fstuff:\/\/<comment>\n=@color{green,\/\/$1}\n
fstuff:\/\*<comment>\*\/=@color{green,$0}
fstuff:<S>=$1
fstuff:const\I=const
fstuff:\:<space><I><space>(<matchparen>)=$0! base class constructor
fstuff:\,<space><I><space>(<matchparen>)=$0! base class constructor
fstuff:<Y0>=@end
!fstuff:typedef =@fail
!fstuff:struct\I\W<i>\W\{=@fail
fstuff:<I>=$1

arglist:(<matchparen>)=$0@end
arglist:=@fail

more-vars:,<space><I><space>=$0
more-vars:\[<matchparen>\]<space>=$0
more-vars:=@end

id-list:<id>=$1
id-list:\,=\,
id-list:<reqspace>=$1
id-list:=@end

! variable definition or declaration
<typed-name><vstuff>=$0
const<S><typed-name><space>\=<matchparen>\;=$0
<I>\;=$0! degenerate case is not a variable
vstuff:\;=\;@end
vstuff:<reqspace>=$1
vstuff:\=<matchparen>\;=$0@end
vstuff:\[<matchparen>\]=$0
vstuff:=@fail

! class definition
class<S><class-name><s><base-part>\{#\}=\
	@color{magenta,@deftag{$2,class}}$1@color{red,$2}$3$4\{$5\}@pop{c}
class<S><class-name><s>\;=$0@pop{c}! ignore forward declarations
class-name:<I><s><optqual>=$0@push{c;$1$2}@end
class-name:=@fail
base-part:extends<reqspace><id><space>=$0
base-part:implements<reqspace><id-list>=$0
base-part:=@end

typed-name:case\I=@fail
typed-name:throws\I=@fail
typed-name:<J><S><I><space>\{<matchparen>\}=@deftag{$3,$1}$2@color{red,$3}$4\{$5\}
typed-name:<I><optqual><space><typemod><id>=@deftag{$5,$1}$2$3$4@color{red,$5}@end
!typed-name:<I><space>\*=$0
!typed-name:<I><space>\&=$1$2\&amp\;
typed-name:<I><space><typemod>=$0
!typed-name:<J>\I<space>\{<matchparen>\}=$0
!typed-name:\*=$0
!typed-name:\&=\&amp\;
!typed-name:\[<space>\]=$0
typed-name:<S><space>=$0
typed-name:return\I=@fail;this\I=@fail
typed-name:<id>=$1@end
typed-name:<reqspace>=$1
typed-name:=@fail

typemod:\[<space>\]<space>=$0
typemod:=@end

id:<I><optqual>=$1$2@end
id:if\I=@fail
id:class\I=@fail
id:public\I=@fail
id:private\I=@fail
id:final\I=@fail
id:=@fail

optqual:\.<s><I>=$0
!optqual:(<L1><I><s>)\G<s>\.<s><id>=$0
optqual:=@end

matchparen:(#)=(#)
matchparen:\{#\}=\{#\}
matchparen:\[#\]=\[#\]
matchparen:\)=@fail;\}=@fail;\]=@fail
matchparen:\/\/<comment>\n=@color{green,\/\/$1}\n
matchparen:\/\*<comment>\*\/=@color{green,$0}
matchparen:\"<string>\"=@color{blue,$0}
matchparen:\'<char>\'=@color{blue,$0}
matchparen::tohtml

! optional white space
space:\/\/<comment>\n=@color{green,\/\/$1}\n
space:\/\*<comment>\*\/=@color{green,$0}
space:<S>=$1
space:=@end

! required white space
reqspace:\/\/<comment>\n=@color{green,\/\/$1}\n
reqspace:\/\*<comment>\*\/=@color{green,$0}
reqspace:<S>=$1
reqspace:=@terminate

! skip reserved words
\Icase\I<matchparen>\;=$0
\Ibreak\;=$0
\Idefault<s>\:=$0
\Iif<s>(<matchparen>)<skip-statement>=$0
\Ifor<s>(<matchparen>)<skip-statement>=$0
\Iswitch<s>(<matchparen>)<skip-statement>=$0
\Ielse\I<skip-statement>=$0
\;=$0
\,=$0

skip-statement:\{<skip-body>=$0@end
skip-statement:\P\}=@end
skip-statement:<reqspace>=$1
skip-statement:<matchparen>\;=$0@end
skip-statement:=@end

static<S>=$0
public<S>=$0
private<S>=$0
final<S>=$0
abstract<S>=$0
native<S>=$0

! skip stray bodies whose headers weren't matched
\{<matchparen>\}=$0
\(<matchparen>\)=$0


! skip other identifiers
<I>=$1

\<=\&lt\;
\>=\&gt\;
\&=\&amp\;

! page break
^L\L\W=\<\/pre\>\<hr\>\<pre\>

! documentation comments for Java Doc
\/\/\/\L<doccomment>\P\n=@color{green,\/\/\/$1}
\/\*\*<S><doccomment>\*\/=@color{green,$0}
reqspace:\/\/\/\L<doccomment>\P\n=@color{green,\/\/\/$1}
reqspace:\/\*\*<S><doccomment>\*\/=@color{green,$0}
doccomment:\A\/=@fail
doccomment:\<<L><d1><tagparam>\>=@color{\#00CCCC,\&lt\;$1$2$3\&gt\;}
doccomment:\<\/<L><d1>\>=@color{\#00CCCC,\&lt\;\/$1$2\&gt\;}
doccomment:\&<j>\;=@color{\#00CCCC,\&amp\;$1\;}
doccomment:\<T\>=\&lt\;T\&gt\;
doccomment:\@<J><-I0>=@color{\#00CCCC,\@$1}
doccomment::comment

tagparam:\shref\=\"http\:\/\/<urlhost>\"=\shref\=\"\<a href\="http\:\/\/$1"\>http\:\/\/$1\<\/a\>\"
tagparam:<S><L>\=\"\L<P>\"=$0
tagparam:<S><L>\=<D>=$0
tagparam:\/=\/
tagparam:=@end

! Highlighting option

@set{pre;\<a name\="found1st"\>}
@set{post;\<\/a\>}
! strings beginning and ending the highlighted region.
! background color works only on MS Internet Explorer.
@set{a;\<font color\=darkorange\>\<b\>}@set{b;\<\/b\>\<\/font\>}
browser:MSIE=@set{a;\<span style\="background\=yellow"\>}@set{b;\<\/span\>}@end
browser:<L>=;?=

! This is the rule that highlights references.
ARGV:-hi\J<i> <G>\n=@set{key;$2}@browser{@getenv{HTTP_USER_AGENT}}\
    @define{tohtml\:\\I@quote{@tohtml{@var{key}}}\\I\=\
      \$a\@var\{pre\}\$0\@var\{post\}\$b\
      \@set\{pre\;\}\@set\{post\;\}}

