#!/usr/local/bin/gema -f

! Filter a C or C++ source file for HTML display with syntax coloring.
!----------------------------------------------------------
!  (The rest of this file is input for the "gema" program.)
!  (Documentation for "gema" is at: "http://gema.sourceforge.net/".)

! case sensitive:
@set-switch{i;0}
! literal characters:
@set-syntax{L;\-\.\(\)}

ARGV:-title *\n=@set{title;$1}@set{heading;$1}

\B\P<emacsparams>=\
	\<html\>\<head\>\n\<title\>@var{title;@file}\<\/title\>\n\
	\<\/head\>\<body\>\n\
	\<h1\>@var{heading;@inpath}\<\/h1\>\n\<pre\>\n
\E=\N\<\/pre\>\n\<\/body\>\<\/html\>\n

@set{t;@left{8;}}
emacsparams:\L*-\*-*tab-width\:\W<D>*-\*-=@set{t;@left{$3;}}@end
emacsparams:=@end
\N\t<moretab>=$t$1
moretab:\t=$t;=@end

color:*,<U>=\<font color\=$1\>$2\<\/font\>
color:<I>,\Z=@end
color:=

tohtml:\<=\&lt\;
tohtml:\>=\&gt\;
tohtml:\&=\&amp\;
tohtml:\N\t<moretab>=$t$1

! comments
\/\/\L<comment>\P\n=@color{green,\/\/$1}
\/\*<comment>\*\/=@color{green,$0}

comment:\Ihttp\:\/\/<urlhost>=\<a href\="$0"\>$0\<\/a\>
comment:\IHTTP\:\/\/<urlhost>=\<a href\="$0"\>$0\<\/a\>
comment:\Iftp\:\/\/<urlhost>=\<a href\="$0"\>$0\<\/a\>
comment:"<j2>tp\:\/\/<P>"="\<a href\=$0\>$1tp\:\/\/$2\<\/a\>"
comment:\<=\&lt\;
comment:\>=\&gt\;
comment:\&=\&amp\;
comment:\N\t<moretab>=$t$1

numprefix:\L<S>=$1
numprefix:\#=$0
numprefix:\Cnum<w>=$0
numprefix:=@end

urlhost:\P. =@terminate;<J>=$1;\/=\/;\:=\:;\#=\#;\.<F0>=\.;<F1>=$1;=@terminate
urlhost:\?=\?;\&=\&;\%=\%
urlhost:\P\, =@terminate;\P\; =@terminate

! constants
\"<string>\"=@color{blue,$0}
string:\\\n=$0
string:\\?=\\@tohtml{?}
string:\<=\&lt\;
string:\>=\&gt\;
string:\&=\&amp\;
\'<char>\'=@color{blue,$0}
char:\\?=\\@tohtml{?}@end;?=@tohtml{?}@end

! pre-processor directives
\#include\L<S>\<<U>\><oneline>=@color{purple,\#include}$1@color{blue,\&lt\;$2\&gt\;}$3
\#\L<s><J>\I<oneline>=@color{purple,$0}
oneline:\\\n\L<s>=$0
oneline:\n=\n@end
oneline:\/\*\L<comment>\*\/=@color{green,$0}
oneline:\"\L<string>\"=@color{blue,$0}
oneline:\/\/<comment>\n=@color{green,\/\/$1}\n@end
oneline:(\L#)=$0
oneline::tohtml

! skip whitespace
\s\L<s>=$0
<S1>=$1

deftag:<I>,<I>\:\:\~<I>=\<a name\="@encodemethod{\~$3}.$2"\>$2\:\:\~$3\<\/a\>
deftag:<I>,<I>\:\:\!<I>=\<a name\="@encodemethod{\!$3}.$2"\>$2\:\:\!$3\<\/a\>
deftag:\W<I>\W,*=\<a name\="$1"\>$2\<\/a\>
deftag:<encodeclass>\:\:\W<encodemethod>\W,*=\<a name\="$2\.$1"\>$3\<\/a\>
deftag:<encodemethod>\W,*=\<a name\="$1"\>$2\<\/a\>
deftag:*,*=$2

! including the following file to define <encodeclass> and <encodemethod>
@define{@read{@mergepath{@inpath;encodemeth.pat;}}}

! function
! don't put name anchor on static declaration
static<reqspace><type><space><I><space><arglist><space>\;=\
	static$1$2$3@color{red,$4}$5$6$7\;
! function with implicit return type or constructor method
\n<I><iopt><space><arglist><space><funbody>=\
	\n@color{red,@deftag{$1$2,$1$2}}$3$4$5$6
! indented constructor
<I>\:\:$1<space><arglist><space><funbody>=@color{red,@deftag{$1\:\:$1,$1\:\:$1}}$2$3$4$5
! general case function:
<typed-name><space><arglist><space><funbody>=$0

iopt:<space>\:\:<space><id>=$0@end
iopt:=@end

type:const<S>=$0
type:unsigned<S>int\I<s><stars>=$0@end
type:<I><s><stars>=$0@end
type:=@fail

funbody:<fstuff>\{<skip-body>=$0@end
funbody:\;=\;@end
funbody:__THROW\I<space>=$0
funbody:__attribute__<s>(<matchparen>)<space>=$0
funbody:__attribute_malloc__\I<space>=$0
funbody:throw<space>(<matchparen>)<space>=$0
funbody:noexcept\I<space>=$0
funbody:=@fail

skip-body:\}=$0@end;\)=$0@end;\]=$0@end
skip-body:\n\}=$0@end
skip-body:(#)=$0
skip-body:\{#\}=$0
skip-body:\[#\]=$0
skip-body:\/\/<comment>\n=@color{green,\/\/$1}\n
skip-body:\/\*<comment>\*\/=@color{green,$0}
skip-body:\"<string>\"=@color{blue,$0}
skip-body:\'<char>\'=@color{blue,$0}
skip-body:\#\L<s><I><oneline>=@color{purple,$0}
skip-body::tohtml

! between ")" and "{" could be "const" for C++ or arg types for K&R C.
fstuff:\/\/<comment>\n=@color{green,\/\/$1}\n
fstuff:\/\*<comment>\*\/=@color{green,$0}
fstuff:<S>=$1
fstuff:<I><s><stars><I><s><more-vars>\;=$0! try to avoid coloring K&R args
fstuff:<typed-name>\G<s><more-vars>\;=$0
fstuff:const\I=const
fstuff:\:<space><I><space>(<matchparen>)=$0! base class constructor
fstuff:\,<space><I><space>(<matchparen>)=$0! base class constructor
fstuff:<Y0>=@end
fstuff:typedef =@fail
fstuff:struct\I\W<i>\W\{=@fail
fstuff:<I>=$1

arglist:(<matchparen>)=$0@end
arglist:<I><s>(<s>(<matchparen>)\G<s>)=$0@end! e.g. `PROTO((...))'
arglist:=@fail

more-vars:,<space><I><space>=$0
more-vars:\[<matchparen>\]<space>=$0
more-vars:=@end

! variable definition or declaration
<typed-name><vstuff>=$0
const<S><typed-name><space>\=<matchparen>\;=$0
extern\ <s><I><s>\;=@deftag{$2,extern}\ $1@color{red,$2}$3\;
<I>\;=$0! degenerate case is not a variable
vstuff:\;=\;@end
vstuff:<reqspace>=$1
vstuff:\=<matchparen>\;=$0@end
vstuff:\[<matchparen>\]=$0
vstuff:=@fail

! macro definition
\#\L<s>define<S><I><s>\n=@color{purple,@deftag{$3,\#$1define}}$2@color{red,$3}$4\n
\#\L<s>define<S><I><s><oneline>=@color{purple,@deftag{$3,\#$1define}}$2@color{red,$3}$4$5
\#\L<s>define<S><I>(<matchparen>)<S><oneline>=@color{purple,@deftag{$3,\#$1define}}$2@color{red,$3}($4)$5$6

! class definition
class<S><stspec><class-name><space><base-part>\{<matchparen>\}<vars>=\
	@deftag{$3,class}$1$2@color{red,$3}$4$5\{$6\}$7
class<S><stspec><class-name><s>\;=$0! ignore forward declarations
template<space>\<<match-angle>\>\G<space>class<S><class-name><space><base-part>\{<matchparen>\}=\
	@deftag{$5,template}$1\&lt\;$2\&gt\;$3class$4@color{red,$5}$6$7\{$8\}

class-name:<I><s><optqual>=$0@end
class-name:=@fail
stspec:<I><S><I0>=@color{gray,$1}$2@end
stspec:__declspec<s>(<matchparen>)=@color{gray,$0}
stspec:=@end
base-part:\:<s><match-angle>=$0
base-part:=@end
vars:\;=\;@end
vars:<I>=@color{red,@deftag{$1,$1}}
vars:\,=\,;\*=\*;\&=\&amp\;;\[<matchparen>\]=$0;<reqspace>=$0
vars:=@end

! structure definition
struct<S><i><space>\{<matchparen>\}<vars>=@deftag{$2,struct}$1@color{red,$2}$3\{$4\}$5
struct<S><class-name><space><base-part>\{<matchparen>\}<vars>=\
	@deftag{$2,struct}$1@color{red,$2}$3$4\{$5\}$6
struct<S><stspec><class-name><space><base-part>\{<matchparen>\}<vars>=\
	@deftag{$3,struct}$1$2@color{red,$3}$4$5\{$6\}$7
union<S><i><space>\{<matchparen>\}<vars>=@deftag{$2,union}$1@color{red,$2}$3\{$4\}$5
struct<S><I><s>\;=$0! ignore forward declarations

enum<S><i><space>\{<enumbody>\}<vars>=@deftag{$2,enum}$1@color{red,$2}$3\{$4\}$5
enum<S><I><s>\;=$0! ignore forward declarations

! templates
template<s>\<<match-angle>\>\G<s>class<S><id><s>\;=\
	@deftag{$5,template}$1\&lt\;$2\&gt\;$3class$4@color{red,$5}$6\;
template<space>\<<match-angle>\>=template$1\&lt\;$2\&gt\;

! type alias
typedef<S>struct<s>\{<matchparen>\}<s><I><s>\;=\
	@deftag{$5,typedef}$1struct$2\{$3\}$4@color{red,$5}$6\;
typedef<S>struct<S><stspec><I><s>\{<matchparen>\}<s><i><s>\;=\
	@deftag{$4,typedef}$1struct$2$3@color{red,$4}$5\{$6\}$7@color{red,$8}$9\;
typedef<S>struct<S><I><S><I><s>\;=\
	@deftag{$5,typedef}$1struct$2$3$4@color{red,$5}$6\;
typedef<S>enum<s>\{<enumbody>\}<s><I><s>\;=\
	@deftag{$5,typedef}$1enum$2\{$3\}$4@color{red,$5}$6\;
typedef<S><typed-name><space><typemod>\;=$0
typedef<S><I><s><stars>(<s><stars><I><s>)<s><i><s>(<matchparen>)<space>\;=\
	@deftag{$7,typedef}$1$2$3$4($5$6@color{red,$7}$8)$9${10}${11}(${12})${13}\;

typemod:(<matchparen>)<space>=$0@end
typemod:\[<matchparen>\]<space>=$0@end
typemod:,<space><vars>=$0@end
typemod:=@end
typedef<S><matchparen>\;=$0! unrecognized!

typed-name:enum<S><i><space>\{<enumbody>\}=\
	@deftag{$2,enum}$1@color{red,$2}$3\{$4\}
typed-name:struct\I<s><i><space>\{<matchparen>\}=\
	@deftag{$2,struct}$1@color{red,$2}$3\{$4\}
typed-name:goto\I=@fail
typed-name:case\I=@fail
typed-name:typedef\I=@fail
typed-name:using\I=@fail
typed-name:namespace\I=@fail
typed-name:<I><S>\P\C<i>PROTO<i><s>\(<s>\(=$0@end
typed-name:<J><S><I><space>\{<matchparen>\}=@deftag{$3,$1}$2@color{red,$3}$4\{$5\}
typed-name:<I><optqual><space><stars><id>=@deftag{$5,$1}$2$3$4@color{red,$5}@end
typed-name:<I><space>\*=$0
typed-name:<I><space>\&=$1$2\&amp\;
typed-name:<J>\I<space>\{<matchparen>\}=$0
typed-name:\*=$0
typed-name:\&=\&amp\;
typed-name:<S><space>=$0
typed-name:return\I=@fail;this\I=@fail
typed-name:<id>=$1@end
typed-name:<reqspace>=$1
typed-name:=@fail

stars:\*<space>=\*$1
stars:\&<space>=\&amp\;$1
stars:=@end

star:<s>\*=$0
star:=@end

id:operator<S><I><star>=$0@end
id:operator\I<s><opchars>=operator$1$2@end
id:<I><optqual>=$1$2@end
id:\~<I><optqual>=$0@end
id:\!<I><optqual>=$0@end
id:if\I=@fail
id:class\I=@fail
id:=@fail

optqual:\:\:<s><id>=$0
optqual:(<L1><I><s>)\G<s>\:\:<s><id>=$0
optqual:<s>\<\L<match-angle>\>=$1\&lt\;$2\&gt\;
optqual:=@end

opchars:\==\=;\-=\-;\+=\+;\<=\&lt\;;\>=\&gt\;;\/=\/;\!=\!;\~=\~
opchars:\%=\%;\^=\^;\&=\&amp\;;\|=\|;\*=\*
opchars:\[<s>\]=$0
opchars:=@terminate

match-angle:\<#\>=\&lt\;$1\&gt\;
match-angle::matchparen

enumbody:<N>=$1
enumbody:<I>=@color{red,@deftag{$1,$1}}
enumbody:,=,;\=<s><I>=$0
enumbody:<reqspace>=$1
enumbody:\#\L<s><I><oneline>=$0
enumbody:?=?

matchparen:(#)=(#)
matchparen:\{#\}=\{#\}
matchparen:\[#\]=\[#\]
matchparen:\)=@fail;\}=@fail;\]=@fail
matchparen:\/\/<comment>\n=@color{green,\/\/$1}\n
matchparen:\/\*<comment>\*\/=@color{green,$0}
matchparen:\"<string>\"=@color{blue,$0}
matchparen:\'<char>\'=@color{blue,$0}
matchparen:\#\L<s><I><oneline>=@color{purple,$0}
matchparen::tohtml

! optional white space
space:\/\/<comment>\n=@color{green,\/\/$1}\n
space:\/\*<comment>\*\/=@color{green,$0}
space:<S>=$1
space:\#\L<s>if<j>\I<oneline>=@color{purple,$0}
space:\#\L<s>e<j>\I<oneline>=@color{purple,$0}
space:=@end

! required white space
reqspace:\/\/<comment>\n=@color{green,\/\/$1}\n
reqspace:\/\*<comment>\*\/=@color{green,$0}
reqspace:<S>=$1
reqspace:\#\L<s><J>\I<oneline>=@color{purple,$0}
reqspace:=@terminate

! skip reserved words
\Icase\I<matchparen>\;=$0
\Ibreak\;=$0
\Idefault<s>\:=$0
\Iif<s>(<matchparen>)<skip-statement>=$0
\Ifor<s>(<matchparen>)<skip-statement>=$0
\Iswitch<s>(<matchparen>)<skip-statement>=$0
\Ielse\I<skip-statement>=$0
\;=$0
\,=$0

namespace<reqspace><I><space>\{=@deftag{$2,namespace}$1@color{red,$2}$3\{

skip-statement:\{<skip-body>=$0@end
skip-statement:\P\}=@end
skip-statement:<reqspace>=$1
skip-statement:<matchparen>\;=$0@end
skip-statement:=@end

extern<s>\C"C"<s>\{=$0
extern<S>=$0
static<S>=$0
inline<S>=$0
__declspec<s>(<matchparen>)=@color{gray,$0}

! skip stray bodies whose headers weren't matched
\{<matchparen>\}=$0
\(<matchparen>\)=$0

! other things to ignore:
using<S>namespace<S><id><s>\;=$0
using<S><id><s>\;=$0

! skip other identifiers
<I>=$1

\<=\&lt\;
\>=\&gt\;
\&=\&amp\;

! page break
^L\L\W=\<\/pre\>\<hr\>\<pre\>

! additions for C++/CLI
ref<S>class<S><I><class-mod><base-part>\{<matchparen>\}<vars>=\
	@deftag{$3,ref$1class}$2@color{red,$3}$4$5\{$6\}$7
class-mod:sealed\I=sealed
class-mod:abstract\I=abstract
class-mod:<reqspace>=$1
class-mod:=@end
vars:\^=\^;\%=\%
!typed-name:<I><space>\^=$0
typed-name:\^=$0
stars:\^<space>=$0
stars:\%<space>=$0

! Highlighting option

@set{pre;\<a name\="found1st"\>}
@set{post;\<\/a\>}
! strings beginning and ending the highlighted region.
@set{a;\<span style\=\'background\:yellow\'\>}@set{b;\<\/span\>}

! This is the rule that highlights references.
ARGV:-hi\J<i> <G>\n=@set{key;$2}\
    @define{tohtml\:\\I@quote{@tohtml{@var{key}}}\\I\=\
      \$a\@var\{pre\}\$0\@var\{post\}\$b\
      \@set\{pre\;\}\@set\{post\;\}}

! documentation comments for Visual Studio or Doxygen
\/\/\/\L<doccomment>\P\n=@color{green,\/\/\/$1}
\/\*\*<S><doccomment>\*\/=@color{green,$0}
matchparen:\/\/\/\L<doccomment>\P\n=@color{green,\/\/\/$1}
matchparen:\/\*\*<S><doccomment>\*\/=@color{green,$0}
reqspace:\/\/\/\L<doccomment>\P\n=@color{green,\/\/\/$1}
reqspace:\/\*\*<S><doccomment>\*\/=@color{green,$0}
doccomment:\A\/=@fail
doccomment:\<<L><d1><tagparam>\>=@color{\#00CCCC,\&lt\;$1$2$3\&gt\;}
doccomment:\<\/<L><d1>\>=@color{\#00CCCC,\&lt\;\/$1$2\&gt\;}
doccomment:\&<j>\;=@color{\#00CCCC,\&amp\;$1\;}
doccomment:\<T\>=\&lt\;T\&gt\;
doccomment:\@<J><-I0>=@color{\#00CCCC,\@$1}
doccomment:\\<J><-I0>=@color{\#00CCCC,\\$1}
doccomment::comment

tagparam:\shref\=\"http\:\/\/<urlhost>\"=\shref\=\"\<a href\="http\:\/\/$1"\>http\:\/\/$1\<\/a\>\"
tagparam:<S><L>\=\"\L<P>\"=$0
tagparam:\/=\/
tagparam:=@end

