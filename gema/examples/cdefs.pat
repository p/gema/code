#!/usr/local/bin/gema -f

! List the definitions in C or C++ source files
!
! Typical usage:
!       gema -f cdefs.pat *.h *.c
! or on Unix, it could be just:
!	cdefs *.h *.c
!
! This reads C or C++ source files and writes to standard output a report of
! which names are defined in each file.  A typical line of output looks like:
!
!	foo	function  bar.c
!
! which means that the identifier `foo' is defined as a function in file
! "bar.c".  Definitions are listed in source file order; you might want
! to pipe the output through "sort" to get an alphabetical listing.
!
! Options:
!	-showpath	show file names as given instead of removing directory
!	-progress	display file names to standard error output
!
!----------------------------------------------------------
!  (The rest of this file is input for the "gema" program.)
!  (Documentation for "gema" is at: "http://gema.sourceforge.net".)

! write to standard output:
@ARGV{-out\n-\n}

! case sensitive:
@set-switch{i;0}
! literal characters:
@set-syntax{L;\-\.\(\)}
! discard unmatched input:
@set-switch{match;1}

! command line options:
ARGV:-progress\n=@define{\\B\=\@err\{\@showfile\{\}...\\n\}}
ARGV:-showpath =@define{showfile\:\=\@inpath}

! skip comments
\/\/*\n=
\/\*<u>\*\/=

! skip constants
\"<string>\"=
string:\\\n=;\\?=\\?;?=?
\'<char>\'=
char:\\?=\\?@end;?=?@end

! skip pre-processor directives
\#\L\W<J>\I<oneline>=
oneline:\\\n\W=\S
oneline:\n=@end
oneline:(\L#)=(#)

report:\A<P>,\G\W\Z=
report:*,\W*=\N$2 @tab{45}$1 @tab{55}@showfile{}\n
showfile:=@file

! function
<I> <I> <I><space>(\W(<matchparen>)\W)<space>\;=@report{fundecl,$2}
<typed-name><space><arglist><space><funbody>=@report{$5,$1}
funbody:<fstuff>\{<skip-body>=function@end
funbody:\;=fundecl@end
funbody:__THROW\I<space>=
funbody:__attribute__<s>(<matchparen>)<space>=
funbody:__attribute_malloc__\I<space>=
funbody:throw<space>(<matchparen>)<space>=
funbody:noexcept\I<space>=
funbody:=function@end
skip-body:\}=@end;\)=@end;\]=@end
skip-body:\n\}=@end
skip-body:(#)=
skip-body:\{#\}=
skip-body:\[#\]=
skip-body:\/\/*\n=
skip-body:\/\*<u>\*\/=
skip-body:\"<string>\"=
skip-body:\'<char>\'=
skip-body:\#\L\W<I><oneline>=

! between ")" and "{" could be "const" for C++ or arg types for K&R C.
fstuff:\/\/*\n=
fstuff:\/\*<u>\*\/=
fstuff:\S=
fstuff:<typed-name>\G\W<more-vars>\;=
fstuff:const\I=
fstuff:\:<space><I><space>(<matchparen>)=! base class constructor
fstuff:\,<space><I><space>(<matchparen>)=! base class constructor
fstuff:<Y0>=@end
fstuff:typedef =@fail
fstuff:struct\I\W<i>\W\{=@fail
fstuff:<I>=$1

arglist:(<matchparen>)=$1@end
arglist:=@fail

more-vars:,<space><I><space>=
more-vars:\[<matchparen>\]<space>=
more-vars:=@end

! variable definition or declaration
<typed-name><vstuff>=@report{variable,$1}
const <typed-name><space>\=<matchparen>\;=@report{constant,$1}
extern\ \S<I>\W\;=@report{variable,$1}
<I>\;=! degenerate case is not a variable
vstuff:\;=@end
vstuff:<reqspace>=
vstuff:\=<matchparen>\;=@end
vstuff:\[<matchparen>\]=
vstuff:=@fail

! macro definition
\#\L\Wdefine <I>\W\n=@report{flag,$1}
\#\L\Wdefine <I> <oneline>=@report{define,$1}
\#\L\Wdefine <I>(<matchparen>)<oneline>=@report{macro,$1}

! class definition
class <class-name><space><base-part>\{<matchparen>\}<vars>=@report{class,$1}$5
class <class-name>\W\;=! ignore forward declarations
class-name:<I> final\I=$1@end! C++11 keyword "final" is not the class name.
class-name:<I> <I0>=! ignore storage specifier
class-name:<I>\W(\W<match-angle>)=$0@end! for macros such as "ooRef(ooObj)"
class-name:<I>\W<optqual>=$1$2@end
class-name:\#if\J<oneline>\W<class-name>\W\#endif<oneline>=$2@end
class-name:=@fail
base-part:\:\W<match-angle>=
base-part:final\I\W=
base-part:=@end
vars:\;=@end
vars:<I>=@report{variable,$1}
vars:\,=;\*=;\&=;\[<matchparen>\]=;<reqspace>=
vars:=@end

! structure definition
struct <i>\W\{<matchparen>\}<vars>=@report{struct,$1}$3
struct <class-name><space><base-part>\{<matchparen>\}<vars>=@report{class,$1}$5
union <condname>\W\{<matchparen>\}<vars>=@report{union,$1}$3
struct <I>\W\;=! ignore forward declarations

enum <i><space>\{<enumbody>\}<vars>=@out{@report{enum,$1}$3}
enum <I>\W\;=! ignore forward declarations

! templates
template\W\<<match-angle>\>\G\Wclass <class-name>\W<base-part>\{<matchparen>\}=@report{template,$2}
template\W\<<match-angle>\>=

! type alias
typedef <typed-name><space><typemod>\;=@report{type,$1}$3
typedef struct <I>\;=
typedef <I>\W<stars>(\W<stars><I>\W)\W<i>\W(<matchparen>)<space>\;=\
	@report{type,$4}
typedef const <I>\W<stars>(\W<stars><I>\W)\W<i>\W(<matchparen>)<space>\;=\
	@report{type,$4}
typedef enum<space>\{<enumbody>\}<space><I><space>\;=@out{$2@report{enum,$4}}
using <I>\W\=\W<id>\W\;=@report{type,$1}! C++11 alias

typemod:(<matchparen>)<space>=@end
typemod:\[<matchparen>\]<space>=@end
typemod:,<space><vars>=$2@end
typemod:=@end
typedef <matchparen>\;=@err{unrecognized: @inpath line @line\: $0\N}

condname:\#if\J<oneline>\W<condname>\W\#endif<oneline>=$2@end
condname:<I>=$1
condname:=@end

typed-name:enum <condname><space>\{<enumbody>\}=@out{@report{enum,$1}$3}
typed-name:struct\I\W<condname><space>\{<matchparen>\}=@out{@report{struct,$1}}
typed-name:union\I\W<condname><space>\{<matchparen>\}=@out{@report{union,$1}}
typed-name:struct <I> <I><space>\{<matchparen>\}=@out{@report{struct,$2}}
typed-name:oo<K1><I>(<I><optqual>)<space><stars><I0>=! ooRef(i) etc. is a type
typed-name:goto\I=@fail
typed-name:case\I=@fail
typed-name:using\I=@fail
typed-name:namespace\I=@fail
typed-name:typedef\I=@fail
typed-name:class <I>\W\;=@fail
typed-name:struct <I>\W\;=@fail
typed-name:<I> \P\C<i>PROTO<i>\W\(\W\(=$1@end
typed-name:<J> <I><space>\{<matchparen>\}=@out{@report{$1,$2}}
typed-name:<I><optqual><space><stars><I0>=
typed-name:<I><space>\*=
typed-name:<I><space>\&=
typed-name:<J>\I<space>\{<matchparen>\}=
typed-name:\*=
typed-name:\&=
typed-name:\S<space>=
typed-name:return\I=@fail;this\I=@fail
typed-name:<id>=$1@end
typed-name:<reqspace>=
typed-name:=@fail

stars:\*<space>=\*
stars:\&<space>=\&
stars:=@end

id:operator <I>=$0@end
id:operator\I\W<opchars>=operator $1@end
id:<I><optqual>=$1$2@end
id:\~<I><optqual>=$0@end
id:if\I=@fail
id:=@fail

optqual:\:\:\W<id>=$0
optqual:(<L1><I>\W)\G\W\:\:\W<id>=$0
optqual:\P\<\<=@end
optqual:\P\<\==@end
optqual:\W\<\L<match-angle>\>=$0
optqual:=@end

opchars:\==\=;\-=\-;\+=\+;\<=\<;\>=\>;\/=\/;\!=\!;\~=\~
opchars:\%=\%;\^=\^;\&=\&;\|=\|;\*=\*
opchars:=@terminate

match-angle:\<\<=$0
match-angle:\<\==$0
match-angle:\<#\>=$0
match-angle::matchparen

enumbody:<N>=
enumbody:<I>=@report{enumconst,$1}
enumbody:,=;\=\W<I>=
enumbody:<reqspace>=
enumbody:\#\L\W<I><oneline>=
enumbody:?=

matchparen:(#)=(#)
matchparen:\{#\}=\{#\}
matchparen:\[#\]=\[#\]
matchparen:\)=@fail;\}=@fail;\]=@fail
matchparen:\/\/*\n=\S
matchparen:\/\*<u>\*\/=\S
matchparen:\"<string>\"=$0
matchparen:\'<char>\'=$0
matchparen:\#\L\W<I><oneline>=$0

! optional white space
space:\/\/*\n=
space:\/\*<u>\*\/=
space:\S=
space:\#\L\Wif<j>\I<oneline>=
space:\#\L\We<j>\I<oneline>=
space:=@end

! required white space
reqspace:\/\/*\n=\S
reqspace:\/\*<u>\*\/=\S
reqspace:\S=\S
reqspace:\#\L\W<J>\I<oneline>=\N$0\N
reqspace:=@terminate

! skip reserved words
\Icase\I<matchparen>\;=
\Ibreak\;=
\Idefault\W\:=
\Iif\W(<matchparen>)<skip-statement>=
\Ifor\W(<matchparen>)<skip-statement>=
\Iswitch\W(<matchparen>)<skip-statement>=
\Ielse\I<skip-statement>=
\;=
\,=

skip-statement:\{<skip-body>=@end
skip-statement:\P\}=@end
skip-statement:<reqspace>=
skip-statement:<matchparen>\;=@end
skip-statement:=@end

extern\W\C"C"\W\{=
extern =
static =
inline =
__declspec\W(<matchparen>)=

namespace <id>\W\{<>\}=@out{@report{namespace,$1}}$2

! skip stray bodies whose headers weren't matched
\{<matchparen>\}=
\(<matchparen>\)=

! other things to ignore:
using namespace <id>\W\;=
using <id>\W\;=

! skip whitespace
\S=
! skip other identifiers
<I>=

