#!/net/one/wrld/drive1/users/gray/bin/solaris7/gema -f

! Filter Python script file for HTML display.
!----------------------------------------------------------
!  (The rest of this file is input for the "gema" program.)
!  (Documentation for "gema" is at: "http://one/~gray/gema-intro.html".)

! write to standard output:
@ARGV{-out\n-\n}

! case sensitive:
@set-switch{i;0}
! literal characters:
@set-syntax{L;\-\.\(\)}

ARGV:-title *\n=@set{title;$1}@set{heading;$1}

\B<prolog>=\
	\<html\>\<head\>\n\<title\>@var{title;@file}\<\/title\>\n\
	\<\/head\>\<body\>\n\
	\<h1\>@var{heading;@inpath}\<\/h1\>\n\<pre\>\n
\E=\N\<\/pre\>\n\<\/body\>\<\/html\>\n

prolog:\/dist\/project\/<F>\/RCS\/<F>\/<F>,v --\> st*out*\nrevision *\n=\
	@set{title;$3 $6}@set{heading;$2\/$3 revision $6}@end
prolog:\/dist\/project\/cvs\/<F>,v --\> st*out*\nrevision *\n=\
	@set{title;$1 $4}@set{heading;$1 revision $4}@end
prolog:=@end

color:*,<U>=\<font color\=$1\>$2\<\/font\>
color:<I>,\Z=@end
color:=

tohtml:\<=\&lt\;
tohtml:\>=\&gt\;
tohtml:\&=\&amp\;

\<=\&lt\;
\>=\&gt\;
\&=\&amp\;

! strings
\'\L<string>\'=\'@color{blue,$1}\'
string:\\\n=$0
string:\\^M\n=\\\n
string:\\?=\\@tohtml{?}
string:\<=\&lt\;
string:\>=\&gt\;
string:\&=\&amp\;

\"\L<string>\"=\"@color{blue,$1}\"
\'\'\'<string>\'\'\'=\'\'\'@color{blue,$1}\'\'\'
\"\"\"<string>\"\"\"=\"\"\"@color{blue,$1}\"\"\"

! skip whitespace
\s\L<s>=$0
<S1>=$1

! comments
\#<comment>\P\n=@color{green,\#$1}
comment:\ISPR<numprefix><D>\I=\<a href\="http\:\/\/one.objy.com\/cgi-bin\/sprnum.cgi\?$2"\>SPR$1$2\<\/a\>
comment:\Ispr<numprefix><D>\I=\<a href\="http\:\/\/one.objy.com\/cgi-bin\/sprnum.cgi\?$2"\>spr$1$2\<\/a\>
comment:\ICall<numprefix><D>\I=\<a href\=http\:\/\/one.objy.com\/cgi-bin\/ctsnum.cgi\?$2\>$0\<\/a\>
comment:\Ihttp\:\/\/<urlhost>=\<a href\="$0"\>$0\<\/a\>
comment:\Iftp\:\/\/<urlhost>=\<a href\="$0"\>$0\<\/a\>
comment:"<j2>tp\:\/\/<P>"="\<a href\=$0\>$1tp\:\/\/$2\<\/a\>"
comment:\<=\&lt\;
comment:\>=\&gt\;
comment:\&=\&amp\;
comment:^M\P\n=@end

numprefix:\L<S>=$1
numprefix:\#=$0
numprefix:\Cnum<w>=$0
numprefix:=@end

urlhost:\P. =@terminate;<J>=$1;\/=\/;\:=\:;\#=\#;\.<F0>=\.;<F1>=$1;=@terminate
urlhost:\?=\?;\&=\&;\%=\%
urlhost:\P\, =@terminate;\P\; =@terminate

! function
def<S><I><s>(=@color{purple,\<a name\="$2"\>def\<\/a\>}$1@color{red,$2}$3(

! global variable
\n<I>\L<s>\==\n@color{red,$1}$2\=
global<S><I><ids>=@color{purple,\<a name\="$2"\>global\<\/a\>}$1@color{red,$2}$3
ids:<I>=@color{red,\<a name\="$1"\>$1\<\/a\>}
ids:,=,;\s=\s;\t=\t;\\\n=\\\n;\\^M\n=\\\n;=@end

! class
class<S><I>=@color{purple,\<a name\="$2"\>class\<\/a\>}$1@color{red,$2}

! keywords
and\I=@color{purple,and}
assert\I=@color{purple,assert}
break\I=@color{purple,break}
continue\I=@color{purple,continue}
del\I=@color{purple,del}
elif\I=@color{purple,elif}
else\I=@color{purple,else}
except\I=@color{purple,except}
exec\I=@color{purple,exec}
finally\I=@color{purple,finally}
for\I=@color{purple,for}
from\I=@color{purple,from}
global\I=@color{purple,global}
if\I=@color{purple,if}
import\I=@color{purple,import}
in\I=@color{purple,in}
is\I=@color{purple,is}
lambda\I=@color{purple,lambda}
not\I=@color{purple,not}
or\I=@color{purple,or}
pass\I=@color{purple,pass}
print\I=@color{purple,print}
raise\I=@color{purple,raise}
return\I=@color{purple,return}
try\I=@color{purple,try}
while\I=@color{purple,while}

! Python 2.3
!yield\I=@color{purple,yield}

! other identifiers
<I>=$1
