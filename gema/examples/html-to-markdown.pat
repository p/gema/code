! Convert HTML to Source Forge wiki Markdown.
! ref https://sourceforge.net/nf/markdown_syntax
! or  https://daringfireball.net/projects/markdown/syntax

! Example usage:
!  gema -f html-to-markdown.pat -odir <directory> -otyp .md *.html


! set case-insensitive mode:
@set-switch{i;1}
! use alternate syntax as in -ml option:
@set-syntax{</>LLL;[|]</>}

@set{w;78}
@set-wrap{$w;}

<HTML>=
</HTML>\W=\N
<HEAD>*</HEAD>\W=\N
<BODY>\W=\N
</BODY>\W=\N
<TITLE>#</TITLE>\W=\N

<B>\W#</B>=\*\*#\*\*
<STRONG>\W#</STRONG>=\*\*#\*\*
<I>\W#</I>=_#_
<EM>\W#</EM>=_#_
<CITE>\W#</CITE>=_#_
<DFN>\W#</DFN>=_#_
<VAR>\W#</VAR>=_#_
<ADDRESS>\W#</ADDRESS>=_#_

<TT>\W#</TT>=\`#\`
<CODE>\W#</CODE>=\`#\`

<NOBR>#</NOBR>=@wrap{#}

<A HREF\=\"[href]\">\W$1\W</A>=\[$1\]
<A HREF\=\"[href]\">\W#</A>=\[$2\]\($1\)
href:\&amp\;=\&
href:\&quot\;=\"

<STRIKE>#</STRIKE>=<s>#</s>

<BR>\W=\S\s\n
<P>\W=\N\n
\n\W=\S
</P>\W=\N\n

! Note: This would need refinement to handle nested quotes, but that's rare.
<BLOCKQUOTE>\W=\N\n\>\s@set-wrap{76;\>\s}
</BLOCKQUOTE>\W=\N\n@set-wrap{$w;}

<PRE>\W[u]</PRE>=\N\`\`\`\n$1\N\`\`\`\n

@set{nextindent;}
<UL>\W=@push{indent;${nextindent}}\
	@push{nextindent;${indent}\s\s\s\s}\N@push{bullet;\*}
</UL>=@pop{indent}@pop{nextindent}@pop{bullet}\N
<OL>\W=@push{indent;${nextindent}}\
	@push{nextindent;${indent}\s\s\s\s}@push{bullet;1.}\N
</OL>=@pop{indent}@pop{nextindent}@pop{bullet}\N
<MENU>\W=@push{indent;${nextindent}}\
	@push{nextindent;${indent}\s\s\s\s}\N@push{bullet;\*}
</MENU>=@pop{indent}@pop{nextindent}@pop{bullet}\N
<LI>\W=\N${indent}${bullet}\s@incritem{${bullet}}
</LI>\W=\N
incritem:[D][u]=@set{bullet;@add{$1;1}$2}
incritem:=@end

<TABLE\I*>\W=\N
<TR>\W=\N
<TD>#</TD>\W</TR>=#\N
<TD>#</TD>\W=#\|
<TH>#</TH>\W</TR>=#
<TH>#</TH>\W=#\|
</TR>\W=\N
</TABLE>\W=\N
<TBODY\I*>\W=\N\n
</TBODY\I*>\W=\N

<H1>\W#</H1>\W=\N\n#\N@repeat{@length{$1};\=}\n\n
<H2>\W#</H2>\W=\N\n#\N@repeat{@length{$1};\-}\n\n
<H[D1]>\W#</H$1>=\n\N@repeat{$1;\#} $2 @repeat{$1;\#}\n

<HR>\W=\N\n----\n

\*=\\\*
\_=\\\_
\~=\\\~
\[=\\\[
\]=\\\]
\{=\\\{
\}=\\\}
\|=\\\|
\!=\\\!

<DIV\I[u]>*</DIV>=\N\n<div$1>$2\N</div>\n\n

<\!--[u]-->\W=\S
<\!*>=

<P\s[T]>\W\P<IMG\s=\N
<IMG SRC\=\"[href]\"\G [T]>=\!\[$2\]($1)
<IMG SRC\=\"[href]\"\G\W>=\!\[\]($1)

\&amp\;=\&
\&lt\;=\<
\&gt\;=\>
\&quot\;=\"
\&apos\;=\'
\&nbsp\;=\xA0

! punctuation which might follow a word and should stay with it:
@defset{B;.,\)\;\:\"\'\?\!\%\}\]\xA2-\xBE}
@defset{W;\xC0-\xFF}
\s[W][i][b]=@wrap{\s$1$2$3}
\n\W[W][i][b]=@wrap{\s$1$2$3}

\*[I0]=\\\*
\-\-[S0]=\-\-
\-=\\\-
C\+\+=C\+\+
\+=\\\+
\[=\\\[
\]=\\\]
\|=\\\|
\![-S0]=\\\!
\`=\\\`
\#=\\\#
\\=\\\\

\NLast modified\:\s*\n=\N\nConverted from "@file", which was last modified *\n

<script\I*>*</script>=@err{Warning at line @line\: unsupported script removed\n}

! unrecognized tags are passed through with a warning
<*>[s]=<$1>$2@err{Warning at line @line\: unsupported\: <$1>\n}
\&[I]\;=$0@err{Warning\: unsupported\: $0\n}
\&\#[D]\;=$0@err{Warning\: unsupported\: $0\n}

