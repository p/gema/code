#!/usr/local/bin/gema -f

! Given a spreadsheet exported by Excel in "tab separated text" format,
! output it as an HTML table.

@set-syntax{</>LLL;[|]</>}
@set{d;th}

ARGV:-title *\n=@set{title;*}
ARGV:-head*\n=@set{d;th}
ARGV:-nohead*\n=@set{d;td}

\B=<html>\n<head>\n  <title>@text{@var{title;\s}}</title>\n</head>\n<body>\n\n\
  <h1>@text{@var{title;\s}}</h1>\n\n<table border>\n<tr><$d>

\E=\N</table>\n\n<p>\n<hr>\n<\!-- Created: @datime -->\n<\!-- hhmts start -->\nLast modified: @file-time\n<\!-- hhmts end -->\n  </body>\n</html>\n

^M[S0]=
\n\t\t\W\P\E=</$d></tr>\n
\n=</$d></tr>\n@set{d;td}<tr><$d>
\t\P[endfield]=</$d><$d>\&nbsp\;
\t=</$d><$d>
"[quoted]"[S0]=$1

endfield:\t=@end
endfield:\n=\n@end
endfield:^M\n=\n@end
endfield:=@fail

<=\&lt\;
>=\&gt\;
\&=\&amp\;
\xA0=\&nbsp\;

text:<=\&lt\;
text:>=\&gt\;
text:\&=\&amp\;

quoted:""="
quoted:\n\Z=\n
quoted:\n=\n<br>
quoted::text
