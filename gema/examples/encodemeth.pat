! encode C++ method names for use as HTML name anchors
!
! This file is included by both "color-source.pat" and "xref-filter.pat"
! so that the anchor and reference will be encoded consistently.

encodeclass:<I>=$1
encodeclass:<S>=
encodeclass:\(=\.;\)\W\P\:\:=;\)=\.
encodeclass:\<<templargs>\>=\.$1
encodeclass:=@terminate
templargs:\,=\.
templargs::encodeclass

encodemethod:<I>=$1
encodemethod:<S>=
encodemethod:\&lt\;<templargs>\&gt\;=\.$1
encodemethod:\~\W<encodemethod>=\$d.$1
encodemethod:\!\W<encodemethod>=\$f.$1
encodemethod:operator\I=\$
encodemethod:\==e;\*=s;\[\W\]=i;\!=n;\+=p;\-=m
encodemethod:\&lt\;=l;\&gt\;=g;\<=l;\>=g;\/=v;\~=c;\&amp\;=a;\&=a
encodemethod:\:\:=\.\.
encodemethod:=@terminate


