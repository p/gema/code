

This "examples" directory contains some non-trivial pattern files for use
by "gema", some of which you might find useful, besides illustrating what
can be done.

These files demonstrate conversion between different documentation formats:

      latex.dat      convert LaTeX to HTML with Unicode
      tex.dat        used with "latex.dat" for low-level TeX features
      ht.dat         convert HTML to LaTeX
      man-html.dat   convert "man" page from "nroff" to HTML
      spreadsheet-to-html.pat  convert tab-separated text to HTML table
      html-to-markdown.pat     convert HTML to Markdown
      
  The script "latex.sh" can be used for LaTeX to HTML conversion after
  modifying it for the actual location of the files used.  It takes care of
  automatically running the program a second time when needed to resolve
  forward references. 

  These conversion pattern files are not complete; they have been
  sufficient for the documents I have been working with, but you may find
  additional features that you need.  The conversion provides a clear
  warning about what is not recognized, and extending the patterns is easy.

These files demonstrate conversion between different programming languages:

      c2dyl.dat     convert C to Dylan
      cpp-dyla.dat  supplements c2dyl.dat with additional rules for C++
      lspdyl.pat    convert Lisp to Dylan

  These are a crude preliminary syntax conversion to the Dylan programming
  language.  Even if you aren't interested in Dylan, the file "c2dyl.dat"
  serves as an example of how recursive pattern matching can be used to
  parse infix expressions; note how the rules look very similar to a BNF
  description of the expression syntax.

Additional examples of programming language parsing:

  The files "color-cpp.pat", "color-java.pat", and "color-python.pat"
  generate an HTML rendering with syntax coloring for programs in the
  respective languages.

  The file "cdefs.pat" scans C or C++ code to produce a list of the
  identifiers defined, and "cxref.pat" produces a cross-reference list of
  where given identifiers are referenced.  See the comments in the files
  for further information.

