@echo off 

REM  "gema" version 2 supplemental test script for Windows
REM  This tests the Unicode capabilities which are in addition to version 1.

set program=..\src\gema.exe

if NOT "%1"=="" set program=%1
del test9*.out >nul 2>nul

%program% -version -f test9utf8pat.dat -in test9utf8in.dat -out test9utf8.out
FC test9utf8out.dat  test9utf8.out

%program% -f test9utf8pat.dat -in test9utf16LEin.dat -out test9utf16LE.out
FC /U test9utf16LEout.dat  test9utf16LE.out

%program% -f test9utf8pat.dat -in test9utf16BEin.dat -out test9utf16BE.out
FC /B test9utf16BEout.dat  test9utf16BE.out

%program% -inenc utf-8 -f test9utf8pat.dat -in test9utf8in.dat -outenc UTF-16LE -out test9utf16LE2.out
FC /B test9utf16LEout.dat   test9utf16LE2.out

%program% -inenc auto -outenc utf-16be test9utf16LEout.dat test9utf16BE2.out
FC /B test9utf16BEout.dat   test9utf16BE2.out

REM   Test automatic recognition of UTF-16 without a BOM
%program% -in test9utf8pat.dat -outenc UTF-16LE -nobom -out test9utf16LEpat.out
%program% -in test9utf8in.dat -outenc UTF-16be -nobom -out test9utf16BEin.out
%program% -f test9utf16LEpat.out -in test9utf16BEin.out -outenc UTF-8 -out test9utf8x.out
FC test9utf8out.dat  test9utf8x.out

REM   Test @set-syntax by using Unicode characters for all syntax characters.
%program% -f altsyntaxpat.dat -inenc ASCII -in testpat.dat -outenc UTF-16LE -out alttestpat.out
%program% -f alttestpat.out -in testin.dat -out alttest.out
fc testout.dat alttest.out

%program% -inenc 8bit -f test10pat.dat -in test10in.dat -outenc 8bit -out test10.out
FC test10out.dat  test10.out

%program% -f test11pat.dat -in test11in.dat -out test11.out
FC test11out.dat  test11.out

%program% -f test12pat.dat -in test12in.dat -out test12.out
FC test12out.dat test12.out

echo The test passes if no errors or file comparison differences are shown above.
