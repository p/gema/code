@echo off 

REM  "gema" test script for MS-DOS or Windows

set program=..\src\gema.exe

if NOT "%1"=="" set program=%1
del test*.out %EF%>nul

%program% -version -f testpat.dat testin.dat test.out
fc testout.dat test.out
%program% -f testpat.dat -i -idchars "-_" -out test2.out test2.dat
fc test2out.dat test2.out
%program% -f testpat.dat -filechars ".,:/\\-_" -out test3.out -in - < test3.dat
fc test3out.dat test3.out
%program% -t -f testtok.dat testin.dat test4.out
fc testout.dat test4.out
%program% -T -W -MATCH -F TESTTW.DAT -ODIR %TEMP% -OTYP .OUT TEST5IN.DAT
fc test5out.dat %TEMP%\test5in.out
rem  tests 6 and 7 are for new features and bug fixes since version 1.3
%program% -ml -f test6pat.dat  test6in.dat > test6.out
fc test6out.dat  test6.out
%program% -f test7pat.dat > test7.out
fc test7out.dat test7.out
%program% -f test8pat.dat -in test8in.dat -out test8.out
fc test8out.dat  test8.out
%program% -f testwinpat.dat "@end" > testwin.out
fc testwinout.dat testwin.out
%program% -f test10pat.dat -in test10in.dat -out test10.out
fc test10out.dat test10.out

echo The test passes if no errors or file comparison differences are shown above.

REM  If this is the wide character configuration, do additional Unicode tests.
%program% "@cmps{@int-char{257};\1;@end;@fail;@end}" && unicode-test.bat %program%
