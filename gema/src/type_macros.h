#ifndef TYPE_MACROS_H
#define TYPE_MACROS_H

#if defined(_USE_ICU)
#define G_CHAR UChar
#define G_SCHAR UChar
#define G_CHAR_BYTES U_SIZEOF_UCHAR  /* number of bytes in a G_CHAR*/
#if U_SIZEOF_UCHAR < 3
#define G_XCHAR int  /* full 21-bit Unicode when UChar is 16 bits */
#else
#define G_XCHAR UChar
#endif
#elif defined(_USE_STDIO)
#define G_CHAR unsigned char /* supports only 8-bit characters */
#define G_SCHAR char	/* signed character */
#define G_CHAR_BYTES 1  /* number of bytes in a G_CHAR */
#define G_XCHAR unsigned char /* holds maximum supported character */
#elif defined(_USE_WCHAR)
#include <wchar.h>
#define G_CHAR wchar_t /* wide characters; might be either 16 bits or 32 bits */
#define G_SCHAR wchar_t
#if WCHAR_MAX == 0xFFFF
#define G_CHAR_BYTES 2
#define G_XCHAR int  /* full 21-bit Unicode when wchar_t is 16 bits */
#else
#define G_CHAR_BYTES 4
#define G_XCHAR wchar_t
#endif

#else
#error "Must define one of _USE_ICU, _USE_STDIO, or _USE_WCHAR"
#endif

#if 0 /* Tod's original concept, but ended up never using G_CHAR_OR_EOF */

 #if defined(_USE_ICU)
 #define G_CHAR_OR_EOF UChar
 #elif defined(_USE_STDIO)
 #define G_CHAR_OR_EOF int
 #elif defined(_USE_WCHAR)
 #define G_CHAR_OR_EOF wint_t
 #endif

 #if defined(_USE_ICU)
 #define G_EOF U_EOF
 #elif defined(_USE_STDIO)
 #define G_EOF EOF
 #elif defined(_USE_WCHAR)
 #define G_EOF WEOF
 #endif

#else
 /* Could now just use EOF instead of G_EOF, but would be too many changes. */
#define G_EOF (-1)
#endif

#define G_XCHAR_OR_EOF int  /* extended character or EOF */

/*
 * G_CHAR_COUNT: the type of G_CHAR counters, G_CHAR array sizes, etc.
 */
#if defined(_USE_ICU)
#define G_CHAR_COUNT int32_t
#else
#define G_CHAR_COUNT int
#endif

#if defined(_USE_ICU)
#define G_NULL_CHAR 0x0000
#elif defined(_USE_STDIO)
#define G_NULL_CHAR '\0'
#elif defined(_USE_WCHAR)
#define G_NULL_CHAR L'\0'
#endif

#if defined(_USE_ICU)
#define G_LINE_SEP 0x0208
#else
#define G_LINE_SEP '\n'
#endif

#endif /* TYPE_MACROS_H */
