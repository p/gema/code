/* character stream functions */

/*********************************************************************
  This file is part of "gema", the general-purpose macro translator,
  written by David N. Gray <dgray@acm.org> in 1994 and 1995.
  Extended for Unicode support in 2023.
  Adapted for the Macintosh by David A. Mundie.
  You may do whatever you like with this, so long as you retain
  an acknowledgment of the original source.
 *********************************************************************/

#if defined(_QC) || defined(_MSC_VER) /* Microsoft C or Quick C */
#pragma check_stack(off)
#endif

#ifdef __GNUC__
  /* For strict mode, allow POSIX functions in addition to ANSI C library. */
#define _POSIX_SOURCE
#endif

#define CSTREAM_C

/* suppress Microsoft warnings about "insecure" string functions */
#define _CRT_SECURE_NO_WARNINGS 1
/* suppress Microsoft warnings about POSIX functions */
#define _CRT_NONSTDC_NO_WARNINGS 1

#if defined(_WIN32) && !defined(_USE_STDIO)
#define boolean rpc_boolean  /* avoid conflict with my definition */
#include <Windows.h> /* for GetConsoleOutputCP and CP_UTF8 */
#undef boolean
#endif

#include "type_macros.h"
#include "io_macros.h"
#include "string_macros.h"
#include "cstream.h"
#include "util.h"
#include "main.h"
#include <assert.h>
#include <stdarg.h>
#include <errno.h>
#include <ctype.h>  /* for isspace */

#if defined(MACOS)
#include <Files.h>
#include <Strings.h>
#else
/* for the `stat' struct: */
#include <sys/types.h>
#include <sys/stat.h>
#endif

#ifndef SEEK_SET
/* hack for pre-ANSI header files */
#define SEEK_SET 0
/* hack for pre-ANSI library (such as "gcc" with SunOS library) */
#define memmove memcpy
#endif

#if defined(_USE_WCHAR) && !defined(_WIN32)
/* Simulate Windows wide string functions on Linux by converting to UTF-8 */
static FILE* _wfopen(const G_CHAR* name, const G_CHAR* perm)
{
  char pathbuf[G_MAX_PATH];
  char permbuf[8];
  wide_to_utf8(name, pathbuf, G_MAX_PATH);
  wide_to_utf8(perm, permbuf, sizeof(permbuf));
  return fopen(pathbuf, permbuf);
}

static int _wremove(const G_CHAR* path)
{
  char pathbuf[G_MAX_PATH];
  wide_to_utf8(path, pathbuf, G_MAX_PATH);
  return remove(pathbuf);
}

static int _wrename(const G_CHAR* old, const G_CHAR* new)
{
  char oldbuf[G_MAX_PATH];
  char newbuf[G_MAX_PATH];
  wide_to_utf8(old, oldbuf, G_MAX_PATH);
  wide_to_utf8(new, newbuf, G_MAX_PATH);
  return rename(oldbuf, newbuf);
}
#endif

static void
free_buffer( G_CHAR* start, G_CHAR* bufend );

static G_CHAR nullstring[] = {G_NULL_CHAR};

#define BOM 0xfeff /* Unicode Byte Order Mark */

typedef int readchar_function_type(CIStream s);
typedef int writechar_function_type(COStream s, G_XCHAR c);
struct encoding {
  const char name[10];
  readchar_function_type * read_function;
  writechar_function_type *write_function;
  boolean write_bom;
  boolean use_binary;
};


/* ============    Input Streams ==============  */

struct input_stream_struct
{
  G_FILE* fs;
  MarkBuf* first_mark;
  G_CHAR* start;
  G_CHAR* end;
  const G_CHAR* next;
  G_CHAR* bufend;
  G_XCHAR_OR_EOF peek_char;
  G_XCHAR_OR_EOF cur_char;
  const G_SCHAR* pathname;
  unsigned long line;
  G_CHAR_COUNT column;
#ifdef LUA
  int eof;
#endif
#if G_CHAR_BYTES > 1
  unsigned char rawbuf[6]; /* look-ahead buffer of raw file bytes */
  unsigned nraw; /* number of characters in rawbuf */
  Encoding file_encoding;
  readchar_function_type *readchar_function; /* virtual method for file read */
  unsigned n_utf8, n_8bit;
  boolean output_started;
#endif
  boolean binary;
};

#define is_string_stream(s) (s->next!=NULL)
#define is_file_stream(s) (s->fs!=NULL)

#define NoMark NULL
#define BufSize 4000

static CIStream in_free_list = NULL;

#if G_CHAR_BYTES > 1
static int readchar_skip_bom(CIStream s)
{
  int ch;
  assert(s->first_mark == NoMark); /* else would need more work */
  /* don't use this function the next time */
  s->readchar_function = s->file_encoding->read_function;
  ch = s->readchar_function(s); /* read first character */
  if ( ch == BOM )
    /* The first character in the file is a Byte Order Mark.
       The fact that it was recognized as such confirms that we are 
       using the correct encoding for the file.
       Skip to the next character. */
    return s->readchar_function(s);
  else return ch;
}
#endif

static CIStream
allocate_input_stream(void) {
  CIStream s;
  if ( in_free_list != NULL ) {
    s = in_free_list;
    in_free_list = (CIStream)(void*)s->next;
  }
  else
    s = (CIStream)allocate( sizeof(struct input_stream_struct), MemoryStream );
  s->first_mark = NoMark;
  s->peek_char = G_EOF;
  s->cur_char = G_EOF;
  s->line = 1;
  s->column = 0;
  s->next = NULL;
  s->start = NULL;
  s->end = NULL;
  s->bufend = NULL;
#ifdef LUA
  s->cur_char ='\0';
  s->eof=0;
#endif
#if G_CHAR_BYTES > 1
  s->nraw = 0;
  s->rawbuf[0] = '\0';
  s->file_encoding = input_encoding;
  s->readchar_function =
    input_encoding->write_bom ? readchar_skip_bom
    : s->file_encoding->read_function;
  s->n_utf8 = 0;
  s->n_8bit = 0;
  s->output_started = FALSE;
#endif
  s->binary = FALSE;
  return s;
}

#if G_CHAR_BYTES > 1

#ifdef _WIN32
#include <io.h> /* for _setmode */
#include <Fcntl.h>  /* for _O_BINARY */

static void set_binary_input(CIStream s, boolean skip_bom)
{
  if ( !s->binary ) { /* not already opened in binary mode */
    _setmode(G_FILENO(s->fs), _O_BINARY); /* set binary mode */
    cis_rewind(s); /* to reset the C library's buffer */
    if ( skip_bom ) {
      G_XCHAR ch;
      ch = s->readchar_function(s); /* skip the BOM */
      if ( ch != BOM ) {
        assert(FALSE);
        s->peek_char = ch;
      }
    }
  }
}
#else
#define set_binary_input(s, k)
#endif /* end _WIN32 */

static int cis_readchar_8bit(CIStream s)
{
  if ( s->nraw > 0 ) { /* read from byte buffer */
    int ch0 = s->rawbuf[0];
    int i;
    int n = s->nraw - 1;
    for ( i = 0 ; i < n ; i++ ) {
      s->rawbuf[i] = s->rawbuf[i+1]; /* shift remaining buffered characters */
    }
    s->nraw = n;
    return ch0;
  }
  return fgetc(s->fs);
}

static int cis_readchar_16le(CIStream s)
{
  int ch1, ch2, chx;
  FILE* f = s->fs;
  ch1 = fgetc(f);
  if ( ch1 == EOF )
    return EOF;
  ch2 = fgetc(f);
  if ( ch2 == EOF )
    return EOF;
  chx = (ch2 << 8) + ch1;
  if ( chx == L'\x0D' && !s->binary ) { /* on Windows, new line is CR,LF */
    int chy = cis_readchar_16le(s);
    if ( chy == L'\x0A' )
      chx = L'\n'; /* swallow the CR */
    else
      s->peek_char = chy;
  }
  else
  if ( IS_HI_SUR(chx) ) { /* upper surrogate */
    int chy = cis_readchar_16le(s);
    if ( IS_LO_SUR(chy) ) { /* lower surrogate */
      chx = COMBINE_SUR(chx,chy); /* extended character */
    } else { /* invalid unpaired surrogate */
      s->peek_char = chy;
    }
  }
  return chx;
}

static int cis_readchar_16be(CIStream s)
{
  int ch1, ch2, chx;
  FILE* f = s->fs;
  ch1 = fgetc(f);
  if ( ch1 == EOF )
    return EOF;
  ch2 = fgetc(f);
  if ( ch2 == EOF )
    return EOF;
  chx = (ch1 << 8) + ch2;
  if ( chx == L'\x0D' && !s->binary ) { /* on Windows, new line is CR,LF */
    int chy = cis_readchar_16be(s);
    if ( chy == L'\x0A' )
      chx = L'\n'; /* swallow the CR */
    else
      s->peek_char = chy;
  }
  else
  if ( IS_HI_SUR(chx) ) { /* upper surrogate */
    int chy = cis_readchar_16be(s);
    if ( IS_LO_SUR(chy) ) { /* lower surrogate */
      chx = COMBINE_SUR(chx,chy); /* extended character */
    } else { /* invalid unpaired surrogate */
      s->peek_char = chy;
    }
  }
  return chx;
}

static int cis_readchar_utf8(CIStream s)
{
  int ch1;
  unsigned nbytes;
  static unsigned nbtab[4] = { 2, 2, 3, 4 }; 
  if ( s->nraw == 0 ) {
    ch1 = fgetc(s->fs);
    if ( ch1 < 0xC2 ) /* EOF, ASCII, or not valid UTF-8 */
      goto not_utf8;
    s->rawbuf[0] = (unsigned char)ch1; /* might be start of UTF-8 sequence */
    s->nraw = 1;
  }
  else {
    ch1 = s->rawbuf[0];
    if ( ch1 < 0xC2 ) { /* ASCII or not valid UTF-8 */
      int i;
      int n = s->nraw - 1;
      for ( i = 0 ; i < n ; i++ ) {
        s->rawbuf[i] = s->rawbuf[i+1]; /* shift remaining buffered characters */
      }
      s->nraw = n;
      goto not_utf8;
    }
  }
  nbytes = nbtab[(ch1 & 0x30) >> 4]; /* number of bytes in UTF-8 sequence */
  while ( s->nraw < nbytes ) { /* buffer complete UTF-8 sequence */
    int chx = fgetc(s->fs);
    s->rawbuf[s->nraw++] = (chx == EOF ? '\0' : chx);
    if ( (chx & 0xC0) != 0x80 ) /* not part of a UTF-8 sequence */
      break;
  }
  s->rawbuf[s->nraw] = '\0';
  {
    int i, n, nused;
    int wch;
    const unsigned char* inptr;
    wch = one_wide_from_utf8(s->rawbuf, &inptr);
    nused = inptr - s->rawbuf; 
    n = s->nraw - nused;
    for ( i = 0 ; i < n ; i++ ) {
      s->rawbuf[i] = *inptr++;/* shift any unused characters */
    }
    s->nraw = n;
    s->rawbuf[n] = '\0';
    if ( nused > 1 )
      s->n_utf8++; /* counting number of UTF-8 sequences */
    else if ( (wch & ~0x7F) == 0x80 ) {
      assert(wch == ch1);
      goto not_utf8;
    }
    return wch;
  }
 not_utf8:
  if ( ch1 > 0x7F ) {
    s->n_8bit++; /* counting 8-bit characters which were not UTF-8 */
    if ( s->n_8bit >= 3 && s->n_utf8 == 0 ) {
      /* This file appears to be just 8 bit, not UTF-8 */
      s->file_encoding = find_encoding("8bit");
      s->readchar_function = cis_readchar_8bit;
    }
  }
  return ch1;
}

#endif /* end G_CHAR_BYTES > 1 */

static boolean
extend_buffer ( CIStream s ) {
  if ( s->first_mark == NoMark ) { /* don't need buffer anymore */
    s->cur_char = cis_prevch(s);
    free( s->start );
    s->start = NULL; s->next = NULL; s->bufend = NULL;
    s->peek_char = G_EOF;
  }
  else if ( s->end < (s->bufend-2) ) { /* read more into buffer */
    if ( G_FGETFILE(s->fs) == stdin && ! binary ) {
      /* for interactive input, don't read more than 1 line at a time */
      G_CHAR* r;
#if defined(_USE_WCHAR)
      if ( s->readchar_function != cis_readchar_8bit ) {
        /* Unicode file */
        const G_CHAR* rlimit = s->bufend-1;
        for ( r = s->end ; r < rlimit ; ) {
          int chn;
          chn = s->readchar_function(s);
          if ( chn == EOF ) {
            if ( r == s->end )
              return FALSE; /* nothing read */
            else break;
          }
#if G_CHAR_BYTES == 2
          if ( chn > 0xFFFF ) {
            /* generate surrogate pair for extended character */
            *r++ = HISUR(chn);
            chn = LOSUR(chn);
          }
#endif
          *r++ = (G_CHAR)chn;
          if ( chn == '\n' )
            break;
        } /* end for r */
        s->end = r;
      } /* end Unicode file */
      else
#endif
        {
          r = G_FGETS(s->end, (s->bufend - s->end), s->fs);
          if ( r != NULL )
            s->end = r + G_STRLEN(r);
          else return FALSE;
        }
    } /* end interactive input */
    else {
      G_CHAR* bp;
      size_t nread, maxread;
      bp = s->end;
      maxread = s->bufend - bp - 1;
      if ( maxread > 128 )
        maxread = 128;
#if defined(_USE_WCHAR)
      for ( nread = 0 ; nread < maxread ; nread++ ) {
        int chn;
        chn = s->readchar_function(s);
        if ( chn == EOF ) {
          if ( nread == 0 )
            return FALSE;  /* nothing read */
          else break;
        }
#if G_CHAR_BYTES == 2
        if ( chn > 0xFFFF ) {
          /* generate surrogate pair for extended character */
          *bp++ = HISUR(chn);
          chn = LOSUR(chn);
          nread++;
        }
#endif
        *bp++ = (G_CHAR)chn;
      } /* end for nread */
      s->end = bp;
      *bp = G_NULL_CHAR;
#else
      nread = G_FREAD(bp, maxread, s->fs);
      if ( nread > 0 ) {
        s->end = bp + nread;
      }
      else return FALSE;
#endif
    }  /* end not interactive input */
  } /* end read into buffer */
  else { /* need to expand the buffer */
    G_CHAR* x;
    size_t n;
    size_t newsize;
    n = s->end - s->start;
    newsize = (s->bufend - s->start) + BufSize;
    x = realloc( s->start, newsize*sizeof(G_CHAR) );
    if ( x != NULL ) { /* expanded OK */
      s->start = x;
      s->end = x + n;
      s->next = s->end;
      s->bufend = x + newsize;
    }
    else {
      fputs("Out of memory for expanding input buffer.\n",stderr);
      exit(EXS_MEM);
    }
  }
  return TRUE;
}

/* read one character or G_EOF */
G_XCHAR_OR_EOF cis_getch(CIStream s){
  int ch;
  if ( is_string_stream(s) ) {
    if ( s->next >= s->end ) {
      if ( is_file_stream(s) ) { /* buffered file stream */
	if ( extend_buffer(s) )
	  return cis_getch(s);
      }
#ifdef LUA
      s->eof = 1;
#endif
      return G_EOF;
    }
    if ( (s->next > s->start) && (s->next[-1] == '\n') ) {
      s->line++;
      s->column = 1;
    }
    else s->column++;
    {
      G_XCHAR xch = *s->next++;
#if G_CHAR_BYTES == 2
      if ( IS_HI_SUR(xch) && IS_LO_SUR(*s->next) )
        xch = COMBINE_SUR(xch,*s->next++);
#endif
      return xch;
    }
  }
  else {
    if ( s->cur_char == '\n' ) {
      s->line++;
      s->column = 1;
    }
    else s->column++;
    if ( s->peek_char != G_EOF ) {
      ch = s->peek_char;
      s->peek_char = G_EOF;
    } else {
#if G_CHAR_BYTES == 1
      ch = fgetc(s->fs);
#else
      ch = s->readchar_function(s); /* read according to file's encoding */
#endif
      if ( ch == EOF ) {
        if ( G_EOF != EOF )
          ch = G_EOF;
        if ( !G_FEOF(s->fs) ) {
#ifdef LUA
        s->eof = 1;
#endif
	if ( s->pathname != NULL )
	  G_PERROR(s->pathname);
	else
	  perror("stdin");
	if ( keep_going )
	  exit_status = EXS_INPUT;
	else exit(EXS_INPUT);
	}
#ifdef LUA
	s->eof = 1;
#endif
      } /* end EOF */
    }
    s->cur_char = ch;

    return ch;
  }
}

/* peek ahead to the next character to be read */
G_XCHAR_OR_EOF cis_peek(CIStream s){
  if ( is_string_stream(s) ) {
    if ( s->next >= s->end ) {
      if ( is_file_stream(s) ) { /* buffered file stream */
	if ( extend_buffer(s) )
	  return cis_peek(s);
      }
      return G_EOF;
    }
    else {
      G_XCHAR xch = s->next[0];
#if G_CHAR_BYTES == 2
      if ( IS_HI_SUR(xch) && IS_LO_SUR(s->next[1]) )
        xch = COMBINE_SUR(xch,s->next[1]);
#endif
      return xch;
    }
  }
  else {
    if ( s->peek_char == G_EOF ) {
#if G_CHAR_BYTES == 1
      int nc = fgetc(s->fs);
#else
      int nc = s->readchar_function(s);
#endif
      s->peek_char = (nc == EOF ? G_EOF : nc);
    }
    return s->peek_char;
  }
}

#if G_CHAR_BYTES == 2
/* peek ahead, returning the next character if it is in the range
   to apply the is... character type functions.  If the next character
   is outside of that range, then nul is returned. */
G_CHAR cis_peek_short(CIStream s){
  G_XCHAR_OR_EOF ch;
  ch = cis_peek(s);
  if ( ch >= 0xFFFF || ch == G_EOF )
    return G_NULL_CHAR;
  else return (G_CHAR)ch;
}
G_CHAR cis_prevch_short(CIStream s){
  G_XCHAR_OR_EOF ch;
  if ( is_string_stream(s) ) {
    if ( s->next <= s->start )
      return G_NULL_CHAR;
    else return s->next[-1];
  }
  ch = s->cur_char;
  if ( ch >= 0xFFFF || ch == G_EOF )
    return G_NULL_CHAR;
  else return (G_CHAR)ch;
}
#endif

#ifdef LUA
int cis_eof(CIStream s)
{
  return (s->eof);
}
#endif


/* return the previous character read */
G_XCHAR_OR_EOF cis_prevch(CIStream s){
  if ( is_string_stream(s) ) {
    if ( s->next <= s->start )
      return G_EOF;
    else return s->next[-1];
  }
  else return s->cur_char;
}

unsigned long cis_line(CIStream s) {
  return s==NULL? (unsigned long)0 : s->line;
}

G_CHAR_COUNT cis_column(CIStream s) {
  return s==NULL? 0 : s->column;
}

/* remember current position */
void cis_mark(CIStream s,MarkBuf* mb){
  long m;
  if ( is_string_stream(s) ) {
    m = s->next - s->start;
    mb->pos_kind = Buffer_Position;
  }
  else {
#if G_CHAR_BYTES > 1
    assert(s->readchar_function != readchar_skip_bom); /* else need more work here */
#endif
#ifndef MSDOS /* seek is too slow on MS-DOS */
    if ( s == stdin_stream  /* can't trust rewinding a pipe */
#if G_CHAR_BYTES > 1
         || ( (!s->output_started) &&
              s->readchar_function == cis_readchar_utf8 && 
              (s->n_utf8 | s->n_8bit) == 0 )
         /* If we don't yet know if this is 8bit or UTF8, buffer ahead to
            try to find out before we need to decide the output file
            encoding. */
#endif
         )
      m = -1;
    else {
      mb->pos_kind = Seek_Position;
      m = G_FTELL(s->fs);
    }
    if ( m < 0 ) /* seek is not supported; need to buffer */
#endif
     {
      G_CHAR* r;
      r = (G_CHAR*) allocate( BufSize*sizeof(G_CHAR), MemoryInputBuf );
      s->start = r;
      if ( s->cur_char != G_EOF )
        PUT_XCHAR(r, s->cur_char);
      s->next = r;
      if ( s->peek_char != G_EOF ) 
        PUT_XCHAR(r, s->peek_char);
      s->end = r;
      s->bufend = s->start + BufSize;
      m = s->next - s->start;
      mb->pos_kind = Buffer_Position;
    }
  }
  if ( s->first_mark == NoMark )
    s->first_mark = mb;
  else assert( m >= s->first_mark->position );
  mb->position = m;
  mb->line = s->line;
  mb->column = s->column;
  mb->prevch = s->cur_char;
  mb->peekch = s->peek_char;
#if G_CHAR_BYTES > 1
  if ( is_string_stream(s) )
    mb->nraw = -1; /* not applicable */
  else {
    if ( s->nraw > 0 ) {
      memcpy(mb->rawbuf, s->rawbuf, s->nraw);
    }
    mb->nraw = s->nraw;
  }
#endif
}

void cis_release(CIStream s, MarkBuf* mb) {
  if ( mb == s->first_mark ) {
      s->first_mark = NoMark;
      if ( is_string_stream(s) && is_file_stream(s) ) {
	size_t len = s->end - s->next;
	size_t done = s->next - s->start;
	if ( len*4 < done && done > 100 ) {
	  G_CHAR* n;

	  memmove( s->start, s->next-1, (len+1)*sizeof(G_CHAR) );
	  n = s->start+1;
	  s->next = n;
	  s->end = n + len;
	  *s->end = '\0';
	}
      }
  }
  else assert( s->first_mark != NoMark );
}

/* restore to remembered position; returns 0 if OK */
void cis_restore(CIStream s, MarkBuf* mb){
  long pos;
  pos = mb->position;
  if ( mb->pos_kind == Buffer_Position ) {
    assert( is_string_stream(s) );
    s->next = s->start + pos;
    if ( s->next < s->start || s->next > s->end ) {
      fprintf(stderr, "cis_restore invalid position %ld\n", pos);
      exit(EXS_INPUT);
    }
  }
  else {
    int status;
    assert( mb->pos_kind == Seek_Position );
    assert( !is_string_stream(s) );
    status = G_FSEEK(s->fs, pos, SEEK_SET);
    if ( status != 0 ) {
      input_error(s, EXS_INPUT, "Can't rewind input file:\n");
      G_PERROR(s->pathname);
      exit(EXS_INPUT);
    }
  }
  cis_release(s, mb);
  s->line = mb->line;
  s->column = mb->column;
  s->cur_char = mb->prevch;
  s->peek_char = mb->peekch;
#if G_CHAR_BYTES > 1
  if ( mb->nraw >= 0 ) {
    assert( mb->pos_kind == Seek_Position );
    assert( !is_string_stream(s) );
    if ( mb->nraw > 0 ) {
      memcpy(s->rawbuf, mb->rawbuf, mb->nraw);
    }
    s->nraw = mb->nraw;
  }
  else assert( mb->pos_kind == Buffer_Position );
#endif
}

/* reset to read from the beginning */
void cis_rewind(CIStream s){
  if ( is_string_stream(s) )
    s->next = s->start;
  else {
    int rc;
    s->peek_char = G_EOF;
    s->cur_char = G_EOF;
    rc = G_FSEEK(s->fs, 0, SEEK_SET);
    if ( rc != 0 ) {
      input_error(s, EXS_INPUT, "Can't rewind input file:\n");
      G_PERROR(s->pathname);
      if ( ! keep_going ) {
        exit(EXS_INPUT);
      }
    }
  }
#if G_CHAR_BYTES > 1
  s->nraw = 0;
#endif
}

/* create stream */
CIStream make_file_input_stream(G_FILE* f, const G_SCHAR* path){
  register CIStream s = allocate_input_stream();
  s->fs = f;
  s->pathname = g_str_dup(path);
  return s;
}

static const G_SCHAR emptystring[2] = { G_NULL_CHAR };

CIStream
open_input_file( const G_SCHAR* pathname, boolean binary )
{
  G_FILE* infs;
  boolean open_as_binary;
  if ( pathname[0] == '-' && pathname[1] == '\0' )
    return stdin_stream;
  open_as_binary = binary;
#if G_CHAR_BYTES > 1
  open_as_binary |= input_encoding->use_binary;
#endif
#if defined(_USE_WCHAR)
  infs = G_FOPEN( pathname, open_as_binary? L"rb" : L"r", NULL, NULL );
#else
  infs = G_FOPEN( pathname, open_as_binary? "rb" : "r", NULL, NULL );
#endif
  if ( infs == NULL ) {
    input_error(input_stream, EXS_INPUT, "Can't open file for reading:\n");
    G_PERROR(pathname);
    if ( keep_going )
      return NULL;
    else exit(EXS_INPUT);
  }
  {
  CIStream result = make_file_input_stream(infs,pathname);
  result->binary = binary;
  return result;
  }
}

const G_SCHAR* cis_pathname(CIStream s){
  return s==NULL? emptystring : s->pathname;
}

boolean cis_is_file(CIStream s) {
  return is_file_stream(s);
}

#ifdef MACOS
static CInfoPBRec myblock;

OSErr get_info_block(const char* pathname) {
	static WDPBRec mywd;
	OSErr e;

	e = PBHGetVol(&mywd, 0);
	if (e) return e;
	pathname = c2pstr(pathname);
	myblock.hFileInfo.ioNamePtr = pathname;
	myblock.hFileInfo.ioVRefNum = mywd.ioVRefNum;
	myblock.hFileInfo.ioDirID = mywd.ioWDDirID;
	e=PBGetCatInfo(&myblock, 0);
	pathname = p2cstr(pathname);
	return e;
}
#endif

time_t cis_mod_time(CIStream s) {
  if ( s != NULL && s->fs != NULL && s->pathname != NULL ) {
#ifdef MACOS
    if (get_info_block(s->pathname)==0)
      return myblock.hFileInfo.ioFlMdDat;
    else return 0;
#else
    struct stat sbuf;
    fstat( G_FILENO(s->fs), &sbuf );
    return sbuf.st_mtime;
#endif
  }
  else return 0;
}

char probe_pathname(const G_CHAR* pathname) {
#ifdef MACOS
    if (get_info_block(pathname)==0) {
      if (myblock.hFileInfo.ioFlAttrib & 16)
	return 'D'; /* Directory */
      else return 'F';        /* File */
    } else return 'X'; /* Unexpected */
#else
    int rc;
#if defined(_USE_WCHAR) && defined(_WIN32)
    struct _stat sbuf;
    rc = _wstat( pathname, &sbuf );
#elif G_CHAR_BYTES == 1
    struct stat sbuf;
    rc = stat( pathname, &sbuf );
#else
    struct stat sbuf;
    char pathbuf[G_MAX_PATH];
    wide_to_utf8(pathname, pathbuf, sizeof(pathbuf));
    rc = stat( pathbuf, &sbuf );
#endif
    if ( rc == 0 ) {
      int stmode;
      stmode = sbuf.st_mode;
#ifdef S_ISDIR /* POSIX */
      if ( S_ISDIR(stmode) )
        return 'D'; /* directory */
      else if ( S_ISREG(stmode) )
        return 'F'; /* file */
      else if ( S_ISCHR(stmode) )
        return 'V'; /* device */
#else     /* older Unix way, still used by Microsoft */
      if ( stmode & S_IFDIR )
        return 'D'; /* directory */
      else if ( stmode & S_IFREG )
        return 'F'; /* file */
      else if ( stmode & S_IFCHR )
        return 'V'; /* device */
#endif
      else return 'X'; /* unexpected mode */
    }
    else return 'U'; /* undefined */
#endif
}

CIStream make_string_input_stream(const G_CHAR* x, G_CHAR_COUNT length,
				  boolean copy){
  register CIStream s = allocate_input_stream();
  if ( length == 0 )
    length = G_STRLEN(x);
  s->fs = NULL;
  s->pathname = NULL;
  s->start = (G_CHAR*)x;
  if ( copy ) {
    size_t alen = length+1;
    size_t bytes = alen * sizeof(G_CHAR);
    s->start = allocate(bytes, MemoryInputBuf);
    memcpy(s->start, x, bytes);
    s->bufend = s->start + alen;
  }
  s->next = s->start;
  s->end = s->start + length;
  return s;
}

#if G_CHAR_BYTES > 1
CIStream make_narrow_string_input_stream(const char* x, int length)
{
  unsigned i;
  size_t alen;
  size_t bytes;
  register CIStream s = allocate_input_stream();
  if ( length == 0 )
    length = strlen(x);
  s->fs = NULL;
  s->pathname = NULL;
  alen = length+1;
  bytes = alen * sizeof(G_CHAR);
  s->start = allocate(bytes, MemoryInputBuf);
  for ( i = 0 ; i < alen ; i++ ) {
    s->start[i] = (unsigned char)x[i];
  }
  s->bufend = s->start + alen;
  s->next = s->start;
  s->end = s->start + length;
  return s;
}
#endif

CIStream clone_input_stream( CIStream in ) {
  register CIStream s = allocate_input_stream();
  assert ( is_string_stream(in) );
  *s = *in;
  s->first_mark = NoMark;
  s->bufend = NULL; /* to not deallocate when closed */
  return s;
}

CIStream copy_read_input_stream(CIStream in)
{
  CIStream result;
  assert( is_string_stream(in) );
  result = make_string_input_stream(in->next, (in->end - in->next), TRUE);
  in->next = in->end; /* read all of input */
  return result;
}

/* Convert a file stream to a string stream for when we need all of it now. */
static void cis_buffer_file ( CIStream s ) {
  if ( is_file_stream(s) ) {
    COStream outbuf;
    CIStream temp;
    outbuf = make_buffer_output_stream();
    cos_copy_input_stream(outbuf, s); /* reads the whole file into memory */

    /* Do like cis_close except don't dealocate the input_stream_struct */
    fclose(s->fs);
    s->fs = NULL;
    if ( s->pathname != NULL ) {
      free((char*)s->pathname);
      s->pathname = NULL;
    }
    assert(s->start == NULL);

    temp = convert_output_to_input(outbuf);

    *s = *temp; /* put the new string stream in the place of the file stream */
    free(temp);
  } /* end is_file_stream */
}

G_CHAR* cis_whole_string ( CIStream s ) {
  if ( s == NULL )
    return nullstring;
  else {
    if ( is_file_stream(s) )
      cis_buffer_file(s);
    assert( is_string_stream(s) );
    assert( *s->end == '\0' );
    return s->start;
  }
}

/* number of array elements in input buffer stream */
unsigned cis_length( CIStream s ){
  if ( s == NULL )
    return 0;
  else {
    if ( is_file_stream(s) )
      cis_buffer_file(s);
    assert( is_string_stream(s) );
    return s->end - s->start;
  }
}

#if G_CHAR_BYTES == 2
/* number of characters in input buffer stream */
unsigned cis_character_count( CIStream s ){
  unsigned int n = 0;
  if ( s != NULL ) {
    const G_CHAR* ptr;
    if ( is_file_stream(s) )
      cis_buffer_file(s);
    assert( is_string_stream(s) );
    for ( ptr = s->start ; ptr < s->end ; ptr++ ) {
      n++;
      if ( IS_HI_SUR(ptr[0]) && IS_LO_SUR(ptr[1]) )
        ptr++; /* surrogate pair counts as one character */
    }
  }
  return n;
}
#endif

/* close stream */
void cis_close(CIStream s){
  if ( s == NULL || s == stdin_stream )
    return;
  if ( is_file_stream(s) ) {
    G_FCLOSE(s->fs);
    s->fs = NULL;
    if ( s->pathname != NULL ) {
      free((char*)s->pathname);
      s->pathname = NULL;
    }
  }
  s->next = NULL;
  if ( s->bufend != NULL ) {
    free_buffer(s->start, s->bufend);
    s->start = NULL;
    s->bufend = NULL;
  }

  /* instead of:  free(s);  */
  s->next = (G_CHAR*)(void*)in_free_list;
  in_free_list = s;
}

void input_error( CIStream s, Exit_States code, const char* format, ... ) {
  va_list args;
  va_start(args,format);
  if ( code > exit_status )
    exit_status = (Exit_States)code;
  fflush(stdout);
  if ( s != NULL ) {
    if ( !is_file_stream(s) )
      s = input_stream;
    if ( s != NULL && is_file_stream(s) ) {
      const G_SCHAR* path = cis_pathname(s);
      if ( path != NULL )
       {
#if defined(MACOS)
	/* For MPW, this makes the output "self-referential I/O" - users can
		execute the error message as a command, and it takes them to
		the point in the file where the error occurred.
		-- D.A.M. 7/21/95 */
	fprintf(stderr, "File \"%s\"; line %ld # ",
#else
        int in_column;
        in_column = cis_column(s);
        if ( in_column > 0 )
          fprintf(stderr, "File \"%s\" line %ld, column %d: ",
                  message_string(pathname_name_and_type(path)),
                  cis_line(s), in_column);
        else
	fprintf(stderr, "File \"%s\" line %ld: ",
#endif
                message_string(pathname_name_and_type(path)),
                cis_line(s) );
       } /* end if path != NULL */
    }
  }
#if defined(_USE_ICU)
  u_vfprintf(stderr, format, args);
#else
  vfprintf(stderr, format, args);
#endif
  fflush(stderr);
  va_end(args);
}


/* ============    Output Streams ==============  */

struct output_stream_struct {
  G_FILE* fs;   /* the file descriptor */
  const G_SCHAR* pathname;
  G_CHAR* start;
  G_CHAR* next;
  G_XCHAR_OR_EOF lastch; /* last character written */
  G_CHAR_COUNT column;
  G_CHAR* bufend;
#if G_CHAR_BYTES > 1
  Encoding file_encoding;
  /* virtual method for file write */
  writechar_function_type *writechar_function;
#endif
  boolean binary;
  char probe_before;
  G_SCHAR* backup_path;
};

#define OutBufSize 512
static G_CHAR* free_buf_list = NULL;

static COStream out_free_list = NULL;

#if G_CHAR_BYTES > 1

#ifdef _WIN32
static void set_binary_output(COStream s)
{
  if ( !s->binary ) {
    _setmode(G_FILENO(s->fs), _O_BINARY);
  }
}
#else
#define set_binary_output(s)
#endif

/* Write one Unicode character to a file using UTF-8 encoding */
static int cos_writechar_utf8(COStream s, G_XCHAR wch) {
  FILE* f;
  int rc;
  f = s->fs;
  if ( wch <= 0x7F )
    return fputc(wch, f);
  else if ( wch <= 0x07FF ) {
    /* 110xxxxx 10xxxxxx */
    rc = fputc((0xC0 + (wch >> 6)), f);
    if ( rc != EOF )
      rc = fputc((0x80 + (wch & 0x3F)), f);
    return rc;
  }
  else if ( wch <= 0xFFFF  ) {
    /* 1110xxxx 10xxxxxx 10xxxxxx */
    rc = fputc((0xE0 + (wch >> 12)), f);
    if ( rc != EOF ) {
      rc = fputc((0x80 + ((wch >> 6) & 0x3F)), f);
      if ( rc != EOF ) {
        rc = fputc((0x80 + (wch & 0x3F)), f);
      }
    }
    return rc;
  }
  else if ( wch <= 0x1FFFFF ) { /* although maximum Unicode is 0x10FFFF */
    /* 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx */
    rc = fputc((0xF0 + (wch >> 18)), f);
    if ( rc != EOF ) {
      rc = fputc((0x80 + ((wch >> 12) & 0x3F)), f);
      if ( rc != EOF ) {
        rc = fputc((0x80 + ((wch >> 6) & 0x3F)), f);
        if ( rc != EOF ) {
          rc = fputc((0x80 + (wch & 0x3F)), f);
        } } }
    return rc;
  }
  else /* this case shouldn't be possible */
    fprintf(stderr, "Invalid character for writing as UTF-8: 0x%X\n",
            (int)wch);
  return fputc(wch,s->fs);
}

/* Write one 8-bit character to a file */
static int cos_writechar_8bit(COStream s, G_XCHAR c) {
  if ( c > 0xFF )
    return cos_writechar_utf8(s, c);
  return fputc(c, s->fs);
}

static int cos_writechar_ascii_only(COStream s, G_XCHAR c) {
  if ( c < 0x7F && (c >= 0x20 || isspace(c)) )
    /* ASCII character can be written as-is */
    return fputc(c, s->fs);
  else {
    int rc;
    /* show hexadecimal code for characters that can't be directly displayed */
    if ( c > 0xFF ) {
      /* Use braces to avoid any ambiguity with whatever comes next. */
      rc = fprintf(s->fs, "\\u{%04X}", (unsigned int)c);
    }
    else {
      char buf[16];  
      char* endp = visualize_char(buf, c);
      *endp = '\0';
      rc = fputs(buf, s->fs);
    }
    return (rc >= 0 ? c : EOF);
  }
}

/* Write one Unicode character to a file using UTF-16 little-endian encoding */
static int cos_writechar_16le(COStream s, G_XCHAR wch) {
  FILE* f;
  int rc;
  if ( wch > 0xFFFF ) {
    /* generate surrogate pair for extended character */
    if ( cos_writechar_16le(s, HISUR(wch)) == EOF )
      return EOF;
    wch = LOSUR(wch);
  }
#if defined(MSDOS)
  if ( (wch == L'\n') && (!s->binary) && (s->lastch != L'\r') ) {
    /* on Windows, new line is CR,LF */
    if ( cos_writechar_16le(s, L'\r') == EOF )
      return EOF;
  }
#endif
  f = s->fs;
  rc = fputc((wch & 0xFF), f);
  if ( rc != EOF )
    rc = fputc((wch >> 8), f);
  return rc;
}

/* Write one Unicode character to a file using UTF-16 big-endian encoding */
static int cos_writechar_16be(COStream s, G_XCHAR wch) {
  FILE* f;
  int rc;
  if ( wch > 0xFFFF ) {
    /* generate surrogate pair for extended character */
    if ( cos_writechar_16be(s, HISUR(wch)) == EOF )
      return EOF;
    wch = LOSUR(wch);
  }
#if defined(MSDOS)
  if ( (wch == L'\n') && (!s->binary) && (s->lastch != L'\r') ) {
    /* on Windows, new line is CR,LF */
    if ( cos_writechar_16be(s, L'\r') == EOF )
      return EOF;
  }
#endif
  f = s->fs;
  rc = fputc((wch >> 8), f);
  if ( rc != EOF )
    rc = fputc((wch & 0xFF), f);
  return rc;
}

#endif /* end G_CHAR_BYTES > 1 */
  
static COStream
allocate_output_stream(void) {
  if ( out_free_list != NULL ) {
    COStream s = out_free_list;
    out_free_list = (COStream)(void*)s->next;
    return s;
  }
  return (COStream)
    allocate( sizeof(struct output_stream_struct), MemoryStream );
}

static void
free_output_stream(COStream s) {
  s->next = (G_CHAR*)(void*)out_free_list;
  out_free_list = s;
}

static void
expand_output_buffer ( COStream s, int need ) {
   G_CHAR* x;
   size_t n;
   size_t more;
   size_t oldsize;
   size_t newsize;

   oldsize = s->bufend - s->start;
   more = OutBufSize + (oldsize >> 2) ;
   if ( (size_t)need >= more )
     more = (size_t)need + OutBufSize;
   newsize = oldsize + more;
   n = s->next - s->start;
#ifdef LUA
   x = realloc( s->start, (newsize + 1)*sizeof(G_CHAR) );
#else
     x = realloc( s->start, newsize*sizeof(G_CHAR) );
#endif
   if ( x != NULL ) { /* expanded OK */
     s->start = x;
     s->next = x + n;
     s->bufend = x + newsize;
   }
   else {
     fputs("Out of memory for expanding output buffer.\n",stderr);
     exit(EXS_MEM);
   }
}

#ifdef LUA
G_CHAR* cos_whole_string ( COStream s ) {
  if ( s == NULL )
    return emptystring;
  else {
    assert( is_string_stream(s) );
    *s->next = G_NULL_CHAR;
    return s->start;
  }
}

#endif

/* write one character */
void cos_putch(COStream s, G_XCHAR_OR_EOF c){
  assert( c != EOF );
  if ( is_string_stream(s) ) {
#if G_CHAR_BYTES == 2
    if ( c > 0xFFFF ) {
      /* generate surrogate pair for extended character */
      cos_putch(s, HISUR(c));
      cos_putch(s, LOSUR(c));
      return;
    }
#endif
    if ( s->next >= s->bufend )
      expand_output_buffer ( s, 1 );
    *s->next++ = c;
  }
  else {
    int rc;
#if G_CHAR_BYTES == 1
    rc = fputc(c,s->fs);
#else
    /* use a virtual method dependent on the file encoding */
    rc = s->writechar_function(s, c);
#endif
    if ( rc == EOF ) {
      fflush(stdout);
      fprintf(stderr, "Error writing to output file:\n");
      G_PERROR(s->pathname);
      exit(EXS_OUTPUT);
    }
    s->lastch = c;
    if ( c == '\n' )
      s->column = 1;
    else s->column++;
  }
}

/* write some number of spaces */
void cos_spaces(COStream s, int n) {
  int i;
  for ( i = n ; i > 0 ; i-- )
    cos_putch(s,' ');
}

#if G_CHAR_BYTES > 1
/* When G_CHAR is larger than char, convert from UTF-8 */
void cos_put_utf8(COStream s, const char* x) {
  /* Note that even if the file encoding is UTF-8, we need to convert
     to wide and back to UTF-8 again in order to maintain the correct
     column count by number of Unicode characters. */ 
  G_CHAR buf[1024];
  utf8_to_wide(x, buf, 1024);
  cos_puts(s, buf);
}

/* Write a null-terminated string of 8-bit characters. */
void cos_putss(COStream s, const char* x) {
  const char* p;
  for ( p = x ; ; p++ ) {
    unsigned char c = *p;
    if ( c == '\0' )
      break;
    cos_putch(s, c);
  }
}
#endif

/* Write null-terminated string followed by new line. */
void cos_put_line(COStream s, const char* x) {
  cos_putss(s, x);
  cos_putch(s, '\n');
  if ( is_file_stream(s) && !is_string_stream(s) )
    fflush(s->fs);
}

/* Avoids duplication of code for a common idiom. */
/* Equivalent to format "%c\"\n" except Unicode characters are supported.*/
void putch_end_quote(G_XCHAR_OR_EOF c)
{
  COStream es = get_stderr_stream();
  if ( c != G_EOF )
    cos_putch(es, c);
  cos_put_line(es, "\"");
}
 
/* write null-terminated string */
void cos_puts(COStream s, const G_SCHAR* x) {
  G_CHAR_COUNT len;
  if ( x == NULL )
    return;
  len = G_STRLEN(x);
  if ( len > 0 ) {
#if G_CHAR_BYTES > 1
    cos_put_len(s, x, len);
#else
    if ( is_string_stream(s) )
      cos_put_len(s, x, len);
    else {
      int n;
      const G_SCHAR* p;
      fputs(x,s->fs);
      p = x+len-1;
      s->lastch = *p;
      for ( n = 0 ; p >= x ; p-- ) {
        if ( *p == '\n' ) {
          s->column = 1;
          break;
        }
        else n++;
      }
      s->column += n;
    } /* end file stream */
#endif      
  } /* end len > 0 */
}

/* write arbitrary data */
void cos_put_len(COStream s, const G_SCHAR* x, G_CHAR_COUNT len) {
  if ( len > 0 ) {
    if ( is_string_stream(s) ) {
      if ( s->next + len >= s->bufend )
	expand_output_buffer ( s, len );
      memcpy( s->next, x, (len+1)*sizeof(G_CHAR) );
      s->next += len;
    }
    else {
      int n;
      const G_CHAR* p;
      p = x;
      for ( n = len ; n > 0 ; n-- )
        {
          G_XCHAR c = *p++;
#if G_CHAR_BYTES == 2
          if ( IS_HI_SUR(c) && IS_LO_SUR(*p) ) {
            c = COMBINE_SUR(c,*p++);
            n--;
          }
#endif
          cos_putch(s,c);
        }
    }
  }
}

/* return the last character written, or G_EOF if nothing written yet */
G_XCHAR_OR_EOF cos_prevch(COStream s) {
  if ( is_string_stream(s) )
    return s->next <= s->start ? G_EOF : s->next[-1];
  else return s->lastch;
}

G_CHAR_COUNT cos_column(COStream s) {
  if ( s == NULL )
    return 0;
  else if ( is_string_stream(s) ) {
    int n;
    const G_CHAR* p;
    n = 1;
    for ( p = s->next-1 ; p >= s->start ; p-- ) {
      if ( *p == '\n' )
	break;
      else n++;
    }
    return n;
  }
  else return s->column;
}

/* start new line if not already at start of line */
void cos_freshline( COStream s ) {
  G_XCHAR_OR_EOF lastch;
  lastch = cos_prevch(s);
  if ( lastch != '\n' && lastch != G_EOF )
    cos_putch(s,'\n');
}

#if G_CHAR_BYTES > 1
boolean bom_switch = TRUE;
/* Write a Byte Order Mark the first time we write to the file. */
static int write_with_bom(COStream s, G_XCHAR wch)
{
  /* This function replaces itself for next time. */
  s->writechar_function = s->file_encoding->write_function;
  if ( bom_switch ) {
    if ( s->writechar_function(s, BOM) == EOF )
      return EOF; /* error */
  }
  return s->writechar_function(s, wch);
}
#endif

/* create stream */
COStream make_file_output_stream(G_FILE* f, const G_SCHAR* path){
  register COStream s = allocate_output_stream();
  s->fs = f;
  s->pathname = g_str_dup(path);
  s->backup_path = NULL;
  s->lastch = G_EOF;
  s->column = 1;
  s->start = NULL;
  s->next = NULL;
  s->bufend = NULL;
  s->probe_before = '\0';
#if G_CHAR_BYTES > 1
  s->file_encoding = output_encoding;
  s->writechar_function =
    s->file_encoding->write_bom ? write_with_bom
    : s->file_encoding->write_function;
#endif
  return s;
}

static G_SCHAR backup_default[5] = { '.', 'b', 'a', 'k', 0 };
G_SCHAR* backup_suffix = backup_default;
  
static COStream current_output = NULL;

COStream
open_output_file( const G_CHAR* pathname, boolean binary )
{
  G_FILE* outfs;
  char output_status;
  boolean open_as_binary;
  G_SCHAR* current_backup = NULL;
  if ( pathname[0] == '-' && pathname[1] == '\0' )
    return stdout_stream;
  output_status = probe_pathname(pathname);
  if ( output_status == 'D' ) {
    /* avoid renaming a directory */
    input_error(input_stream, EXS_OUTPUT,
  	        "Output path \"%s\" is not a file.\n",
		message_string(pathname));
    if ( keep_going )
      return NULL;
    else
      exit(EXS_OUTPUT);
  }
  if ( (backup_suffix[0] != '\0') && (output_status == 'F') ) {
    /* save a backup of the previously existing output file */
    CIStream bakpath;
    COStream outbuf;
    const G_CHAR* backup_pathname;
    outbuf = make_buffer_output_stream();
#if defined(MSDOS) && !defined(_WIN32)
    /* on MS-DOS (8.3 filenames), replace any previous extension */
    merge_pathnames( outbuf, FALSE, NULL, pathname, backup_suffix);
#else
    /* on Unix or Win32, append ".bak" to the file name */
    cos_puts(outbuf,pathname);
    cos_puts(outbuf,backup_suffix);
#endif
    bakpath = convert_output_to_input( outbuf );
    backup_pathname = cis_whole_string(bakpath);
    if ( (G_REMOVE(backup_pathname) != 0) && (errno != ENOENT) ) {
      input_error(input_stream, EXS_OUTPUT, "Can't remove old backup file:\n");
      G_PERROR(backup_pathname);
    }
    else if ( G_RENAME(pathname,backup_pathname) == 0 ) {
      current_backup = g_str_dup(backup_pathname);
    }
    else {
      input_error(input_stream, EXS_OUTPUT,
                  "Can't rename old output file to \"%s\":\n",
                  message_string(backup_pathname));
      G_PERROR(pathname);
    }
    cis_close(bakpath);
    if ( current_backup == NULL ) { /* backup failed */
      if ( ! keep_going ) {
        exit(EXS_OUTPUT);
      } else {
        fprintf(stderr,
                "Continuing by discarding output intended for \"%s\".\n",
                message_string(pathname));
        return NULL;
      }
    } /* end failed */
  } /* end save a backup */
  open_as_binary = binary;
#if G_CHAR_BYTES > 1
  open_as_binary |= output_encoding->use_binary;
#endif
#if defined(_USE_WCHAR)
  outfs = G_FOPEN( pathname, open_as_binary? L"wb" : L"w", NULL, NULL );
#else
  outfs = G_FOPEN( pathname, open_as_binary? "wb" : "w", NULL, NULL );
#endif
  if ( outfs == NULL ) {
    input_error(input_stream, EXS_OUTPUT, "Can't open output file:\n");
    G_PERROR(pathname);
    if ( current_backup != NULL ) {
      free(current_backup);
      current_backup = NULL;
    }
    if ( ! keep_going ) {
      exit(EXS_OUTPUT);
    }
    return NULL;
  }
  else {
    current_output = make_file_output_stream(outfs,pathname);
    current_output->binary = binary;
    current_output->backup_path = current_backup;
    current_output->probe_before = output_status;
    return current_output;
  }
}
 
COStream make_buffer_output_stream(){
  register COStream s = allocate_output_stream();
  s->fs = NULL;
  s->pathname = NULL;
  s->backup_path = NULL;
  s->probe_before = '\0';
  s->lastch = G_EOF;
  s->column = 0;
  if ( free_buf_list != NULL ) {
    s->start = free_buf_list;
    free_buf_list = *(G_CHAR**)free_buf_list;
  }
  else
    s->start = (G_CHAR*) allocate( OutBufSize*sizeof(G_CHAR), MemoryOutputBuf );
  s->next = s->start;
  s->bufend = s->start + OutBufSize;
#if (G_CHAR_BYTES > 1) && !defined(NDEBUG)
  s->writechar_function = NULL; /* not used in this case */
#endif
  return s;
}

const G_SCHAR* cos_pathname(COStream s){
  return s==NULL? emptystring : s->pathname;
}

/* number of characters written so far to output buffer stream */
G_CHAR_COUNT cis_out_length( COStream s ){
  if ( is_file_stream(s) ) {
    /* This is actually number of bytes instead of number of characters, 
       but it doesn't matter because the result is only
       tested for zero or not zero. */
    return (unsigned)G_FTELL(s->fs);
  }
  else return s->next - s->start;
}

static void
free_buffer( G_CHAR* start, G_CHAR* bufend ) {
  if ( bufend != NULL ) {
    if ( (bufend - start) == OutBufSize ) {
      *(G_CHAR**)start = free_buf_list;
      free_buf_list = start;
    }
    else
      free( start );
  }
}

/* close stream */
void cos_close(COStream s){
  if ( s == NULL || s == stdout_stream )
    return;
  if ( is_file_stream(s) ) {
    G_FCLOSE(s->fs);
    s->fs = NULL;
  }
  if ( is_string_stream(s) ) {
    free_buffer(s->start, s->bufend);
    s->start = NULL;
    s->next = NULL;
  }
  if ( s->pathname != NULL ) {
      free((char*)s->pathname);
      s->pathname = NULL;
    }
  if ( s->backup_path != NULL ) {
    free((char*)s->backup_path);
    s->backup_path = NULL;
  }
  if ( s == current_output )
    current_output = NULL;
  free_output_stream(s);
}

CIStream convert_output_to_input( COStream out ) {
  size_t len, buflen;
  G_CHAR* buf;
  CIStream in;

  assert( is_string_stream(out) && !is_file_stream(out) );
  len = out->next - out->start;
  buflen = out->bufend - out->start;
  if ( free_buf_list == NULL && buflen == OutBufSize && len < buflen )
    buf = out->start;
  else {
    buflen = len+1;
    buf = realloc( out->start, buflen*sizeof(G_CHAR) ); /* release unused space */
    if (buf == NULL) { /* RD: To avoid warning from MS Visual Studio */
        fprintf(stderr, "Out of memory for stream. Aborting.\n");
        exit((int)EXS_MEM);
    }
  }
  buf[len] = '\0';
  in = make_string_input_stream(buf, len, FALSE);
  in->bufend = buf + buflen; /* cause free when input stream is closed */
  out->start = NULL;
  out->next = NULL;
  free_output_stream(out);
  return in;
}

void cos_copy_input_stream(COStream out, CIStream in) {
  if ( in != NULL ) {
    if ( is_file_stream(in) ) {
      G_XCHAR_OR_EOF ch;
      while ( (ch = cis_getch(in)) != G_EOF )
	cos_putch(out,ch);
    }
    else {
      cos_put_len(out, in->next, in->end - in->next);
      in->next = in->end;
    }
  }
}

/* When using same file for translation input and output,
   actually read the backup file instead. */
const G_CHAR*
get_backup_path(const G_CHAR* input_pathname, COStream output_stream)
{
  if ( output_stream->backup_path != NULL ) {
    input_pathname = output_stream->backup_path;
    /*  the pathname string will be copied in make_file_input_stream */
  }
  else if ( output_stream->probe_before == 'U' ) {
    /* We created an output file in place of a non-existent input file. */
    input_error(input_stream, EXS_INPUT,
                "Input file \"%.99s\" does not exist.\n",
                message_string(input_pathname));
    if ( output_stream->lastch == G_EOF ) {
      /* Have not yet written to the created output file,
         so delete it to avoid confusion later. */
      const G_CHAR* out_path = output_stream->pathname;
      /* Not using cos_close yet because it would free the pathname. */
      G_FCLOSE(output_stream->fs);
      output_stream->fs = NULL;
      if ( (G_REMOVE(out_path) != 0) && (errno != ENOENT) ) {
        input_error(input_stream, EXS_OUTPUT,
                    "Can't remove created output file:\n");
        G_PERROR(out_path);
      }
      cos_close(output_stream);
      output_stream = NULL;
    }
    exit(EXS_INPUT);
  }
  else {
    /* The file has already been opened for output, so the input has
       been lost.  It would be better to have detected this earlier. */
    input_error( input_stream, EXS_INPUT,
                 "input and output files same but no backup\n" );
    exit(EXS_INPUT);
  }
  return input_pathname;
}


#if G_CHAR_BYTES > 1

/* ============    Encoding options   ==============  */

static int cis_readchar_init(CIStream s);
static int cos_writechar_init(COStream s, G_XCHAR c);

static const struct encoding auto_encoding =
  /* name, read function, write function, write BOM, use binary mode */
  {"auto", cis_readchar_init, cos_writechar_init, FALSE, FALSE };
static const struct encoding ascii_encoding = 
  { "ASCII", cis_readchar_8bit, cos_writechar_ascii_only, FALSE, FALSE };
static const struct encoding eight_bit_encoding =
  { "8bit", cis_readchar_8bit, cos_writechar_8bit, FALSE, FALSE };
static const struct encoding utf8_encoding =
  { "UTF-8", cis_readchar_utf8, cos_writechar_utf8, TRUE, FALSE };
static const struct encoding utf16le_encoding =
  { "UTF-16LE", cis_readchar_16le, cos_writechar_16le, TRUE, TRUE };
static const struct encoding utf16be_encoding =
  { "UTF-16BE", cis_readchar_16be, cos_writechar_16be, TRUE, TRUE };

static Encoding encoding_table[] =
  { &auto_encoding, &ascii_encoding, &eight_bit_encoding,
    &utf8_encoding, &utf16le_encoding, &utf16be_encoding };

Encoding find_encoding(const char* str)
{
  int i;
  int n = sizeof(encoding_table)/sizeof(Encoding);
  for ( i = 0 ; i < n ; i++ ) {
    Encoding e = encoding_table[i];
    if ( stricmp(str, e->name) == 0 )
      return e;
  }
  return NULL;
}

void show_encodings()
{
  int i;
  int n = sizeof(encoding_table)/sizeof(Encoding);
  for ( i = 0 ; i < n ; i++ ) {
    Encoding e = encoding_table[i];
    fputs(e->name, stderr);
    fputc(' ', stderr);
  }
}

Encoding input_encoding  = &auto_encoding; /* default is automatic */
Encoding output_encoding = &auto_encoding; /* default is automatic */

/* The first time a character is read from the file, figure out 
   what kind of file encoding is being used. 
   Returns the first character as a wide character, if applicable.
   Returns EOF for end of file or read error. 
   Subsequent uses of readchar_function will use the appropriate version. */
static int cis_readchar_init(CIStream s)
{
  FILE* f = s->fs;
  int ch1, ch2;
  s->nraw = 0;
  ch2 = 0;
  ch1 = fgetc(f);
  if ( ch1 == EOF ) { /* empty file */
    s->readchar_function = cis_readchar_8bit;
    return EOF;
  }
  if ( ch1 == 0xEF ) {
    s->rawbuf[0] = (unsigned char)ch1;
    s->nraw = 1;
    ch2 = fgetc(f);
    if ( ch2 != EOF ) {
      s->rawbuf[1] = (unsigned char)ch2;
      s->nraw = 2;
      if ( ch2 == 0xBB ) {
        int ch3 = fgetc(f);
        if ( ch3 != EOF ) {
          s->rawbuf[2] = (unsigned char)ch3;
          s->nraw = 3;
          if ( ch3 == 0xBF ) {
            /* UTF-8 Byte Order Mark; discard it and continue reading */
            s->nraw = 0;
            s->n_utf8 = 1; /* really is UTF-8, not just guessing */
            s->file_encoding = &utf8_encoding;
            s->readchar_function = s->file_encoding->read_function;
            return s->readchar_function(s);
          }
        }
      }
    }
  }
  else if ( ch1 >= 0xFE ) {
    ch2 = fgetc(f);
    if ( ch1 == 0xFF && ch2 == 0xFE ) {
      /* Byte Order Mark in UTF-16 Little Endian */
      s->file_encoding = &utf16le_encoding;
      s->readchar_function = s->file_encoding->read_function;
      set_binary_input(s, TRUE); /* don't treat Control-Z as EOF */
      return s->readchar_function(s);
    }
    else if ( ch1 == 0xFE && ch2 == 0xFF ) {
      /* Byte Order Mark in UTF-16 Big Endian */
      s->file_encoding = &utf16be_encoding;
      s->readchar_function = s->file_encoding->read_function;
      set_binary_input(s, TRUE); /* don't treat Control-Z as EOF */
      return s->readchar_function(s);
    }
    s->rawbuf[0] = (unsigned char)ch1;
    s->rawbuf[1] = (unsigned char)ch2;
    s->nraw = 2;
  }
  else {
    s->rawbuf[0] = (unsigned char)ch1;
    s->nraw = 1;
  }
  
  if ( s->nraw < 2 && s->fs != stdin && ch2 != EOF) {
    ch2 = fgetc(f);
    s->rawbuf[1] = (unsigned char)ch2;
    s->nraw = 2;
  }

  if ( ch2 == EOF ) { /* file is only one byte long */
    s->nraw = 0;
    s->readchar_function = cis_readchar_8bit;
    return ch1;
  }

  /* default is either 8-bit or UTF-8 */
  s->file_encoding = &utf8_encoding;

  if ( s->fs != stdin ) { /* don't read ahead on pipe or console input */
    if ( ch1 <= 8 || ch2 < 8 ) { /* unlikely for 8-bit text */
      /* Might this be UTF-16 without a BOM? */
      /* Read ahead to get enough data to look for a pattern. */
      unsigned char buffer[128];
      unsigned int i, j, n;
      size_t nread;
      for ( j = 0 ; j < s->nraw ; j++ ) {
        buffer[j] = s->rawbuf[j];
      }
      nread = fread(buffer+s->nraw, sizeof(char), sizeof(buffer)-4, f);
      n = nread + s->nraw;
      if ( ((n&1) == 0) && (n > 20) ) { /* have enough data */
        unsigned char even_counts[256];
        unsigned char odd_counts[256];
        unsigned int unique_even = 0;
        unsigned int unique_odd = 0;
        memset(even_counts, 0, sizeof(even_counts));
        memset(odd_counts,  0, sizeof(odd_counts));
        for ( i = 0 ; i < n ; i += 2 ) {
          even_counts[buffer[i]]++;
          odd_counts[buffer[i+1]]++;
        }
        for ( i = 0 ; i < 256 ; i++ ) {
          if ( even_counts[i] > 0 )
            unique_even++;
          if ( odd_counts[i] > 0 )
            unique_odd++;
        }
        /* Heuristic tests */
        if ( unique_even*8 < unique_odd ||
             ( ((unsigned)even_counts[0])*2 >= n && odd_counts[0] == 0 ) ) {
          /* looks like UTF-16 Big Endian */
          s->file_encoding = &utf16be_encoding;
        }
        else if ( unique_odd*8 < unique_even  ||
                  ( ((unsigned)odd_counts[0])*2 >= n && even_counts[0] == 0 )) {
          /* looks like UTF-16 Little Endian */
          s->file_encoding = &utf16le_encoding;
        }
      } /* end have enough data */
      cis_rewind(s); /* start over from the beginning */
    } /* end unlikely 8-bit text */
  }   /* end not stdin */
  
  s->readchar_function = s->file_encoding->read_function;
  if ( s->file_encoding->use_binary )
    set_binary_input(s, FALSE);
  return s->readchar_function(s); /* will use buffered characters in rawbuf */
}

static int cos_writechar_maybe_utf8(COStream s, G_XCHAR wch) {
  if ( wch <= 0x7F ) { /* just ASCII so far */
    return fputc(wch, s->fs);
  } else {
    Encoding enc = &utf8_encoding;    
    if ( wch <= 0xFF ) {
      CIStream in = input_stream;
      if ( in != NULL ) {
        in->output_started = TRUE;
        if ( in->n_8bit > 0 && in->n_utf8 == 0 )
          /* Looks like we can get by with just 8-bit characters. */
          enc = &eight_bit_encoding;
      }
    }
    s->file_encoding = enc;
    s->writechar_function = enc->write_function;
    return s->writechar_function(s, wch);
  }
}

/* When automatic encoding is in effect, this is used the first time we
   write to the file to decide which encoding to use. */
static int cos_writechar_init(COStream s, G_XCHAR c) {
  /* Try to use the same encoding as the input file. */
  Encoding enc = &auto_encoding;
  boolean use_bom = FALSE; /* should we write a BOM to the file first? */
  CIStream in = input_stream;
  if ( in != NULL ) {
    in->output_started = TRUE;
    enc = in->file_encoding;
    if ( enc == &auto_encoding && in != stdin_stream ) {
      (void)cis_peek(in); /* force test for a BOM */
      enc = in->file_encoding;
    }
    if ( enc == &utf8_encoding ) {
      if ( in->n_utf8 > 0 || c > 0xFF ) {
        /* really do need UTF-8 encoding */
        use_bom = (in->n_8bit == 0);
      }
      else if ( in->n_8bit > 0 ) {
        /* try to get by with 8-bit characters, 
           but might need to switch to UTF-8 later */
        enc = &eight_bit_encoding;
        use_bom = FALSE;
      }
      else if ( c <= 0x7F ) { 
        s->file_encoding = enc;
        /* delay the decision for 8-bit or UTF-8 */
        s->writechar_function = cos_writechar_maybe_utf8;
        in->output_started = FALSE;
        assert ( !enc->use_binary );
        /* Finally, write the character. */
        return s->writechar_function(s, c);
      }
      else
        use_bom = FALSE;
    } /* end input using utf8_encoding */
    else
      use_bom = enc->write_bom;
  } /* end in != NULL */
  
  if ( enc == &auto_encoding ) {
    /* when no other information, tentatively default to UTF-8 */
    enc = &utf8_encoding;
    use_bom = FALSE;
  }

  s->file_encoding = enc;
  /* this function replaces itself for future calls */
  s->writechar_function = use_bom ? write_with_bom : enc->write_function;
  if ( enc->use_binary )
    set_binary_output(s); /* need to handle CR/LF mapping ourselves */
  
  /* Finally, write the character using the chosen encoding. */
  return s->writechar_function(s, c);
}

/* used by @encoding{} to query the encoding of the current input file */
const char* input_encoding_name()
{
  Encoding enc;
  CIStream in;
  in = input_stream;
  if ( (in != NULL) && is_file_stream(in) ) {
    enc = in->file_encoding;
    if ( (in->line | in->column) > 1 ) { /* reading has begun */
      if ( in->readchar_function == cis_readchar_utf8 &&
           in->peek_char == G_EOF && in->n_utf8 == 0 )
        /* were looking for UTF-8 encodings but didn't find any */
        enc = in->n_8bit > 0 ? &eight_bit_encoding : &ascii_encoding;
    }
  }
  else
    enc = input_encoding;
  return enc->name;
}

#endif /* end  G_CHAR_BYTES > 1 */

/* ============    standard streams   ==============  */

void init_stdio_streams(void) {
  G_FILE * in = NULL;
  G_FILE * out = NULL;

  in = G_FINIT(stdin, NULL, NULL);
  stdin_stream = make_file_input_stream(in, emptystring);

  out = G_FINIT(stdout, NULL, NULL);
  stdout_stream = make_file_output_stream(out, emptystring);

  stdin_stream ->binary = FALSE;
  stdout_stream->binary = FALSE;
  
#if G_CHAR_BYTES > 1
  stdin_stream->file_encoding = &auto_encoding;
  stdin_stream->readchar_function = cis_readchar_init;
  /* Assume stdout is always UTF-8 */
  stdout_stream->file_encoding = &utf8_encoding;
  stdout_stream->writechar_function = cos_writechar_utf8; /* without BOM */
#endif
}

#if defined(_WIN32) && (G_CHAR_BYTES > 1)
static int old_code_page = 0;
static void restore_cp(void) { SetConsoleOutputCP(old_code_page); }
#endif

COStream get_stderr_stream(void) {
  static COStream err_stream = NULL;
  if ( err_stream == NULL ) { /* create stream first time needed */
#if G_CHAR_BYTES > 1
    G_FILE* err = G_FINIT(stderr, NULL, NULL);
    err_stream = make_file_output_stream(err, L"stderr");
#ifdef _WIN32
    old_code_page = GetConsoleOutputCP();
    if ( old_code_page != CP_UTF8 ) {
      if ( old_code_page > 0 &&
           getenv("TERMCAP")==NULL /* not Emacs shell buffer */ &&
           SetConsoleOutputCP(CP_UTF8) != 0 ) {
        /* Temporarily changed the current command window to use UTF-8. */
        atexit(restore_cp); /* switch back at end of this process */
        err_stream->file_encoding = &utf8_encoding;
      }
      else {
        /* When in a Windows command window which doesn't support UTF-8,
           use \x or \u notation for non-ASCII characters. */
        err_stream->file_encoding = &ascii_encoding;
      }
    }
    else
#endif
    {
      err_stream->file_encoding = &utf8_encoding; /* UTF-8, no BOM */
    }
    err_stream->writechar_function = err_stream->file_encoding->write_function;
#else
    err_stream = make_file_output_stream(stderr,"stderr");
#endif
    err_stream->binary = FALSE;
  }
  return err_stream;
}

#if G_CHAR_BYTES > 1
boolean stderr_ascii()
{
  COStream err_stream = get_stderr_stream();
  return( err_stream->writechar_function == cos_writechar_ascii_only);
}
#endif

/* ============    optional unit test   ==============  */
 
#ifdef TEST_CSTREAM

/* otherwise defined in gema.c */
COStream output_stream;

boolean keep_going = FALSE;
boolean binary = FALSE;
Exit_States exit_status = EXS_OK;

/* otherwise defined in match.c */
CIStream input_stream = NULL;

int
main(int argc, char ** argv)
{
  int rc = 0;

  char * usage_fmt = "Usage: %s [-i input] [-o output]\n";

  char * infile = "-";
  char * outfile = "-";

  CIStream input;
  COStream output;

  {
    int i = 1;
    char * opt = NULL;

    for ( i=1; i<argc && argv[i][0]=='-'; i++ ) {
      opt = argv[i];
      if ( strcmp(opt, "-i") == 0 ) {
	infile = argv[++i];
      } else if ( strcmp(opt, "-o") == 0 ) {
	outfile = argv[++i];
      } else {
	printf(usage_fmt, argv[0]);
	exit(1);
      }
    }
  }
  init_stdio_streams();
  input = open_input_file(infile, FALSE);
  output = open_output_file(outfile, FALSE);

  cos_copy_input_stream(output, input);

  cis_close(input);
  cos_close(output);

  exit(rc);
}

#endif /* TEST_CSTREAM */
