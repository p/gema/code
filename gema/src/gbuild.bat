:: .Title Gema build - Windows batch

@echo off

set CC=cl /nologo /DNDBEUG /DTRACE /D_CRT_SECURE_NO_WARNINGS /D_USE_WCHAR /O2 /W2 /c
set LN=link /nologo /subsystem:console /incremental:no /out:
set AR=link -lib /nologo /out:

:: echo on

%CC% version.c
%CC% var.c
%CC% util.c
%CC% reg-expr.c
%CC% read.c
%CC% match.c
%CC% gema_glob.c
%CC% gema.c
%CC% cstream.c
%CC% action.c

%LN%gema.exe gema.obj version.obj var.obj util.obj reg-expr.obj read.obj match.obj gema_glob.obj cstream.obj action.obj
