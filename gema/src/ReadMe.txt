
This is the source code for gema version 2.

What is now the "master" branch was originally developed as the
"Unicode-dev" branch, which started from merging Tod Olson's unfinished
changes (from branch "Tod-Unicode") with version 1.5 (git tag "v1-5").
The "v2-0" tag corresponds to the official 2.0 release.

The configuration using wchar_t internally, selected by defining
_USE_WCHAR (tested in "type_macros.h"), has been tested on Windows,
Linux, Macintosh, and Solaris to provide the intended functionality.
The original 8-bit character functionality can still be obtained by
building with _USE_STDIO, but there no longer seems to be any reason
to do that. 
I have not been updating or testing Tod's _USE_ICU configuration
(using the ICU library), which would need a lot more work.

I have not been updating any of the code specific to "gel" (in
"gel_bind.c", and under "#ifdef LUA").  It is unchanged since
Remo's 1.4.1 release (branch v-1-4-1).

While gema originally needed only C89 (AKA C90), the _USE_WCHAR
configuration needs some library functions from C95.

For an explanation of the new functionality in 2.0 since 1.x, see:
https://gema.sourceforge.net/new/unicode-intro.html

  -- David N. Gray <DGray@acm.org>

