
/* read pattern definitions */

/*********************************************************************
  This file is part of "gema", the general-purpose macro translator,
  written by David N. Gray <dgray@acm.org> in 1994 and 1995.
  Extended for Unicode support in 2023.
  You may do whatever you like with this, so long as you retain
  an acknowledgment of the original source.
 *********************************************************************/

#if defined(_QC) || defined(_MSC_VER) /* Microsoft C or Quick C */
#pragma check_stack(off)
#endif

/* suppress Microsoft warnings about "insecure" string functions */
#define _CRT_SECURE_NO_WARNINGS 1
/* suppress Microsoft warnings about POSIX functions */
#define _CRT_NONSTDC_NO_WARNINGS 1

#include "pattern.h"
#include "util.h"
#include "patimp.h"
#include <ctype.h>  /* for toupper */
#include "ctype_macros.h"  /* for isspace etc. */
#include <string.h>
#include <assert.h>
#include "reg-expr.h"
#include "main.h"  /* for EXS_SYNTAX */
#include "var.h"	/* for get_var */

boolean line_mode = FALSE;
boolean token_mode = FALSE;

boolean discard_unmatched = FALSE;

boolean debug_switch = FALSE;

enum char_kinds {
  PI_LITERAL,	/* character represents itself */
  PI_LIT_CTRL,	/* control character that represents itself but has
		  the same code as a pattern operator */
  PI_CR,	/* Carriage Return */
  PI_ARG,	/* template argument -- wild card match */
  PI_1ARG,	/* template argument -- match any one character */
  PI_RARG,	/* template argument with recursive translation */
  PI_SEP,	/* separates template and action */
  PI_PUT,	/* outputs an argument or variable in an action */
  PI_QUOTE,	/* take next character literally */
  PI_ESC,	/* escape character */
  PI_CTRL,	/* combines with next character for ASCII control */
  PI_SPACE,	/* matches one or more white space characters */
  PI_BEGIN_ARG,	/* begin argument list */
  PI_ARG_SEP,	/* separates arguments of action operators */
  PI_END_ARG,	/* end argument list */
  PI_END,	/* end of pattern */
  PI_DOMAIN,	/* terminates domain name preceding pattern */
  PI_BEGIN_DOMAIN_ARG,
  PI_END_DOMAIN_ARG,
  PI_END_REGEXP,
  PI_BEGIN_REGEXP,
  PI_CHAR_OP, 	/* makes following character special */
  PI_OP,	/* introduces named command */
  PI_COMMENT,	/* rest of line is a comment */
  PI_ABBREV_DOMAIN, /* single-character recursive argument domain */
  PI_IGNORE,	/* character that is completely ignored */
  PI_IGNORED_SPACE, /* ignored unless needed as delimiter */
  PI_QUOTE_STRING, /* take characters literally until matching quote */
  PI_EOF,	/* end of file */
  Num_Char_Kinds	/* must be last element of enumeration */
};

#if G_CHAR_BYTES == 1
/* uses an array of all possible 8-bit characters */
#define NUMCHAR 256
#else
/* for wide characters, use array for ASCII, and extension table for rest */
#define NUMCHAR 128
#endif
static unsigned char default_syntax_chars[Num_Char_Kinds+1] =
		 ".\1\r*?#=$\\\\^ {;};:<>//@@!\0\0\0\0\0";
static G_XCHAR syntax_chars[Num_Char_Kinds+1];

static char char_table[NUMCHAR+1] = {
#if defined(_USE_STDIO) && (EOF == (-1))
 /* -1: */ PI_EOF,
#endif
 /* 00: */ PI_LIT_CTRL,
 /* 01: */ PI_LIT_CTRL,
 /* 02: */ PI_LIT_CTRL,
 /* 03: */ PI_LIT_CTRL,
 /* 04: */ PI_LIT_CTRL,
 /* 05: */ PI_LIT_CTRL,
 /* 06: */ PI_LIT_CTRL,
 /* 07: */ PI_LIT_CTRL,
 /* 08: */ PI_LITERAL, /* ASCII BS */
 /* 09: */ PI_LITERAL, /* ASCII HT */
 /* 0A: */ PI_LITERAL, /* ASCII LF */
 /* 0B: */ PI_LITERAL,  /* VT */
 /* 0C: */ PI_LITERAL,  /* FF */
#if defined(MSDOS) || '\r' == '\n'
 /* 0D: */ PI_LITERAL,  /* CR */
#else
 /* 0D: */ PI_CR,  	/* CR */
#endif
 /* 0E: */ PI_LIT_CTRL,
 /* 0F: */ PI_LIT_CTRL,
 /* 10: */ PI_LIT_CTRL,
 /* 11: */ PI_LIT_CTRL,
 /* 12: */ PI_LIT_CTRL,
 /* 13: */ PI_LIT_CTRL,
 /* 14: */ PI_LIT_CTRL,
 /* 15: */ PI_LITERAL,  /* EBCDIC NL */
 /* 16: */ PI_LIT_CTRL,
 /* 17: */ PI_LIT_CTRL,
 /* 18: */ PI_LIT_CTRL,
 /* 19: */ PI_LIT_CTRL,
 /* 1A: */ PI_LIT_CTRL,
 /* 1B: */ PI_LITERAL,  /* ASCII ESC */
 /* 1C: */ PI_LIT_CTRL,
 /* 1D: */ PI_LIT_CTRL,
 /* 1E: */ PI_LIT_CTRL,
 /* 1F: */ PI_LIT_CTRL,
 /* 20: */ PI_LITERAL };

#if G_CHAR_BYTES > 1  /* if wide characters are supported */
struct extended_syntax_char {
  G_XCHAR xchar;
  enum char_kinds kind;
  struct extended_syntax_char* next;
};
#define NUMX 32
static struct extended_syntax_char* extended_char_table[NUMX];

static enum char_kinds
char_kind(G_XCHAR_OR_EOF ch){
  if (  ch == G_EOF )
    return PI_EOF;
  if ( ((unsigned)ch) >= NUMCHAR ) {
    unsigned int key;
    struct extended_syntax_char* zp;
    assert( (NUMX & (NUMX-1)) == 0 ); /* power of 2 */
    key = ((unsigned int)ch) & (NUMX-1);
    for ( zp = extended_char_table[key] ; zp != NULL ; zp = zp->next ) {
      if ( zp->xchar == ch )
        return zp->kind;
    }
    return PI_LITERAL;
  }
  return (enum char_kinds) char_table[ch];
}

static void set_char_kind(G_XCHAR ch, enum char_kinds k)
{
  if ( ((unsigned)ch) < NUMCHAR )
    char_table[ch] = k;
  else {
    unsigned int key = ((unsigned int)ch) & (NUMX-1);
    struct extended_syntax_char* zp;
    struct extended_syntax_char* zp1 = extended_char_table[key];
    for ( zp = zp1 ; zp != NULL ; zp = zp->next ) {
      if ( zp->xchar == ch ) {
        zp->kind = k;
        return;
      }
    } /* end for zp */
    zp = (struct extended_syntax_char*)
      allocate(sizeof(struct extended_syntax_char), MemoryDispatch);
    zp->xchar = ch;
    zp->kind = k;
    zp->next = zp1;
    extended_char_table[key] = zp;
  }
}
#else /* end Unicode version; begin single byte version */

#if defined(_USE_STDIO) && (EOF == (-1))
#define char_kind(ch) ((enum char_kinds)((char_table+1)[ch]))
#define set_char_kind(ch,k) char_table[(ch)+1] = (k)
#else
/* I don't know of any implementation where EOF is not -1, but the ANSI
   standard does not require it. */
static enum char_kinds
char_kind(G_XCHAR_OR_EOF ch){
  if (  ch == G_EOF )
    return PI_EOF;
  if ( ch >= NUMCHAR )
    return PI_LITERAL;
  return (enum char_kinds) char_table[ch];
}
#define set_char_kind(ch,k) char_table[ch] = (k)
#endif
#endif /* end single byte version */

boolean is_operator(G_XCHAR_OR_EOF x) {
  return char_kind(x) == PI_LIT_CTRL;
}

static enum char_kinds
default_char_kind( G_XCHAR_OR_EOF pc ) {
  const char* x;
      if ( pc > 0xFF )
        return PI_LITERAL;
      x = strrchr((const char*)default_syntax_chars,(char)pc);
      if ( x == NULL )
	return PI_LITERAL;
      else
	return (enum char_kinds)(x - (const char*)default_syntax_chars);
}

boolean set_syntax( int type0, const G_CHAR* char_set ) {
  enum char_kinds k;
  const G_CHAR* s;
  int type = ((type0 <= max_ctype_char) ? toupper(type0) : type0);
  switch(type) {
    case 'L': k = PI_LITERAL; break;
    case 'Q': k = PI_QUOTE; break;
    case 'M': k = PI_QUOTE_STRING; break;
    case 'E': k = PI_ESC; break;
    case 'C': k = PI_COMMENT; break;
    case 'A': k = PI_ARG_SEP; break;
    case 'T': k = PI_END; break;
    case 'F': k = PI_OP; break;
    case 'I': k = PI_IGNORE; break;
    case 'S': k = PI_IGNORED_SPACE; break;
    case 'D': k = PI_ABBREV_DOMAIN; break;
    case 'K': k = PI_CHAR_OP; break;
    default:
       k = char_kind(type);
       if ( k <= PI_LIT_CTRL ) {
	 k = default_char_kind(type);
	   if ( k <= PI_LIT_CTRL )
	     return FALSE;
       }
       break;
  } /* end switch */
  for ( s = char_set ; s[0] != '\0' ; ) {
    G_XCHAR ch = NEXT_XCHAR(s);
    enum char_kinds oldk = char_kind(ch);
    G_XCHAR* scp = &syntax_chars[oldk];
    G_XCHAR sc = *scp;
    if ( sc == ch
         /* This is ugly; should be a cleaner way to handle characters
            with double meanings. */
         && !( (oldk == PI_QUOTE && k == PI_ESC) ||
               (oldk == PI_ESC && k == PI_QUOTE)) )
      *scp = '\0'; /* ch doesn't mean what it used to */
    set_char_kind(ch,k);
    if ( (k == PI_BEGIN_REGEXP) &&
         (syntax_chars[PI_END_REGEXP] == syntax_chars[PI_BEGIN_REGEXP]) )
      syntax_chars[PI_END_REGEXP] = ch;
    if (k < Num_Char_Kinds) /* RD: To suppress MS Visual Studio Warning.*/
      syntax_chars[k] = ch;
  }
  return TRUE;
}

#if 0 /* old way */
/* special characters in external pattern definitions: */
#define PI_ARG '*'      /* template argument -- wild card match */
#define PI_1ARG '?'     /* template argument -- match any one character */
#define PI_RARG '#'     /* template argument with recursive translation */
#define PI_SEP '='      /* separates template and action */
#define PI_PUT '$'      /* outputs an argument or variable in an action */
#define PI_QUOTE '\\'   /* take next character literally */
#define PI_ESC '\\'     /* escape character */
#define PI_CTRL '^'     /* combines with next character for ASCII control */
#define PI_SPACE ' '    /* matches one or more white space characters */
#define PI_END '\n'     /* end of pattern */
#define PI_ALTEND ';'   /* end of pattern */
#define PI_BEGIN_ARG '{' /* begin argument list */
#define PI_ARG_SEP ';'  /* separates arguments of action operators */
#define PI_END_ARG '}'  /* end argument list */
#define PI_DOMAIN ':'   /* terminates domain name preceding pattern */
#define PI_BEGIN_DOMAIN_ARG '<'
#define PI_END_DOMAIN_ARG '>'
#define PI_BEGIN_REGEXP '/'
#define PI_END_REGEXP '/'
#define PI_OP '@'       /* introduces named command */
#define PI_COMMENT '!'  /* rest of line is a comment */
#endif

int ndomains = 0;
#if G_CHAR_BYTES == 1
 #if MAX_DOMAINS >= (1<<14)
  #error "MAX_DOMAINS is too large; must fit in 14 bits."
 #endif
#elif G_CHAR_BYTES == 2
 #if MAX_DOMAINS >= (1<<16)
  #error "MAX_DOMAINS is too large; must fit in 16 bits."
 #endif
#endif
Domain domains[MAX_DOMAINS] = { NULL };

static G_SCHAR* trim_name( G_CHAR* x ) {
  G_CHAR* s;
  G_CHAR* end;
  s = x;
  while ( (s[0] <= max_ctype_char && G_IS(space)(s[0]))
          || s[0] == PT_SPACE || s[0] == PT_ID_DELIM )
    s++;
  if ( s[0] != PT_RECUR ) {
    end = s + G_STRLEN((const G_SCHAR*)s);
    while ( end > s &&
            ( (end[-1] <= max_ctype_char && G_IS(space)(end[-1]))
               || end[-1] == PT_SPACE || end[-1] == PT_ID_DELIM ) )
      end--;
    end[0] = '\0';
  }
  return (G_SCHAR*)s;
}

static int find_domain( const G_SCHAR* name ) {
  int i;
  for ( i = ndomains ; i > 0 ; ) {
    i--;
    if ( case_insensitive ) {
      if( G_STRICMP(name, domain_name(i)) == 0 )
	return i;
    } else if ( G_STRCMP(name, domain_name(i)) == 0 )
	return i;
  }
  if ( ndomains >= MAX_DOMAINS ) {
    fprintf(stderr,"More than %d domain names; aborting.\n", MAX_DOMAINS);
    exit((int)EXS_SYNTAX);
    return -1; /* just to avoid warning from SGI compiler */
  }
  else {
    Patterns p;
    Domain dp;
    i = ndomains++;
    dp = (Domain)allocate(sizeof(struct domain_struct), MemoryPatterns);
    p = &dp->patterns;
    domains[i] = dp;
    dp->name = g_str_dup(name);
    dp->inherits = NULL;
    p->head = NULL;
    p->tail = NULL;
    p->dispatch = NULL;
    dp->init_and_final_patterns = NULL;
    return i;
  }
}

Domain get_domain( const G_SCHAR* name ) {
  return domains[ find_domain(name) ];
}

#ifdef LUA
void
#else
static void
#endif
delete_pattern ( Pattern p ) {
  if ( ! p->is_copy ) {
    free((char*)p->pattern);
    free((char*)p->action);
  }
  free(p);
}

#ifdef LUA
void
#else
static void
#endif
delete_patterns ( Patterns ps ) {
  Pattern p;
  Pattern n;
  if ( ps->dispatch != NULL ) {
    int i;
    Patterns xp;
    for ( i = 0 ; i < DISPATCH_SIZE ; i++ ) {
      xp = ps->dispatch[i];
      if ( xp != NULL ) {
        delete_patterns(xp);
        free(xp);
      }
    }
    free(ps->dispatch);
    ps->dispatch = NULL;
  }
  for ( p = ps->head ; p != NULL ; ) {
    n = p->next;
    delete_pattern(p);
    p = n;
  }
  ps->head = NULL;
  ps->tail = NULL;
}

void delete_domain(int n) {
  Domain dp;
  assert( n < ndomains );
  dp = domains[n];
  delete_patterns( &dp->patterns);
  while ( dp->init_and_final_patterns != NULL ) {
    Pattern p = dp->init_and_final_patterns;
    dp->init_and_final_patterns = p->next;
    delete_pattern(p);
  }
  if ( n == ndomains-1 ) {
    ndomains--;
    free((char*)dp->name);
    free(dp);
  }
}

void quoted_copy( CIStream in, COStream out ) {
  /* copy the input stream to the output stream, quoting any
     characters that have special meaning in patterns. */
  G_XCHAR_OR_EOF qc;
  G_XCHAR quote = syntax_chars[PI_QUOTE];
  for ( ; ; ) {
  	qc = cis_getch(in);
  	if ( char_kind(qc) != PI_LITERAL ||
	     default_char_kind(qc) != PI_LITERAL ) {
	  if ( qc == G_EOF )
	    break;
  	  cos_putch(out, quote);
	  if ( qc == '\n' )
	    qc = 'n';
	}
  	cos_putch(out, qc);
  }
}

#if G_CHAR_BYTES > 1
void escaped_copy( CIStream in, COStream out ) {
  /* copy the input stream to the output stream, converting any
     control characters or characters beyond ASCII to ASCII escape
     sequences for human readability */
  for ( ; ; ) {
    char buf[20];  
    G_XCHAR_OR_EOF qc;
    qc = cis_getch(in);
    if ( qc == G_EOF )
      break;
    if ( qc > 0xFF ) {
      G_XCHAR_OR_EOF pk = cis_peek(in);
      if ( (pk < 0x7F) && isalnum(pk) ) {
        /* Use braces to avoid ambiguity with following character. */
        sprintf(buf, "\\u{%04X}", (unsigned int)qc);
        cos_putss(out, buf);
        continue;
      }
    }
    if ( qc < 0x20 || qc > 0xFF ) {
      char* endp = visualize_char(buf, qc);
      *endp = '\0';
      cos_putss(out, buf);
      continue;
    }
    else if ( qc >= 0x7F ) {
      sprintf(buf, "\\x%02X", (unsigned int)qc);
      cos_putss(out, buf);
      continue;
    }
    if ( char_kind(qc) != PI_LITERAL ||
         default_char_kind(qc) != PI_LITERAL ) {
      cos_putch(out, '\\');
    }
    cos_putch(out, qc);
  }
}
#endif

const char* safe_string(const G_CHAR* s) {
  if ( s == NULL )
    return "";
  return message_string(s);
}

static void
describe_character(enum char_kinds chartype, const char* description ) {
  int ch = (int)syntax_chars[(int)chartype];
  if ( ch != '\0' ) {
#if G_CHAR_BYTES > 1
    fputs("\t'", stderr);
    cos_putch(get_stderr_stream(), ch);
    fprintf(stderr, "' %s\n", description);
#else
    fprintf(stderr, "\t'%c' %s\n", ch, description );
#endif
  }
}

void pattern_help( FILE* f ) {
  int i;
  COStream es = get_stderr_stream();
  assert( f == stderr );
  cos_putss(es, "Pattern syntax:\n\t<template>");
  cos_putch(es, syntax_chars[PI_SEP]);
  cos_putss(es, "<replacement>\n");
  
  cos_putss(es, "Text matching <template> is replaced by <replacement>.\n");

  cos_putss(es, "Patterns are separated by a newline or '");
  cos_putch(es, syntax_chars[PI_END]);
  cos_putss(es, "'.\n");
	    
  fputs("Special characters within a template:\n", f);
  describe_character( PI_ARG,  "argument - match any number of characters" );
  describe_character( PI_1ARG, "argument - match any one character" );
  describe_character( PI_RARG, "argument, recursively translated" );
  describe_character( PI_BEGIN_REGEXP, "regular expression delimiter" );
  fputs("Special characters within the replacement:\n", f );
  describe_character( PI_PUT, "followed by digit, insert numbered argument" );
  describe_character( PI_OP, "prefixes name of function to call" );
  fputs("Within both template and replacement:\n", f);
  describe_character( PI_ESC, "is an escape character." );
  describe_character( PI_CTRL, "adds the Control key to the following character" );
  fputs( "Following are all of the characters with special meaning:\n\t", f);
  for ( i = 0 ; i < NUMCHAR ; i++ ) {
    enum char_kinds kind = char_kind(i);
    if ( kind > PI_CR && isgraph(i) ) {
      fputc( i, f );
      fputc( ' ', f );
    }
  }
#if G_CHAR_BYTES > 1
    for ( i = 0 ; i < NUMX ; i++ ) {
      struct extended_syntax_char* zp;
      for ( zp = extended_char_table[i] ; zp != NULL ; zp = zp->next ) {
        if ( zp->kind > PI_CR ) {
          cos_putch(es, zp->xchar);
          cos_putch(es, ' ');
        }
      }
    }
#endif
  fputs( "\nSee the man page for further details."
         "  More at http://gema.sourceforge.net\n", f );
}

void
skip_whitespace( CIStream s ) {
  while ( G_IS(space)(cis_peek_short(s)) )
    (void)cis_getch(s);
}

/* return the result of combining the Crtl key with the given character key */
static G_CHAR control_mod(G_XCHAR_OR_EOF xc, CIStream s)
{
  if ( xc < 0x40 || xc > 'z' ) {
    COStream es = get_stderr_stream();
    input_error(s, EXS_SYNTAX, "Invalid control character:  \"");
    cos_putch(es, syntax_chars[PI_CTRL]);
    putch_end_quote(xc);
    return xc;
  }
  else
    return toupper(xc) ^ 0x40;
}

static G_CHAR*
escaped_char( int ch, G_CHAR* bp, CIStream s ) {
      G_XCHAR_OR_EOF nc;
      G_XCHAR_OR_EOF pc=0; /* RD 20040117 Added initialization */
      unsigned int errch;  /* escape character for use in error messages */
#if G_CHAR_BYTES > 1  /* if wide characters are supported */
      errch = (ch < 0x7F ? ch : default_syntax_chars[PI_ESC]);
#else
      errch = ch;
#endif
      nc = cis_getch(s);
      if ( syntax_chars[PI_ESC] != ch && char_kind(ch) == PI_QUOTE )
	pc = nc;
      else
      switch(nc) {
	/* control characters */
#ifndef MSDOS
#if '\r' != '\n'
	case '\r':
	  if ( cis_peek(s) != '\n' ) {
	    pc = nc;
	    break;
	  }
	  /* else fall-through to ignore redundant CR in MS-DOS files */
#endif
#endif
	case '\n': /* ignore new line and leading space on next line */
	  	skip_whitespace(s);
      		return bp;
	case 'n': pc = '\n'; break;
	case 't': pc = '\t'; break;
	case 'a': pc = '\a'; break;
	case 'b': pc = '\b'; break;
	case 'f': pc = '\f'; break;
	case 'r': pc = '\r'; break;
	case 'v': pc = '\v'; break;
	case 's': pc = ' '; break;
#if 'A' == 0x41
	case 'e': pc = ((char)0x1B) ; break; /* ASCII Escape */
	case 'd': pc = ((char)0x7F) ; break; /* ASCII Delete */
	case 'c': {	/* control */
	  pc = control_mod(cis_getch(s), s);
	  break;
	}
#elif 'A' == 0xC1
	case 'e': pc = ((char)0x27) ; break; /* EBCDIC Escape */
	case 'd': pc = ((char)0x07) ; break; /* EBCDIC Delete */
#endif
		/* the following two are the same in ASCI and EBCDIC */
	case 'o': pc = ((char)0x0E) ; break; /* shift out */
	case 'i': pc = ((char)0x0F) ; break; /* shift in */
	
	case 'x':
#if G_CHAR_BYTES > 1
	 if ( cis_peek(s) != '{' )
#endif
	 {
	  G_SCHAR cbuf[4];
	  G_SCHAR* endp;
	  cbuf[0] = (G_SCHAR)cis_getch(s);
	  cbuf[1] = (G_SCHAR)cis_getch(s);
	  cbuf[2] = '\0';
	  pc = (int)G_STRTOL(cbuf,&endp,16);
	  if ( *endp != '\0' )
	    input_error(s, EXS_SYNTAX,
			"Invalid escape sequence in pattern:  \"%cx%s\"\n",
			errch, message_string(cbuf));
	  break;
	 } /* else fall through for \x{...} same as \u{...} */
#if G_CHAR_BYTES > 1  /* if wide characters are supported */
      case 'u': { /* Unicode character */
          G_SCHAR cbuf[16];
          G_SCHAR* endp;
          G_XCHAR_OR_EOF xc;
          int i;
          xc = (G_SCHAR)cis_getch(s);
          if ( xc == '{' || xc == syntax_chars[PI_BEGIN_ARG] ) {
            for ( i = 0 ; ; ) { /* collect characters until matching } */
              xc = cis_getch(s);
              if ( xc == '}' || xc == syntax_chars[PI_END_ARG] )
                break;
              if ( xc == '\n' || xc == G_EOF ||
                   i >= (sizeof(cbuf)/sizeof(G_SCHAR)-2) ) {
                cbuf[i] = G_NULL_CHAR;
                input_error(s, EXS_SYNTAX,
                            "No closing brace for %c%c{%s\n",
                            errch, nc, message_string(cbuf));
                break;
              }
              cbuf[i++] = (G_SCHAR)xc;
            }
          }
          else {
            cbuf[0] = (G_SCHAR)xc;  /* needs at least one hex digit */
            for ( i = 1 ; i < 8 ; ) {  /* allows up to 7 more */
              if ( G_IS(xdigit)(cis_peek_short(s)) )
                cbuf[i++] = (G_SCHAR)cis_getch(s);
              else break;
            }
          }
          cbuf[i] = G_NULL_CHAR;
          pc = (int)G_STRTOL(cbuf,&endp,16);
          if ( *endp != G_NULL_CHAR )
            input_error(s, EXS_SYNTAX,
                        "Invalid escape sequence in pattern:  \"%c%c%s\"\n",
                        errch, nc, message_string(cbuf));
          else if ( (unsigned)pc > 0x10FFFF ) {
            input_error(s, EXS_SYNTAX,
                        "Out of range Unicode character %c%c%06X\n", 
                        errch, nc, pc);
#if G_CHAR_BYTES > 2
            if ( (unsigned)pc > 0x1FFFFF ) /* not representable by UTF-8 */
#endif          /* else not representable by UTF-16 in string of G_CHAR */
              pc = 0xFFFD; /* replacement character */
          }
          break;
        }
#endif
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	  pc = nc - '0';
	  while ( G_IS(digit)(cis_peek_short(s)) )
	    pc = (pc << 3) + cis_getch(s) - '0';
	  break;

	/* template match operators */
	case 'S': pc = PT_SPACE; goto store;
	case 'W': pc = PT_SKIP_WHITE_SPACE; goto store;
	case 'I': pc = PT_ID_DELIM ; goto store;
	case 'X': pc = PT_WORD_DELIM ; goto store;
#if 0		/* changed my mind */
	case 'V': pc = PT_ARG_DELIM ; goto store;
#endif
	case 'N': pc = PT_LINE ; goto store;
	case 'B': pc = PTX_BEGIN_FILE ; goto aux;
	case 'E': pc = PTX_END_FILE ; goto aux;
	case 'L': pc = PTX_ONE_LINE ; goto aux;
	case 'C': pc = PTX_NO_CASE ; goto aux;
	case 'A': pc = PTX_INIT;  goto aux;
	case 'Z': pc = PTX_FINAL; goto aux;
	case 'P': pc = PTX_POSITION; goto aux;
	case 'G': pc = PTX_NO_GOAL; goto aux;
	case 'J': pc = PTX_JOIN; goto aux;

#ifdef LUA
	case 'M': pc = PTX_MARK;
	case 'Q': pc = (pc?pc:PTX_COND);
	case 'q': pc = (pc?pc:PTX_NCOND);
	          if (G_IS(digit)(cis_peek_short(s))) 
	            pc = pc + cis_getch(s) - '0';
	          /*fprintf(stderr,"AUX: %c, %x\n",nc,pc);*/
	          goto aux;
#endif
	/* quoting literal character */
	default:
	  if ( (nc < 0x7F && isalnum(nc)) ||
	       ( ch != '\\' && ch != syntax_chars[PI_QUOTE]
		 && char_kind(ch) != PI_QUOTE && nc != ch ) )
	    input_error(s, EXS_SYNTAX,
		"Invalid escape sequence in pattern:  \"%c%c\"\n",
		errch, nc);
	  pc = nc;
	  break;
      } /* end switch */
    if ( is_operator(pc) )
      *bp++ = PT_QUOTE;
    goto store;
aux:
    *bp++ = PT_AUX;
store:
#if G_CHAR_BYTES == 2
    if ( pc > 0xFFFF ) {
      *bp++ = HISUR(pc);
      pc = LOSUR(pc);
    }
#endif
    *bp = pc;
    return bp+1;
}

struct regex_struct {
	const G_SCHAR* source;
	unsigned char* compiled;
};

static struct regex_struct * regex_table = NULL;

static int last_regex = -1;

#define MAX_NUM_REGEXP 255

int intern_regexp( G_CHAR* exp, CIStream in ) {
  int i;
  if ( regex_table == NULL )
    regex_table = (struct regex_struct*)
		allocate( MAX_NUM_REGEXP * sizeof(struct regex_struct*),
		MemoryRegexp);
  for ( i = last_regex ; i >= 0 ; i-- )
    if ( G_STRCMP( (const G_SCHAR*)exp, regex_table[i].source )==0 )
      return i;
  if ( last_regex >= (MAX_NUM_REGEXP-1) ) {
    input_error(in, EXS_SYNTAX, "More than %d unique regular expressions.\n",
		MAX_NUM_REGEXP);
    exit(exit_status);
    return -1; /* just to avoid warning from SGI compiler */
  }
  else {
    struct regex_struct* p;
    const char* msg;
    void *tmpptr;
    size_t bufsize;
    bufsize = 200;
    p = regex_table + ++last_regex;
    p->source = g_str_dup(exp);
    p->compiled = allocate ( bufsize, MemoryRegexp);
    for ( ; ; ) {
      msg = regexp_comp(exp, p->compiled, bufsize);
      if ( msg == NULL ) /* OK */
	break;
      else if ( msg == regexp_dfa_buffer_too_short ) {
	bufsize = bufsize * 2;
	tmpptr = realloc(p->compiled, bufsize);
	/* RD: To avoid MS Visual Studio warning */
	if ( tmpptr == NULL ) {
	  input_error(in, EXS_MEM, "Out of memory for regular expression.\n");
	  exit(exit_status);
	}
	p->compiled = tmpptr;
	continue;
      }
      else {
	static G_CHAR x1[2] = { '\1', G_NULL_CHAR };
	input_error(in, EXS_SYNTAX, "Error in regular expression: %s\n", msg);
	if ( ! keep_going ) {
	  exit(exit_status);
	}
	return intern_regexp( x1, in );
      }
    }
    return last_regex;
  }
}

const G_CHAR*
regex_match(int regex_num, const G_CHAR* text, boolean start_of_line) {
  /* if the regular expression interned as number `regex_num' matches the
     beginning of `text', return the position of the end of the match,
     else return NULL. */
  boolean match;
  assert( regex_num >= 0 && regex_num <= last_regex );
  match = regexp_exec( text, start_of_line, FALSE,
			regex_table[regex_num].compiled );
  if ( !match )
    return NULL;
  else return regexp_eopat[0];
}

#ifdef LUA
unsigned char cond=0xFF;
char fake_nl='\0';
#endif

const G_SCHAR* regex_string(int regex_num) {
  return regex_table[regex_num].source;
}

static int
regexp_key( int regex_num ) {
  const G_SCHAR* s;
  int ch;
  s = regex_table[regex_num].source;
  if ( s[0] == '^' )
    s++;
  if ( s[0] == '\\' && s[1] == '<' )
    s += 2;
  ch = (G_CHAR)s[0];
  if ( (ch > 0x7F) || (strchr(".*+\\[]", ch )==NULL && !is_operator(ch)) )
    return ch;
  else return PT_REGEXP;
}

static struct action_ops {
  const char* name;
  unsigned char code;
  unsigned char nargs;
} action_operators[] =
  { { "abort", OP_ABORT, 0 },
    { "add", OP_ADD, 2 },
    { "and", OP_AND, 2 },
    { "append", OP_APPEND, 2 },
    { "as-ascii", OP_EQUOTE, 1 },
    { "bind", OP_BIND, 2 },
    { "center", OP_CENTER, 2 },
    { "char-int", OP_CHARINT, 1 },
    { "close", OP_CLOSE, 1 },
    { "cmpi", OP_STRI_CMP, 5 }, /* compare strings, case insensitive */
    { "cmpn", OP_NUM_CMP, 5 },  /* compare numbers */
    { "cmps", OP_STR_CMP, 5 },  /* compare strings, case sensitive */
    { "column", OP_COL, 0 },
#ifdef LUA
    { "column", OP_COL_MRK, 1 },
#endif
    { "date", OP_DATE, 0 },
    { "datime", OP_DATIME, 0 },
    { "decr", OP_DECR, 1 },
    { "define", OP_DEFINE, 1 },
    { "defset" ,  OP_CHAR_SET, 2 },
    { "div", OP_DIV, 2 },
    { "downcase", OP_DOWNCASE, 1 },
#if G_CHAR_BYTES > 1
    { "encoding", OP_ENCODING, 0 },
#endif
    { "end", OP_EXIT, 0 },
    { "err", OP_ERR, 1},
    { "exit-status", OP_EXIT_STATUS, 1 },
    { "expand-wild", OP_EXP_WILD, 1 },
    { "fail", OP_FAIL, 0 },
    { "file", OP_FILE, 0 },
    { "file-time", OP_MODTIME, 0 },
    { "fill-center", OP_FILL_CENTER, 2 },
    { "fill-left", OP_FILL_LEFT, 2 },
    { "fill-right", OP_FILL_RIGHT, 2 },
    { "getenv", OP_GETENV, 1 },
    { "getenv", OP_GETENV_DEFAULT, 2 },
    { "get-switch", OP_GET_SWITCH, 1 },
    { "getset" , OP_GET_SET, 1 },
    { "incr", OP_INCR, 1 },
    { "inpath", OP_PATH, 0 },
    { "int-char", OP_INTCHAR, 1 },
    { "line", OP_LINE, 0 },
#ifdef LUA
    { "line", OP_LINE_MRK, 1 },
#endif
    { "left", OP_LEFT, 2 },
    { "length", OP_LENGTH, 1 },
#ifdef LUA
    { "lua", OP_LUA, 1 },
#endif    
    { "makepath", OP_COMBINEPATH, 3 },
    { "mergepath", OP_MERGEPATH, 3 },
    { "mul", OP_MUL, 2 },
    { "mod", OP_MOD, 2 },
#ifdef LUA
    { "newline", OP_NEWLINE, 0 },
#endif    
    { "not", OP_NOT, 1 },
    { "or", OP_OR, 2 },
    { "out", OP_OUT, 1},
    { "outpath", OP_OUTFILE, 0 },
    { "out-column", OP_OUTCOL, 0 },
    { "push", OP_BIND, 2 },
    { "pop", OP_UNBIND, 1 },
    { "probe", OP_PROBE, 1 },
    { "quote", OP_QUOTE, 1 },
    { "radix", OP_RADIX, 3 },
    { "read", OP_READ, 1 },
    { "relpath", OP_RELPATH, 2 },
    { "relative-path", OP_RELPATH, 2 },
    { "repeat", OP_REPEAT, 2 },
    { "reset-syntax", OP_DEFAULT_SYNTAX, 0 },
    { "reverse", OP_REVERSE, 1 },
    { "right", OP_RIGHT, 2 },
#ifdef LUA
    { "rules", OP_GETRULES, 1 },
    { "rules", OP_RULES, 2 },
#endif    
    { "set", OP_SET, 2 },
#if defined(_WIN32) || !defined(MSDOS)
    { "set-locale", OP_LOCALE, 1 },
#endif
    { "set-switch", OP_SET_SWITCH, 2 },
    { "set-syntax", OP_SYNTAX, 2 },  /*  @set-syntax{type;charset}  */
    { "set-parm", OP_SET_PARM, 2 },
    { "set-wrap", OP_SET_WRAP, 2 },
    { "shell", OP_SHELL, 1},
    { "show-help", OP_HELP, 0},
    { "sub", OP_SUB, 2 },
    { "subst", OP_SUBST, 2 },	/*  @subst{patterns;operand}  */
    { "substring", OP_SUBSTRING, 3 }, /*  @substring{skip,length,string}  */
    { "tab", OP_TAB, 1 },
    { "terminate", OP_END_OR_FAIL, 0 },
    { "time", OP_TIME, 0 },
    { "unbind", OP_UNBIND, 1 },
    { "undefine", OP_UNDEFINE, 1 },
    { "upcase", OP_UPCASE, 1 },
    { "var", OP_VAR, 1 },
    { "var", OP_VAR_DFLT, 2 },
    { "version", OP_VERSION, 0 },
    { "wrap", OP_WRAP, 1 },
    { "write", OP_WRITE, 2 },
    { NULL, 0, 0 }
  };

/* number of arguments for each built-in function: */
unsigned char fnnargs[OP_last_op] = { 0 };

#if G_CHAR_BYTES > 1
/* Chinese character ranges */
static struct char_range hranges[] = {
  {0x2E80, 0x2EFF}, {0x3000, 0x303F}, {0x31C0, 0x31EF},
  {0x3200, 0x9FFF}, {0x20000, 0x323FF} };
/* valid text character ranges (as of Unicode 15.0) */
static struct char_range vranges[] =
  { {0x08, 0x0D}, {0x20, 0x7E}, {0xA0, 0xD7FF},
    {0xF900, 0xFDCF}, {0xFDF0, 0xFEFE}, {0xFF01, 0xFFFC},
    {0x10000, 0x1FFFD}, {0x20000, 0x2FFFD}, {0x30000, 0x323FF},
    {0xE0000, 0xE01FF} };
/* emojis, pictographs, geometric shapes, and other non-language graphics */
static struct char_range eranges[] =
  { {0x2190, 0x21FF},
    {0x2300, 0x2360}, {0x237B, 0x239A}, {0x23B4, 0x23FF},
    {0x2500, 0x27BF}, {0x27F0, 0x27FF}, {0x2900, 0x297F},
    {0x2B00, 0x2BFF}, {0xFFE8, 0xFFEE}, {0x1CF00, 0x1D24F},
    {0x1F000, 0x1F0FF}, {0x1F300, 0x1FBFF} };
/* additional punctuation characters (beyond ASCII) */
static struct char_range punctranges[] =
  { /* {0xAB, 0xAB}, {0xBB, 0xBB},  -- omit because breaks test8 */
    {0x2010, 0x2027}, {0x2030, 0x205E}, { 0x27E6, 0x27EF }, {0x2E00, 0x2E7F} };

#endif

/* mathematical operator symbols */
static struct char_range mranges[] = {
  {'+', '+'}, {'-', '-'}, {'/', '/'}, {'<', '>'}, {'~', '~'}, 
  {0xAC, 0xAD}, {0xB1, 0xB1},
  {0xD7, 0xD7}, {0xF7, 0xF7}
#if G_CHAR_BYTES > 1
  , {0x2061, 0x2064}, {0x2200, 0x22FF},
  {0x2336, 0x237A}, {0x27C0, 0x27EF}, 
  {0x2980, 0x2AFF}, {0x1D400, 0x1D7CD}
#endif
};

void initialize_syntax(void) {
  int i;
  assert(G_CHAR_BYTES==sizeof(G_CHAR)); /* verify configuration */
  assert(sizeof(G_XCHAR) >= sizeof(G_CHAR));
  assert(sizeof(G_XCHAR_OR_EOF) >= sizeof(G_XCHAR));
#if G_CHAR_BYTES > 1
  assert(sizeof(G_XCHAR) >= 3);
  for ( i = 0 ; i < NUMX ; i++ )
    extended_char_table[i] = NULL;
#endif
  if ( sizeof(*syntax_chars) != sizeof(*default_syntax_chars) ) {
    for ( i = 0 ; i <= Num_Char_Kinds ; i++ )
      syntax_chars[i] = default_syntax_chars[i];
  } else
  memcpy(syntax_chars, default_syntax_chars, sizeof(syntax_chars));
  for ( i = 0x20 ; i < NUMCHAR ; i++ )
    set_char_kind(i,PI_LITERAL);
  set_char_kind('\n',PI_END);
  for ( i = 0 ; i < Num_Char_Kinds ; i++ ) {
    unsigned int ch = syntax_chars[i];
    if ( ch != '\0' )
      set_char_kind(ch,i);
  }
}

#define CHAR_SET_FROM_RANGES(ranges) \
  char_set_from_ranges(ranges, sizeof(ranges)/sizeof(struct char_range))

/* The following initializations are separated from initialize_syntax
   because they are not to be repeated by @reset-syntax */
void initialize_misc(void) {
  {
    const struct action_ops * tp;
    for ( tp = &action_operators[0] ; tp->name != NULL ; tp++ )
      fnnargs[tp->code] = tp->nargs;
  }
  memset(character_sets, 0, sizeof(character_sets));
  /* For compatibility with version 1, here hyphen is just an
     ordinary character, not a range. */
  charset_for_letter('I') = make_char_set( idchars,  FALSE, G_NULL_CHAR, NULL);
  charset_for_letter('F') = make_char_set(filechars, FALSE, G_NULL_CHAR, NULL);
#if G_CHAR_BYTES > 1
  charset_for_letter('H') = CHAR_SET_FROM_RANGES(hranges); /* Chinese */
  charset_for_letter('V') = CHAR_SET_FROM_RANGES(vranges); /* valid text */
  charset_for_letter('E') = CHAR_SET_FROM_RANGES(eranges); /* emojis */
  charset_for_letter('Y') = CHAR_SET_FROM_RANGES(punctranges); /* punctuation */
#endif
  charset_for_letter('M') = CHAR_SET_FROM_RANGES(mranges); /* mathematical */
}

static G_CHAR*
read_action( CIStream s, G_CHAR* bp, int nargs,
	     G_XCHAR* arg_keys );

static G_XCHAR_OR_EOF
read_put( CIStream s, G_CHAR** app, int nargs,
	  G_XCHAR* arg_keys ) {
  G_XCHAR_OR_EOF ch;
  G_XCHAR_OR_EOF pc;
  G_CHAR* ap;
  ap = *app;
  ch = cis_getch(s);
  if ( ch <= '9' && ch >= '0' ) {  /* "$1" is argument */
    pc = ch - '0';
    if ( pc == 0 ) /* $0 is special case */
      pc = PT_MATCHED_TEXT;
    else if ( pc < 1 || pc > nargs ) {
      pc = syntax_chars[PI_PUT];
      input_error(s, EXS_SYNTAX,
    		  "Invalid argument number: \"%c%c\"\n", pc, ch);
    }
    else *ap++ = PT_PUT_ARG;
  }			/* "$x" is single-letter variable */
  else if ( (ch <= max_ctype_char && ch > 0) ? G_IS(alpha)(ch)
            : (char_kind(ch) == PI_LITERAL) ) {
    *ap++ = PT_VAR1;
    pc = ch;
  }
  else if ( char_kind(ch) == PI_BEGIN_ARG && arg_keys != NULL ) {
	/* "${varname}" */
    G_XCHAR_OR_EOF xc;
    G_CHAR* ap1;
    *ap++ = PT_OP;
    *ap++ = OP_VAR;
    ap1 = ap;
    ap = read_action( s, ap,  nargs, arg_keys );
    pc = PT_SEPARATOR;
    xc = cis_prevch(s);
    if ( xc == syntax_chars[PI_ARG_SEP] || char_kind(xc) == PI_ARG_SEP ) {
      (*app)[1] = OP_VAR_DFLT;
      *ap++ = PT_SEPARATOR;
      ap = read_action( s, ap,  nargs, arg_keys );
      xc = cis_prevch(s);
    }
    else {
      int n;
      G_SCHAR* end;
      *ap = '\0';
      n = (int)G_STRTOL((G_SCHAR*)ap1,&end,10);
      if ( n > 0 && end == (G_SCHAR*)ap && n <= nargs ) {
	/* an argument number instead of a variable */
	ap1[-2] = PT_PUT_ARG;
	ap = ap1-1;
	pc = n;
      }
    }
    if ( char_kind(xc) != PI_END_ARG ) {
      COStream es = get_stderr_stream();
      input_error(s, EXS_SYNTAX, "Missing \"");
      cos_putch(es, syntax_chars[PI_END_ARG]);
      cos_putss(es, "\" for \"");
      cos_putch(es, syntax_chars[PI_PUT]);
      putch_end_quote(syntax_chars[PI_BEGIN_ARG]);
    }
  }
  else {
    COStream es = get_stderr_stream();
    input_error(s, EXS_SYNTAX, "Invalid variable reference: \"");
    cos_putch(es, syntax_chars[PI_PUT]);
    putch_end_quote(ch);
    PUT_XCHAR(ap, syntax_chars[PI_PUT]);
    pc = ch;
  }
  *app = ap;
  return pc;
}

#ifdef LUA
static boolean 
skip_comment( CIStream s ) {
  G_XCHAR_OR_EOF ch='\1';
  G_XCHAR_OR_EOF nc;
  static COStream lua_buf=NULL;
  
  for ( ; ; ) {
    nc = cis_getch(s);
    if ( nc == G_EOF ) {
      if (lua_buf) {
        input_error(s, EXS_SYNTAX, "Lua buffer left open at EOF\n");
        cos_close(lua_buf);
	    lua_buf=NULL;
      }
	  break; 
    }
    else if ((ch == '\1') && (nc == '[')) {
	  assert(lua_buf == NULL);
	  lua_buf=make_buffer_output_stream();
	  if (!lua_buf)
	    input_error(s, EXS_FAIL, "No space to store Lua code\n");
    }
    else if (ch == '\0' && nc == '!' && cis_peek(s) == ']' ) {
	  if (lua_buf) {
		CIStream t = convert_output_to_input( lua_buf );
        if (t) {
          dolua(s,NULL,cis_whole_string(t));
	      cis_close(t);
        }
        lua_buf=NULL;
      }
    }
    else if ( nc == '\n' ) {
	  if (lua_buf) {
		ch='\0';
		cos_putch(lua_buf,'\n');
	  }
      else
        break;
    }
    else {
	  ch = nc; 
	  if (lua_buf) cos_putch(lua_buf,ch);
    }
  }
  /* ch is the character just before the EOL or EOF */
  if ( char_kind(ch) == PI_ESC ) {
    /* line ends in "\"; continue with the next line. */
    /* ignore new line and leading space on next line */
    skip_whitespace(s);
    return FALSE;
  }
  else return (lua_buf == NULL);
}

#else
static boolean 
skip_comment( CIStream s ) {
  G_XCHAR_OR_EOF ch;
  ch = 0;
  for ( ; ; ) {
    G_XCHAR_OR_EOF nc;
    nc = cis_getch(s);
    if ( nc == '\n' || nc == G_EOF )
      break;
#ifndef MSDOS
#if '\r' != '\n'
    else if ( nc == '\r' ) /* allow CR,LF from MSDOS */
      continue;
#endif
#endif
    else ch = nc;
  }
  if ( char_kind(ch) == PI_ESC ) {
    /* line ends in "\"; continue with the next line. */
    /* ignore new line and leading space on next line */
    skip_whitespace(s);
    return FALSE;
  }
  else return TRUE;
}
#endif

static void control_error(CIStream s, G_XCHAR ch, G_XCHAR pc)
{
  COStream es = get_stderr_stream();
  input_error(s, EXS_SYNTAX, "Undefined control sequence: \"");
  cos_putch(es, ch);
  putch_end_quote(pc);
}

static void unmatched_error(CIStream s, G_XCHAR ch)
{
  COStream es = get_stderr_stream();
  input_error(s, EXS_SYNTAX, "Unmatched '");
  cos_putch(es, ch);
  cos_put_line(es, "'");
}

/* maximum length of template or action: */
#define BUFSIZE 1200

static G_CHAR*
read_action( CIStream s, G_CHAR* bp, int nargs,
	     G_XCHAR* arg_keys ) {
  G_CHAR* ap;
  enum char_kinds kind;
    for ( ap = bp ; ; ) {
      G_XCHAR_OR_EOF pc; /* code for action */
      G_XCHAR_OR_EOF ch; /* character read */

      ch = cis_getch(s);
      pc = ch; /* just to avoid warning from Gnu compiler */
      kind = char_kind(ch);
dispatch:
      switch ( kind ) {
      case PI_COMMENT: /* ignore rest of line */
	if( !skip_comment(s) )
	  continue;
	/* else fall-through to end the line */
      case PI_END:
      case PI_ARG_SEP:
      case PI_END_ARG:
      case PI_EOF:
	*ap = PT_END;
	return ap;

      case PI_BEGIN_ARG: {
        COStream es = get_stderr_stream();
        input_error(s, EXS_SYNTAX, "Unexpected '");
        cos_putch(es, ch);
        cos_put_line(es, "' encountered.");
        pc = ch;
        break;
      }

      case PI_PUT:	/* argument or variable */
	pc = read_put( s, &ap, nargs, arg_keys );
	break;

    case PI_ARG:
    case PI_RARG:
    case PI_ABBREV_DOMAIN:
    case PI_1ARG: {
	int ai;
	pc = ch;
	for ( ai = 1 ; ai <= nargs ; ai++ )
	  if ( (int)arg_keys[ai] == ch ) {
	    *ap++ = PT_PUT_ARG;
	    pc = ai;
	    arg_keys[ai] = '\0';
	    break;
	  }
	if ( pc == ch && nargs > 0  ) {
	  COStream es = get_stderr_stream();
	  input_error(s, EXS_OK, "More '");
	  cos_putch(es, ch);
	  cos_put_line(es, "' in action than in template.");
	}
	break;
	}

    case PI_CHAR_OP:
charop: {
      pc = cis_getch(s);
      kind = default_char_kind(pc);
      if ( kind <= PI_LIT_CTRL ) {
        control_error(s, ch, pc);
        break;
      }
      else {
	ch = pc;
	goto dispatch;
      }
    }

    case PI_OP: { /* function */
	  G_XCHAR_OR_EOF xc;
	  G_CHAR* xp;
	  G_CHAR* apcode;
	  *ap = (G_CHAR)ch; /* not actually used? */
	  for ( xp = ap+1 ; ; ) {
	    xc = cis_peek(s);
	    /* scanning until the end of the name is found */
	    if ( ( xc & ~0x7F ) /* not ASCII */
		 ? (char_kind(xc) != PI_LITERAL)
		 : ( !G_IS(alnum)(xc) && xc != '_' && xc != '-' ) ) {
	      const struct action_ops * tp;
	      G_SCHAR* name;
	      const char* narrow_name;
#if G_CHAR_BYTES > 1
	      char nbuf[100];
#endif
	      if ( xp == ap+1 && ch == syntax_chars[PI_CHAR_OP] &&
	           char_kind(xc) != PI_BEGIN_ARG )
		goto charop;
	      *xp = '\0';
	      name = trim_name(ap+1);
#if G_CHAR_BYTES > 1
	      wide_to_utf8(name, nbuf, sizeof(nbuf));
	      narrow_name = nbuf;
#else
	      narrow_name = name;
#endif
	      for ( tp = &action_operators[0] ; ; tp++ ) {
	        if ( tp->name == NULL ) {
		  /* Reached the end of the action_operators list. */
		  /* See if it is a domain instead of a built-in function. */
		  int domain = find_domain(name);
	          *ap++ = PT_DOMAIN;
#if SINGLE_CHAR_DOMAIN_NUM
		  *ap++ = (G_CHAR)(domain + 1);
#else
                  *ap++ = (unsigned char)((domain & 0x7f)|0x80);
                  *ap++ = (unsigned char)(((domain>>7) & 0x7f)|0x80);
#endif
		  if ( char_kind(xc) == PI_BEGIN_ARG ) {
		    int term_kind;
		    G_XCHAR_OR_EOF term_char;
		    (void)cis_getch(s);
		read_arg:
		    ap = read_action( s, ap,  nargs, arg_keys );
		    term_char = cis_prevch(s);
		    term_kind = char_kind(term_char);
		    if ( term_kind != PI_END_ARG ) {
		      COStream es = get_stderr_stream();
		      if ( term_kind == PI_ARG_SEP ||
			   term_char == syntax_chars[PI_ARG_SEP] ) {
			input_error(s, EXS_SYNTAX, "Arg separator \"");
			cos_putch(es, term_char);
			cos_putss(es, "\" in domain call \"");
			cos_putch(es, ch);
			cos_puts(es, domain_name(domain));
			putch_end_quote(xc);
			*ap++ = term_char;
			goto read_arg;
		      }
		      else {
			input_error(s, EXS_SYNTAX, "Missing \"");
			cos_putch(es, syntax_chars[PI_END_ARG]);
			cos_putss(es, "\" for \"");
			cos_putch(es, ch);
			cos_puts(es, domain_name(domain));
			putch_end_quote(xc);
		      }
		    }
		  }
		  else {
		    COStream es = get_stderr_stream();
		    input_error(s, EXS_SYNTAX, "Error: missing '");
		    cos_putch(es, syntax_chars[PI_BEGIN_ARG]);
		    cos_putss(es, "' after \"");
		    cos_putch(es, ch);
		    cos_puts(es, domain_name(domain));
		    cos_put_line(es, "\"");
		  }
	          pc = PT_SEPARATOR;
	          goto store;
	        } /* end tp->name == NULL */
	        if ( stricmp(narrow_name, tp->name) == 0 ) {
		  unsigned n = 0;
	          *ap++ = PT_OP;
		  apcode = ap;
	          *ap++ = tp->code;
		  if ( char_kind(xc) == PI_BEGIN_ARG ) {
		    (void)cis_getch(s);
		    for ( ; ; ) {
		      ap = read_action( s, ap,  nargs, arg_keys );
		      *ap++ = PT_SEPARATOR;
		      n++;
		      xc = cis_prevch(s);
		      if ( xc != syntax_chars[PI_ARG_SEP] &&
		    	   char_kind(xc) != PI_ARG_SEP ) {
			if ( char_kind(xc) != PI_END_ARG ) {
			  COStream es = get_stderr_stream();
			  input_error(s, EXS_SYNTAX, "Missing \"");
			  cos_putch(es, syntax_chars[PI_END_ARG]);
			  cos_putss(es, "\" for \"");
			  cos_putch(es, ch);
			  cos_putss(es, tp->name);
			  putch_end_quote(syntax_chars[PI_BEGIN_ARG]);
			}
		    	break;
		      }
		    }
		  }
		  if ( n != tp->nargs ) {
		    /* @fn{} could mean no args or one empty arg. */
		    if ( n == 1 && tp->nargs == 0 && ap == apcode+2 ){
		      assert( ap[-1] == PT_SEPARATOR );
		      ap--;
		    }
		    else {
	              const struct action_ops * xtp;
		      for ( xtp = tp+1; ; xtp++ ) {
	                if ( xtp->name == NULL ) {
		          input_error(s, EXS_SYNTAX,
	                    "wrong number of arguments for %c%s:"
			    " expected %d, found %d\n",
			    ch <= 0xFF ? ch : default_syntax_chars[PI_OP],
			    tp->name, tp->nargs, n);
	                  break;
	                }
	                else if ( xtp->nargs == n &&
		      		stricmp(tp->name,xtp->name) == 0 ) {
		      	  *apcode = xtp->code;
			  break;
		      	}
	             }
	            }
		  }
	          break;
	        } /* end name matches */
	      } /* end for tp */
	      goto end_op;
	    } /* end !isalpha */
	    PUT_XCHAR(xp, cis_getch(s)); /* collecting character of name */
	  } /* end for xp */
      end_op:
	  continue;
	} /* end PI_OP */

      case PI_IGNORED_SPACE:
      	while ( char_kind(cis_peek(s)) == PI_IGNORED_SPACE )
	  (void)cis_getch(s);
	if ( ap > bp && isident(ap[-1]) && isident(cis_peek(s)) )
	  pc = ' ';
	else if ( ap <= bp || ap[-1] != PT_ID_DELIM )
	  pc = PT_ID_DELIM;
	else continue;
	break;
      case PI_SPACE:
	*ap++ = PT_SPACE;
      	while ( cis_peek(s) == ' ' ) /* extra spaces are literals */
	  *ap++ = cis_getch(s);
	continue;

      case PI_QUOTE:
      case PI_ESC: {
	 G_CHAR* sp;
	 sp = ap;  *sp = '\0';
	 ap = escaped_char(ch,ap,s);
	 if ( *sp == PT_AUX ) {
	   COStream es = get_stderr_stream();
	   input_error(s, EXS_OK, "Escape sequence \"");
	   cos_putch(es, (ch < 0x7F ? ch : default_syntax_chars[PI_ESC]));
	   cos_putch(es, cis_prevch(s));
	   cos_put_line(es, "\" is not meaningful in an action.");
	 }
	 continue;
       }

      case PI_QUOTE_STRING:
	for ( ; ; ) {
	  pc = cis_getch(s);
	  if ( pc == ch )
	    break;
	  if ( pc == '\n' || pc == G_EOF ) {
	    unmatched_error(s, ch);
	    *ap = PT_END;
	    return ap;
	  }
	  PUT_XCHAR(ap,pc);
	}
	continue;
      
#ifndef MSDOS
#if '\r' != '\n'
      case PI_CR:
	if ( cis_peek(s) == '\n' )
	  /* ignore redundant CR in files copied from MS-DOS to UNIX */
	  continue;
	else pc = ch;
	break;
#endif
#endif
      case PI_CTRL:  /* control character */
        ch = control_mod(cis_getch(s), s);
	 /* and fall-through */
    case PI_DOMAIN:		/* This group of characters are */
    case PI_SEP:		/* special in templates but not */
    case PI_BEGIN_DOMAIN_ARG:	/* in actions. */
    case PI_END_DOMAIN_ARG:
    case PI_BEGIN_REGEXP:
    case PI_END_REGEXP:
	 if ( !is_operator(ch) ) {
	   pc = ch;
	   break;
	 } /* else fall-through */
    case PI_LIT_CTRL:
       /* quote characters that might be mistaken for internal control */
       *ap++ = PT_QUOTE;
       /* and fall-through */
    case PI_LITERAL:
#ifdef MSDOS
    case PI_CR:
#endif
       pc = ch;
       break;
    case PI_IGNORE:
       continue;
    case Num_Char_Kinds: /* just to avoid compiler warning */
#ifndef NDEBUG
    default:
	input_error(s, EXS_FAIL, "Invalid char kind %d for '%c'\n", kind, ch);
#endif
	pc = ch;
	break;
      } /* end switch(kind) */
   store:
      if ( ap >= &bp[BUFSIZE-10] ) {
	input_error(s, EXS_SYNTAX, "Action is too long.\n");
	ap = &bp[BUFSIZE/2];
      }
      PUT_XCHAR(ap,pc);
    } /* end for */
} /* end read_action */

static G_CHAR*
read_actions( CIStream s, G_CHAR* pbuf, int nargs,
	      G_XCHAR* arg_keys ) {
    G_CHAR* ap;
    G_XCHAR_OR_EOF prevch;
    ap = read_action(s,pbuf,nargs,arg_keys);
    prevch = cis_prevch(s);
    while ( char_kind(prevch) == PI_END_ARG && cis_peek(s) != G_EOF ) {
      COStream es = get_stderr_stream();
      input_error(s, EXS_SYNTAX, "Extraneous '");
      cos_putch(es, prevch);
      cos_put_line(es, "'");
      *ap++ = prevch;
      ap = read_action(s,ap,nargs,arg_keys);
      prevch = cis_prevch(s);
    }
    *ap++ = PT_END;
    if ( PT_END != '\0' )
	*ap++ = '\0';
    return ap;
}

static Pattern
read_pattern ( CIStream s, int * domainpt, int default_domain,
		boolean undef ){
  G_XCHAR_OR_EOF ch;  /* character read */
  enum char_kinds kind; /* kind of character read */
  G_XCHAR_OR_EOF pc;  /* pattern character */
  G_CHAR pbuf [BUFSIZE];
  G_CHAR* bp;
  G_CHAR* start_bp;
  G_CHAR* prev_bp;
  size_t plen;
  Pattern pt;
  G_CHAR* astring;
  G_CHAR* pstring;
  int nargs;
  G_XCHAR arg_keys[MAX_ARG_NUM+2];
  boolean domain_seen;
  static G_CHAR dummybuf[2] = { PT_END, G_NULL_CHAR };

top:
  if ( cis_prevch(s) == '\n' )
    *domainpt = default_domain;
  nargs = 0;
  bp = pbuf;
  start_bp = dummybuf;
  domain_seen = FALSE;
  if ( cis_peek(s) == '\f' )
    (void)cis_getch(s);

  while ( char_kind(cis_peek(s)) == PI_OP ) { /* immediate action */
      enum Translation_Status save_status;
      if ( *domainpt != default_domain ) {
        input_error(s, EXS_OK,
            "Warning: immediate action within a domain is not meaningful.\n");
      }
      (void)read_actions(s,pbuf,0,NULL);
      save_status = translation_status;
      do_action( pbuf, NULL, output_stream);
      if ( (translation_status >= Translate_Exited) &&
           (save_status < Translate_Exited) &&
           cis_is_file(s) && (cis_peek(s) != G_EOF) ) {
        int c;
        c = syntax_chars[PI_OP];
        if ( c > 0xFF )
          c = default_syntax_chars[PI_OP];
        input_error(s, EXS_OK,
            "Warning: execution will be terminated by an immediate "
                    "%cend or %cfail within the file.\n", c, c);
      }
    }

  /* read template to match */
  for ( ; ; ) {
    prev_bp = start_bp;
    start_bp = bp;
    ch = cis_getch(s);
    pc = ch; /* just to avoid warning from Gnu compiler */
    kind = char_kind(ch);
dispatch:
    switch (kind) {
    case PI_COMMENT: /* ignore rest of line */
      if ( pbuf[0] == PT_RECUR && cis_column(s) == 2 && cis_line(s) == 1 )
	/* As a special case, if the first line begins with "#!", then treat
	   the whole line as a comment on the assumption that this is an
	   executable file using "gema" instead of a shell.  For example:
			#!/usr/local/bin/gema -f
	   Note that this wouldn't be meaningful as a pattern anyway. */
	bp = pbuf;
      if( !skip_comment(s) )
	continue;
      /* else fall-through to end line */
    case PI_END:
      if ( bp == pbuf ) /* empty pattern; continue reading */
	goto top;
      /* else fall-through */
    case PI_EOF:
      if ( bp != pbuf && !undef ) {
        /* Reached end of line without separator between template and action. */
        COStream es = get_stderr_stream();
        input_error(s, EXS_SYNTAX, "Missing '");
        cos_putch(es, syntax_chars[PI_SEP]);
        cos_put_line(es, "'");
        goto top; /* discard incomplete rule to avoid other errors */
      }
      /* and fall-through */
    case PI_SEP:
      goto done;
#ifndef MSDOS
#if '\r' != '\n'
    case PI_CR:
      if ( cis_peek(s) == '\n' )
	/* ignore redundant CR in files copied from MS-DOS to UNIX */
	continue;
      else pc = ch;
      break;
#endif
#endif

    case PI_DOMAIN:  /* explicit domain name */
      if ( domain_seen ) {
	if ( bp == pbuf ) {
	  /* double colon indicates domain inheritance */
	  G_SCHAR* iname;
	  G_CHAR* xp;
	  Domain dp = domains[*domainpt];

	  for ( xp = bp ; ; ) {
	    enum char_kinds xc = char_kind(cis_peek(s));
	    if ( xc == PI_END || xc == PI_COMMENT || xc == PI_EOF )
	      break;
	    else {
	      G_XCHAR dch = cis_getch(s);
	      PUT_XCHAR(xp,dch);
	    }
	  }
	  *xp = '\0';
	  iname = trim_name(bp);
	  if ( dp->inherits != NULL ) {
	    input_error(s, EXS_SYNTAX,
	  		"Domain \"%s\" already inherits from \"%s\".\n",
	  		safe_string(dp->name),
			safe_string(dp->inherits->name) );
	  }
	  else dp->inherits = get_domain(iname);
	  goto top;
	}
	else {
	  COStream es = get_stderr_stream();
	  input_error(s, EXS_OK, "More than one '");
	  cos_putch(es, ch);
	  cos_put_line(es, "'");
      	  pc = ch;
	}
	break;
      }
      else {
	G_SCHAR* name;
	*bp = '\0';
	name = trim_name(pbuf);
	if ( domain_name(*domainpt)[0] == ' ' ) {
	  /* only the domain " temp " created by OP_SUBST can have a
	     leading space */
	  COStream es = get_stderr_stream();
	  input_error(input_stream, EXS_SYNTAX, "Domain change \"");
	  cos_puts(es, name);
	  cos_putch(es, ch);
	  cos_put_line(es, "\" not allowed in temporary pattern.");
	}
	else
#if SINGLE_CHAR_DOMAIN_NUM
	*domainpt = name[0] == PT_RECUR ? name[1]-1 : find_domain( name );
#else
	*domainpt = name[0] == PT_RECUR ? (unsigned int)(name[1]&0x7f) | (unsigned int)((name[2]&0x7f)<<7) : find_domain( name );
#endif
	bp = pbuf;
	domain_seen = TRUE;
	nargs = 0; /* don't count <name> on left of : as an argument */
	continue;
      }

    case PI_OP:
      if ( syntax_chars[PI_CHAR_OP] != ch ) {
          COStream es = get_stderr_stream();
          input_error(s, EXS_OK, "'");
          cos_putch(es, ch);
          cos_put_line(es, "' not valid in template");
          pc = ch;
          break;
      }
	/* else fall through when same character used for both */
   case PI_CHAR_OP: {
      pc = cis_getch(s);
      kind = default_char_kind(pc);
      if ( kind > PI_LIT_CTRL ) {
	  ch = pc;
	  goto dispatch;
      	}
      control_error(s, ch, pc);
      break;
    }

    case PI_ARG:
    case PI_RARG:
    case PI_ABBREV_DOMAIN:
    case PI_1ARG:
    case PI_BEGIN_DOMAIN_ARG:
    case PI_BEGIN_REGEXP:
      if ( nargs >= MAX_ARG_NUM ) {
	*bp = '\0';
	if ( nargs == MAX_ARG_NUM )
	  input_error(s, EXS_SYNTAX, "Pattern has more than %d arguments.\n",
      		      MAX_ARG_NUM);
	pc = ch;
      }
      else {
	nargs++;
	arg_keys[nargs] = ch;
	if ( kind == PI_RARG ) {
	  int domain = *domainpt;
	  *bp++ = PT_RECUR;
#if SINGLE_CHAR_DOMAIN_NUM
	  pc = domain + 1;
#else
	  /* Save low byte of domain index */
	  *bp++ = (unsigned char)((domain &0x7f)|0x80);
	  pc = ((domain>>7) & 0x7f)|0x80;
#endif
	}
	else if ( kind == PI_ABBREV_DOMAIN ) {
	  int domain;
	  G_SCHAR aname[2];
	  aname[0] = ch;
	  aname[1] = G_NULL_CHAR;
	  domain = find_domain(aname);
	  *bp++ = PT_RECUR;
#if SINGLE_CHAR_DOMAIN_NUM
	  pc = 1 + domain;
#else
	  /* Save low byte of domain index */
	  *bp++ = (unsigned char)((domain &0x7f)|0x80);
	  pc = (((unsigned int)domain>>7) & 0x7f)|0x80;
#endif
	}
	else if ( kind == PI_BEGIN_DOMAIN_ARG ) {
	  G_XCHAR_OR_EOF xc;
	  G_CHAR* xp;
	  for ( xp = bp ; ; ) {
	    enum char_kinds xk;
	    xc = cis_getch(s);
	    xk = char_kind(xc);
	    if ( xk == PI_END_DOMAIN_ARG ||
		 syntax_chars[PI_END_DOMAIN_ARG] == xc ||
		 ( (xk == PI_CHAR_OP || xc == syntax_chars[PI_CHAR_OP]) &&
		   cis_peek(s)==(int)default_syntax_chars[PI_END_DOMAIN_ARG] &&
		   cis_getch(s) ) ) {
	      int inverse;
	      const G_SCHAR* dname;
	      const G_CHAR* rname;
	      unsigned char rname0;
	      *xp = '\0';
	      inverse = 0;
	      dname = trim_name(bp);
	      rname = dname;
#if G_CHAR_BYTES > 1
	      /* built-in recognizers use only ASCII letters */
	      if ( (rname[0] | rname[1]) > 0x7F )
		rname0 = 0; /* to fail tests below */
	      else
#endif
	      rname0 = (unsigned char)rname[0];
	      /* can't make up my mind which to use here: */
	      if ( strchr("^/~-!", rname0) != NULL && rname0 != '\0' ) {
	      	rname++;
		rname0 = (unsigned char)rname[0];
		inverse = 0x80;
	      }
	      if ( ( rname[1] == '\0'||
		     ( G_IS(digit)(rname[1]) &&
	               ( rname[2] == '\0' ||
	    		 ( G_IS(digit)(rname[2]) &&
			   rname[3] == '\0' ) ) ) ) &&
	    	   isalpha(rname0) ) {
		/* built-in domains with single-letter names */
		unsigned parms;
		parms = inverse | (toupper(rname0) - ('A'-1));
		if ( islower(rname0) )
		  parms |= 0x40;
		if ( rname[1] == '\0' )
		  pc = 0xFF;
		else {
		  G_SCHAR* endp;
		  const G_SCHAR* rname1 = rname+1;
		  pc = 1 + (int)G_STRTOL(rname1, &endp, 10);
#if !defined(NDEBUG) /* can't actually happen because ruled out above */
		  if ( *endp != G_NULL_CHAR ) {
		    input_error(s, EXS_SYNTAX,
				"Invalid argument: %c%s%c\n",
				syntax_chars[PI_BEGIN_DOMAIN_ARG],
				message_string(dname),
				syntax_chars[PI_END_DOMAIN_ARG]);
		  }
#endif
		}
		*bp++ = PT_SPECIAL_ARG;
		*bp++ = parms;
	      }
	      else {  /* user-defined domain */
		int domain = find_domain( dname );
	        *bp++ = PT_RECUR;
#if SINGLE_CHAR_DOMAIN_NUM
	        pc = 1 + domain;
#else
		/* Save low byte of domain index */
		*bp++ = (unsigned char)((domain & 0x7f)|0x80);
		pc = ((domain>>7) & 0x7f)|0x80;
#endif
	      }
	      break;
	    }
	    else if ( xc == G_EOF || xk == PI_END || xk == PI_SEP ) {
	      unmatched_error(s, ch);
	      bp = xp;
	      goto done;
	    }
	    PUT_XCHAR(xp, xc);
	  }
	} /* end PI_BEGIN_DOMAIN_ARG */
	else if ( kind == PI_BEGIN_REGEXP ) {
	  G_XCHAR_OR_EOF xc;
	  G_CHAR* xp;
	  boolean op_found = FALSE;
	  pc = ch;
	  for ( xp = bp ; ; ) {
	    xc = cis_getch(s);
	    if ( xc == '\\' ) { /* not PI_ESC because this is regexp syntax */
	      xc = cis_peek(s);
	      if ( xc == 'u' || xc == 'x' || xc == 's'|| xc == 't'  ) {
		/* use gema escape semantics */
		xp = escaped_char('\\',xp,s);
		continue;
	      } /* else use regular expression semantics */
	      *xp++ = '\\';
	      xc = cis_getch(s);
	      if ( xc < 'A' && strchr("<>()", xc) != NULL )
	        op_found = TRUE;
	    }
	    else {
	    if ( syntax_chars[PI_END_REGEXP] == xc ||
		 char_kind(xc) == PI_END_REGEXP ||
		 ( (char_kind(xc) == PI_CHAR_OP || 
		    xc == syntax_chars[PI_CHAR_OP]) &&
		   cis_peek(s) == (int)default_syntax_chars[PI_END_REGEXP] &&
		   cis_getch(s) ) ) {
	        *xp = '\0';
		if ( ! op_found ) {
		  COStream es = get_stderr_stream();
		  input_error(s, EXS_OK, "You probably intended \"\\");
		  cos_putch(es, ch);
		  cos_put_line(es, "\" instead of a regular expression.");
		}
	        pc = intern_regexp( bp, s ) + 1;
 	        *bp++ = PT_REGEXP;
	        break;
	    }
	    if ( xc == '\n' || xc == G_EOF ) {
	      input_error(s, EXS_SYNTAX, "Unterminated regular expression.\n");
	      pc = PT_END;
	      break;
	    }
	    if ( xc < 0x7F && strchr(".+*[^$|", xc) != NULL )
	      op_found = TRUE;
	    }
	    PUT_XCHAR(xp,xc);
	  } /* end for xp */
	} /* end regexp */
	else pc = kind == PI_ARG ? PT_MATCH_ANY : PT_MATCH_ONE;
      }
      break;
    case PI_END_DOMAIN_ARG:
    case PI_END_REGEXP: {
      COStream es = get_stderr_stream();
      input_error(s, EXS_OK, "Warning: unmatched '");
      cos_putch(es, ch);
      cos_put_line(es, "'");
      pc = ch;
      break;
    }

    case PI_PUT:	/* argument or variable */
      pc = read_put( s, &bp, nargs, NULL );
      break;

    case PI_QUOTE:
    case PI_ESC: {
       bp = escaped_char(ch,bp,s);
       if ( start_bp > pbuf ) {
	 if ( *start_bp == PT_AUX ) {
	   if ( start_bp[1] == PTX_INIT || start_bp[1] == PTX_BEGIN_FILE )
	     input_error(s, EXS_SYNTAX,
   		"\"%c%c\" only meaningful at beginning of template.\n",
   		(ch < 0x7F ? ch : default_syntax_chars[PI_ESC]), cis_prevch(s));
	   if ( start_bp[1] == PTX_JOIN && *prev_bp == PT_ID_DELIM ) {
	     bp = prev_bp;
	   }
	 }
	 else if ( *start_bp == *prev_bp && *prev_bp == PT_ID_DELIM )
	   /* delete redundancy */
	   bp = start_bp;
       }
       continue;
    }

    case PI_QUOTE_STRING:
      for ( ; ; ) {
	pc = cis_getch(s);
	if ( pc == ch )
	  break;
	if ( pc == '\n' || pc == G_EOF ) {
	  unmatched_error(s, ch);
	  goto top; /* discard incomplete rule, start over on next line */
	}
	PUT_XCHAR(bp,pc);
      }
      continue;

    case PI_IGNORED_SPACE: {
	G_XCHAR_OR_EOF nextch;
      	while ( char_kind(nextch = cis_peek(s)) == PI_IGNORED_SPACE )
	  (void)cis_getch(s);
	if ( !( ( *prev_bp == PT_ID_DELIM || isident(*prev_bp) ) &&
		isident(nextch) ) ) {
	  if ( char_kind(nextch) != PI_LITERAL &&
	       *prev_bp != PT_SKIP_WHITE_SPACE) {
	    pc = PT_SKIP_WHITE_SPACE;
	    break;
	  }
	  else continue;
	}
	/* else fall-through */
    }
    case PI_SPACE:
    	pc  = PT_SPACE;
	break;
    case PI_CTRL:  /* control character */
       pc = control_mod(cis_getch(s), s);
       if ( is_operator(pc) )
	 /* quote characters that might be mistaken for an internal control */
	 *bp++ = PT_QUOTE;
       break;
    case PI_LIT_CTRL:
       *bp++ = PT_QUOTE;
       /* and fall-through */
    case PI_BEGIN_ARG: /* not special in this context */
    case PI_END_ARG:   /* not special in this context */
    case PI_ARG_SEP:   /* not special in this context */
    case PI_LITERAL:
    #ifdef MSDOS
      case PI_CR:
    #endif

       if ( token_mode && isident(ch) ) {
	 enum char_kinds peek_kind;
       	 if ( is_operator(*prev_bp) && *prev_bp != PT_ID_DELIM ) {
	   if ( prev_bp[0] == PT_AUX && prev_bp[1] == PTX_JOIN )
	     bp = prev_bp;
	   else *bp++ = PT_ID_DELIM;
	   start_bp = bp;
    	   }
	 peek_kind = char_kind(cis_peek(s));
	 if ( peek_kind != PI_LITERAL && peek_kind != PI_SPACE ) {
	  PUT_XCHAR(bp,ch);
	  start_bp = bp;
	  pc = PT_ID_DELIM;
	  break;
	}
       } /* end token_mode */
       pc = ch;
       break;
    case PI_IGNORE:
       continue;
    case Num_Char_Kinds: ; /* just to avoid compiler warning */
#ifndef NDEBUG
    default:
	input_error(s, EXS_FAIL, "Invalid char kind %d for '%c'\n",
		    kind, ch);
#endif
    }
    PUT_XCHAR(bp,pc);
    if ( bp > &pbuf[BUFSIZE-10] ) {
      input_error(s, EXS_SYNTAX, "Template is too long.\n");
      bp = &pbuf[BUFSIZE/2];
    }
  } /* end for */
 done:
  if ( bp == pbuf && kind != PI_SEP )
    return NULL;
  *bp++ = PT_END;
  if ( PT_END != '\0' )
    *bp++ = '\0';
  pt = (Pattern) allocate(sizeof(*pt), MemoryPatterns);
  plen = (bp - pbuf) * sizeof(G_CHAR);
  pstring = (G_CHAR*)allocate(plen, MemoryPatterns);
  memcpy(pstring,pbuf,plen);
  astring = NULL;
  pt->next = NULL;
  pt->is_copy = FALSE;
 {
  G_CHAR* ap;
  ap = pbuf;
  if ( undef )
    *ap++ = PT_UNDEF;
  if ( kind == PI_SEP ) {
    /* read action template */
    ap = read_actions(s,ap,nargs,arg_keys);
  }
  else *ap++ = PT_END;
  if ( pstring[0] == PT_MATCH_ONE && pstring[1] == PT_END ) {
    /* optimize special case when template is just "?" */
    G_CHAR* zp;
    pstring[0] = PT_ONE_OPT;
    for ( zp = pbuf ; *zp != PT_END ; zp++ )
      if ( zp[0] == PT_PUT_ARG && zp[1] == 1 ) {
      	zp[0] = PT_ONE_OPT;
      	G_STRCPY((G_SCHAR*)zp+1,(G_SCHAR*)zp+2);
      }
  }
  if ( pbuf[0] != PT_END ) {
      int len;
      len = (ap - pbuf) * sizeof(G_CHAR);
      astring = (G_CHAR*)allocate(len,MemoryPatterns);
      memcpy(astring,pbuf,len);
  }
 }
 pt->pattern = pstring;
 pt->action = astring;
#ifndef NDEBUG
  if ( debug_switch ) {
    fprintf(stderr, "%s: \"%s\" -> \"%s\"\n",
	    safe_string(domain_name(*domainpt)),
	    safe_string(pt->pattern), safe_string(pt->action));
  }
#endif
  return pt;
}

unsigned
get_template_element( const G_CHAR** ap, boolean for_goal ) {
  unsigned result;
  G_CHAR ch;
  const G_CHAR* a = *ap;
again:
    ch = *a++;
    switch(ch){
      case PT_END:
	result = ENDOP;
	break;
      case PT_AUX:
	if ( *a++ == PTX_NO_GOAL && for_goal ) {
	  result = ENDOP;
	  break;
	}
	else goto again;
      case PT_LINE:
	if ( for_goal && *a == PT_END ) {
	  result = '\n';
	  break;
	}
	else goto again;
      case PT_QUOTE:
	result = *a++;
	break;
      case PT_VAR1:
	if ( for_goal ) {
	  G_SCHAR vname[2];
	  const G_SCHAR* value;
	  size_t length;
	  vname[0] = *a++;
	  vname[1] = G_NULL_CHAR;
	  value =  get_var(vname,TRUE,&length);
	  if ( value != NULL ) {
	    if ( length == 0 )
	      goto again;
	    else {
	      result = value[0];
	      break;
	    }
	  }
	}
	/* else fall-through */
      case PT_RECUR:
      case PT_REGEXP:
      case PT_PUT_ARG:
	result = UPOP(ch) | *a++;
	break;
      case PT_SPECIAL_ARG:
	result = UPOP(ch) | *a;
	a += 2;
	break;
      case PT_ID_DELIM:
      case PT_WORD_DELIM:
	*ap = a;
	result = get_template_element(ap,for_goal);
	if ( result != ENDOP )
	  return result;
	result = UPOP(ch);
	break;
      case PT_SKIP_WHITE_SPACE:
	goto again;
      default:
	result = ch;
	if ( is_operator(ch) )
	  result = UPOP(result);
	break;
    } /* end switch */

  *ap = a;
  return result;
}

int
compare_specificity( const G_CHAR* a, const G_CHAR* b ) {
/* Given two templates (in internal representation), return > 0 if a is
   more specific than b, or < 0 if a is less specific, or 0 if they should
   remain in the same order in which they were defined.
   The two templates are compared (ignoring certain operators that don't
   matter for this purpose) until the first difference is found.  At that
   point, a literal is most specific, the end of the template is least
   specific, and arguments are intermediate.
*/
  const G_CHAR* xp;
  const G_CHAR* yp;
  int count = 0;
  for ( xp = a, yp = b ; ; ) {
    unsigned xc, yc;

    xc = get_template_element( &xp, FALSE );
    yc = get_template_element( &yp, FALSE );

    if ( xc != yc ) {
      if ( xc == ENDOP )
	return -1; /* a less specific than b */
      if ( yc == ENDOP )
	return 1; /* a more specific than b */
      if ( ISOP(yc) ) {
	if ( !ISOP(xc) )
	  return count;  /* a more specific than b (unless at beginning) */
	else if ( *xp == PT_END && yc == UPOP(PT_MATCH_ANY) )
	  return 1;  /* trailing "*" is always least specific */
      }
      else if ( ISOP(xc) )
	return -count;  /* a less specific than b (unless at beginning) */
      return 0;
    }
    else if ( xc == ENDOP )
      return 0;
    count++;
  }
}

static void
chain_pattern( Pattern pat, Patterns patset, int key ) {
  boolean undef = pat->action != NULL && pat->action[0] == PT_UNDEF;
     if ( patset->tail == NULL ) {
     	if ( undef ) {
	  delete_pattern(pat);
	  return;
     	}
	patset->head = pat;
     }
     else {
	Pattern old;
	Pattern prev;
	assert ( patset->tail->next == NULL );
	prev = NULL;
	for ( old = patset->head ; old != NULL ; old = old->next ) {
	  int cmp;
	  cmp = compare_specificity( pat->pattern, old->pattern );
	  if ( cmp > 0 && !undef ) {
	    /* new pattern more specific than old, needs to go before it. */
	    pat->next = old;
	    if ( prev == NULL )
	      patset->head = pat;
	    else prev->next = pat;
	    return;
	  }
	  else if ( cmp == 0 && G_STRCMP((const G_SCHAR*)old->pattern,
					 (const G_SCHAR*)pat->pattern) == 0 ) {
	    /* identical template */
	    if ( undef ) {
	      if ( pat->action[1] == PT_END ||
       		   G_STRCMP((const G_SCHAR*)old->action,
			    (const G_SCHAR*)pat->action+1) == 0 ) {
		/* undefine the old macro */
		if ( prev == NULL )
		  patset->head = old->next;
		else prev->next = old->next;
		if ( old == patset->tail )
		  patset->tail = prev;
		delete_pattern(old);
		delete_pattern(pat);
		return;
	      }
	    } /* end PT_UNDEF */
	    else { /* newer definition replaces older */
	    if ( old->action != pat->action ) {
	      /* Have to be careful not to delete an action that might
		 have been installed in more than one place as the result of
		 a "\W" or "\S" at the beginning of the template.
		 This crude hack favors memory leaks over corrupted data.*/
	      if ( key > max_ctype_char || !G_IS(space)(key) )
		free((char*)old->action);
	      old->action = pat->action;
	    }
	    if ( old->pattern != pat->pattern )
	      if ( key > max_ctype_char || !G_IS(space)(key) )
	      free((char*)pat->pattern);
	    free(pat);
	    return;
	    } /* end replace */
	  }
	  prev = old;
	} /* end for */
     	if ( undef ) {
	  delete_pattern(pat);
	  return;
     	}
	patset->tail->next = pat;
     }
    patset->tail = pat;
}

static Pattern
copy_pattern( Pattern opat ) {
  /* share the template and action pointers, but need separate `next' cell. */
  Pattern pt;
  pt = (Pattern) allocate(sizeof(*pt), MemoryPatterns);
  pt->pattern = opat->pattern;
  pt->action = opat->action;
  pt->next = NULL;
  pt->is_copy = TRUE;
  return pt;
}

static void
install_pattern( const G_CHAR* template, Pattern pat,
	     	 Patterns patset );

boolean
literal_key( const G_CHAR* ps );

static void
install_pattern_for_key( G_XCHAR key, const G_CHAR* rest,
	     		Pattern pat, Patterns patset ) {
  int index;
  Patterns sub;
  const G_CHAR* more;

  index = dispatch_index(key);
  if ( patset->dispatch == NULL ) {
    int i;
    patset->dispatch = (Patterns*) allocate( DISPATCH_SIZE * sizeof(Patterns),
	                                MemoryDispatch );
    for ( i = 0 ; i < DISPATCH_SIZE ; i++ )
      patset->dispatch[i] = NULL;
  }
  sub = patset->dispatch[index];
  if ( sub == NULL ) {
    sub = (Patterns)allocate( sizeof(struct patterns_struct), MemoryDispatch );
    patset->dispatch[index] = sub;
    sub->dispatch = NULL;
    sub->head = pat;
    sub->tail = pat;
  }
  else if ( sub->dispatch != NULL )
    install_pattern( rest, pat, sub );
  else if ( sub->head != sub->tail &&
	    ( (more=pat->pattern), (key==get_template_element(&more,FALSE))) &&
	    more == rest && /* not already at second level */
	    ( literal_key(more) || 
	      ( (more=sub->head->pattern) , get_template_element(&more,FALSE) ,
		literal_key(more) ) ) ) {
    /* already have at least two entries and adding a third,
       and at least one of them has a literal following the initial key;
       change to use a second-level dispatch table. */
    Pattern old;
    old = sub->head;
    sub->head = NULL;
    sub->tail = NULL;
    while ( old != NULL ) {
      Pattern next;
      unsigned op;
      const G_CHAR* old_template;
      next = old->next;
      old->next = NULL;
      old_template = old->pattern;
      op = get_template_element(&old_template,FALSE);
      if ( op == key || ( op < 0xFF && toupper(op) == toupper(key) ) )
	install_pattern( old_template, old, sub );
      else chain_pattern( old, sub, key );
      old = next;
    }
    install_pattern( rest, pat, sub );
  }
  else chain_pattern( pat, sub, key );
}

static void
install_pattern( const G_CHAR* template, Pattern pat,
	     	 Patterns patset ) {
  G_XCHAR key;
  const G_CHAR* rest;
  boolean nocase = case_insensitive;

  rest = template;
  key = NEXT_XCHAR(rest);
  while ( key == PT_AUX ) {
    G_CHAR op = *rest++;
    if ( op == PTX_NO_CASE ) /*  \C  */
      nocase = TRUE;
    key = NEXT_XCHAR(rest);
  }
  while ( key == PT_ID_DELIM || key == PT_WORD_DELIM ||
	  key == PT_LINE )
    key = NEXT_XCHAR(rest);
  if ( key == PT_SKIP_WHITE_SPACE || key == PT_SPACE ) {
      install_pattern_for_key( ' ', rest, copy_pattern(pat), patset );
      install_pattern_for_key( '\t', rest, copy_pattern(pat), patset );
#if '\r' != '\n'
      install_pattern_for_key( '\r', rest, copy_pattern(pat), patset );
#endif
      if ( pat->pattern[0] != PT_LINE && rest[0] != PT_LINE &&
  	   !line_mode && !(pat->pattern[0] == PT_AUX &&
  		           pat->pattern[1] == PTX_ONE_LINE ) ) {
	install_pattern_for_key( '\n', rest, copy_pattern(pat), patset );
	install_pattern_for_key( '\f', rest, copy_pattern(pat), patset );
      }
      if ( charset_for_letter('S') == NULL ) {
        /* dispatch accounts for all possible space characters */
        if ( key == PT_SPACE )
          return;
        else /* for optional space, key on what follows */
          key = NEXT_XCHAR(rest);
      } /* else chain the rule to check for the additional characters */
  } /* end \W or \S */
  if ( key == PT_REGEXP )
    key = regexp_key( rest[0]-1 );
  if ( key == PT_QUOTE )
    key = NEXT_XCHAR(rest);
  else if ( is_operator(key) ) {
    if ( key == PT_RECUR
#if SINGLE_CHAR_DOMAIN_NUM
	 && patset == &domains[rest[0]-1]->patterns
#else
	 && patset == &domains[(unsigned int)(rest[0]&0x7f)
			      |(unsigned int)((rest[1]&0x7f)<<7)]->patterns
#endif
	 )
      /* template beginning with recursive arg in same domain is
	 going to get stack overflow */
      input_error(input_stream, EXS_SYNTAX, "Unbounded recursion.\n");
    chain_pattern( pat, patset, key );
    return;
  }
  install_pattern_for_key( key, rest, pat, patset );
  /* For case-insensitive matching, upper and lower case ASCII letters
     have the same dispatch key in dispatch_index, but check here
     for extended letters. */
  if ( nocase && (key > 0x7F) &&
       ( key <= max_ctype_char || charset_for_letter('J') != NULL ) ) {
    G_XCHAR key2;
    key2 = g_toupper(key);
    if ( key2 == key )
      key2 = g_tolower(key);
    if ( (key2 % DISPATCH_SIZE) != (key % DISPATCH_SIZE) )
      /* put a copy of the pattern in the dispatch slot for the other case */
      install_pattern_for_key( key2, rest, copy_pattern(pat), patset );
  }
}

boolean
literal_key( const G_CHAR* ps ) {
  /* returns true if the template begins with a literal instead of
     an argument. */
  return !ISOP(get_template_element(&ps,FALSE));
}

int read_patterns ( CIStream s, const G_SCHAR* default_domain,
		    boolean undef ){
  Pattern pat;
  int domain, top;
  CIStream save_input;
  save_input = input_stream;
  if ( input_stream == NULL || cis_pathname(s) != NULL )
    input_stream = s;
  top = find_domain(default_domain);
  domain = top;
  while ( NULL != (pat = read_pattern(s, &domain, top, undef)) ) {
    const G_CHAR* ps = pat->pattern;
    if ( ps[0] == PT_AUX &&
  	 ( ps[1] == PTX_INIT || ps[1] == PTX_BEGIN_FILE ||
	   ps[1] == PTX_FINAL || ps[1] == PTX_END_FILE ) ) {
      Pattern* opp = &domains[domain]->init_and_final_patterns;
      while ( *opp != NULL )
	opp = &(*opp)->next;
      *opp = pat;
    }
    else install_pattern( ps, pat, &domains[domain]->patterns );
   } /* end while */
  input_stream = save_input;
  return top;
}
