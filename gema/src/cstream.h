/* character stream functions */

/*********************************************************************
  This file is part of "gema", the general-purpose macro translator,
  written by David N. Gray <dgray@acm.org> in 1994 and 1995.
  You may do whatever you like with this, so long as you retain
  an acknowledgment of the original source.
 *********************************************************************/

#ifndef CSTREAM_H
#define CSTREAM_H

/* suppress Microsoft warnings about "insecure" string functions */
#define _CRT_SECURE_NO_WARNINGS 1

#include <time.h>  /* for time_t */

#if defined(_USE_ICU)
#include "ustdio.h" /* for ICU's UFILE, includes stdio.h */
#elif defined(_USE_STDIO)
#include <stdio.h> /* for FILE */
#endif

#include "bool.h"
#include "main.h"
#include "util.h"
#include "type_macros.h"
#include "io_macros.h"
#include "string_macros.h"

typedef const struct encoding * Encoding; /* file encoding */
Encoding find_encoding(const char* str);
void show_encodings();

extern Encoding input_encoding;
extern Encoding output_encoding;
const char* input_encoding_name();

/* ============    Input Streams ==============  */

typedef struct input_stream_struct* CIStream;

typedef struct {
  long position;
  enum { Buffer_Position = 1, Seek_Position = 2 } pos_kind;
  unsigned long line;
  G_CHAR_COUNT column;
  G_XCHAR_OR_EOF prevch;
  G_XCHAR_OR_EOF peekch;
#if G_CHAR_BYTES > 1
  unsigned char rawbuf[6];
  signed short  nraw;
#endif
} MarkBuf;

/* read one character */
G_XCHAR_OR_EOF cis_getch(CIStream s);

/* return next or previous character */
G_XCHAR_OR_EOF cis_peek(CIStream s);
G_XCHAR_OR_EOF cis_prevch(CIStream s);

#if G_CHAR_BYTES == 2
G_CHAR cis_peek_short(CIStream s);
G_CHAR cis_prevch_short(CIStream s);
#else
#define cis_peek_short(s) cis_peek(s)
#define cis_prevch_short(s) cis_prevch(s)
#endif

/* remember current position */
void cis_mark(CIStream s, MarkBuf* b);

/* restore to remembered position */
void cis_restore(CIStream s, MarkBuf* b);

/* release saved position (will not be returning to it) */
void cis_release(CIStream s, MarkBuf* b);

void cis_rewind(CIStream s);

void skip_whitespace( CIStream s );

/* create stream */
CIStream make_file_input_stream(G_FILE* f, const G_SCHAR* path);
CIStream make_string_input_stream(const G_CHAR* start, G_CHAR_COUNT length,
				boolean copy);
#if defined(_USE_WCHAR)
CIStream make_narrow_string_input_stream(const char* x, int length);
#else
#define make_narrow_string_input_stream(x, len) make_string_input_stream(x, len, FALSE);
#endif
CIStream clone_input_stream( CIStream in );
CIStream copy_read_input_stream( CIStream in );

CIStream
open_input_file( const G_SCHAR* pathname, boolean binary );

/* return name of a file stream, or NULL */
const G_SCHAR* cis_pathname(CIStream s);

/* return current line and column number (first is 1) */
unsigned long cis_line(CIStream s);
G_CHAR_COUNT cis_column(CIStream s);

/* distinguishes the input file from internal streams */
boolean cis_is_file(CIStream s);

/* return modification time of file; 0 if not applicable */
time_t cis_mod_time(CIStream s);

/* test whether a file exists; return 'U', 'F', 'D' or 'V' */
char probe_pathname(const G_CHAR* pathname);

/* close stream */
void cis_close(CIStream s);

/* report a syntax or semantic error in the input */
void input_error( CIStream s, Exit_States code, const char* format, ... );

extern CIStream input_stream; /* used only for error message location */

/* ============    Output Streams ==============  */

typedef struct output_stream_struct* COStream;

/* write one character */
void cos_putch(COStream s, G_XCHAR_OR_EOF c);

/* write some number of spaces */
void cos_spaces(COStream s, int n);

/* write null-terminated string */
void cos_puts(COStream s, const G_SCHAR* x);
#if defined(_USE_STDIO)
#define cos_putss(s,c)  cos_puts(s,c)
#define cos_put_utf8 cos_puts
#else
void cos_putss(COStream s, const char* x); /* put string of 8-bit characters */
void cos_put_utf8(COStream s, const char* x); /* put UTF-8 string */
#endif

/* write null-terminated 8-bit string, followed by new line and flush */
void cos_put_line(COStream s, const char* x);
/* output close quote and end of line to stderr */
void putch_end_quote(G_XCHAR_OR_EOF c);

/* write arbitrary data */
void cos_put_len(COStream s, const G_SCHAR* x, G_CHAR_COUNT len);

/* start new line if not already at start of line */
void cos_freshline( COStream s );

/* return the last character written, or EOF if nothing written yet */
G_XCHAR_OR_EOF cos_prevch(COStream s);

/* create stream */
COStream make_file_output_stream (G_FILE* f, const G_SCHAR* path);

COStream
open_output_file( const G_CHAR* pathname, boolean binary );

extern G_SCHAR* backup_suffix;

/* return name of a file stream, or NULL */
const G_SCHAR* cos_pathname(COStream s);

/* close stream */
void cos_close(COStream s);

/* create buffer to hold output and then read it back in */
COStream make_buffer_output_stream(void);
CIStream convert_output_to_input( COStream out );
#ifdef LUA
char* cos_whole_string ( COStream s );
#endif
G_CHAR* cis_whole_string ( CIStream in );
unsigned cis_length( CIStream in );
#if G_CHAR_BYTES == 2
unsigned cis_character_count( CIStream in );
#else
#define cis_character_count(s) cis_length(s)
#endif

/* column number (1 when at beginning of line) */
G_CHAR_COUNT cos_column(COStream s);

/* number of characters written so far to output buffer stream */
G_CHAR_COUNT cis_out_length( COStream out );

/* copy input stream to output stream */
void cos_copy_input_stream(COStream out, CIStream in);

/* return backup path to read from when input path is same as output */
const G_CHAR*
get_backup_path(const G_CHAR* input_pathname, COStream output_stream);

 /* ==========  utilities  ===========  */

void
merge_pathnames( COStream out, boolean just_dir,
		 const G_SCHAR* dpath, const G_SCHAR* npath, const G_SCHAR* tpath );

void
expand_wildcard ( const G_CHAR* file_spec, COStream out );

void
init_stdio_streams( void );
     
 /* ==========  globals  ===========  */

extern COStream stdout_stream;
extern CIStream stdin_stream;
COStream get_stderr_stream(void);
  
#endif
