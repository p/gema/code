#ifndef IO_MACROS_H
#define IO_MACROS_H

#if defined(_USE_ICU)
#include "ustdio.h" /* for ICU's UFILE, includes stdio.h */
#else
#include <stdio.h> /* for FILE */
#endif

#if defined(_USE_ICU)
#define G_FILE UFILE
#else
#define G_FILE FILE
#endif

/* There's no reliable and useful maximum length for a path name, but
   I've never seen anyone need more than this. */
#define G_MAX_PATH 1024
/*
 * Emulate stdio file operations
 */

#if defined(_USE_ICU)
#define G_FOPEN(name, perm, locale, codepage) u_fopen_u(name, perm, locale, codepage)
#elif defined(_USE_WCHAR)
#define G_FOPEN(name, perm, locale, codepage) _wfopen(name, perm)
#else
#define G_FOPEN(name, perm, locale, codepage) fopen(name, perm)
#endif

#if defined(_USE_WCHAR)
#define G_STAT(path, buffer) _wstat(path, buffer)
#define G_REMOVE(path) _wremove(path)
#define G_RENAME(old, new) _wrename(old, new)
#else
#define G_STAT(path, buffer) stat(path, buffer)
#define G_REMOVE(path) remove(path)
#define G_RENAME(old, new) rename(old, new)
#endif

#if defined(_USE_ICU)
#define G_FCLOSE(f) u_fclose(f)
#else
#define G_FCLOSE(f) fclose(f)
#endif

#if defined(_USE_ICU)
#define G_FINIT(f, locale, codepage) u_finit(f, locale, codepage)
#else
#define G_FINIT(f, locale, codepage) (f)
#endif

/* G_FGETFILE: G_FILE -> FILE* */
#if defined(_USE_ICU)
#define G_FGETFILE(f) u_fgetfile(f)
#else
#define G_FGETFILE(f) (f)
#endif

#if defined(_USE_ICU)
#define G_FGETLOCALE(f) u_fgetlocale(f)
#else
#define G_FGETLOCALE(f) NULL
#endif

#if defined(_USE_ICU)
#define G_FSETLOCALE(locale, f) u_fsetlocale(locale, f)
#else
#define G_FSETLOCALE(locale, f) NULL
#endif

#if defined(_USE_ICU)
#define G_FGETCODEPAGE(f) u_fgetcodepage(f)
#else
#define G_FGETCODEPAGE(f) NULL
#endif

#if defined(_USE_ICU)
#define G_FSETCODEPAGE(codepage, f) u_fsetcodepage(codepage, f)
#else
#define G_FSETCODEPAGE(codepage, f) NULL
#endif

/*
 * G_FGETS: G_CHAR* x int x G_FILE -> G_CHAR*
 */
#if defined(_USE_ICU)
#define G_FGETS(s, n, f) u_fgets(f, n, s)
#elif defined(_USE_WCHAR)
#define G_FGETS(s, n, f) fgetws(s, n, f)
#else
#define G_FGETS(s, n, f) fgets(s, n, f)
#endif

/*
 * G_FILENO: G_FILE* -> int
 *
 * Not strictly part of stdio
 */
#if defined(_USE_ICU)
#define G_FILENO(fp) fileno(u_fgetfile(fp))
#else
#define G_FILENO(fp) fileno(fp)
#endif

/* 
 * Emulate stdio character I/O functions
 */

#if defined(_USE_ICU)
#define G_FGETC(f) u_fgetc(f)
#else
#define G_FGETC(f) getc((f))
#endif

#if defined(_USE_ICU)
#define G_FPUTC(c, f) u_fputc((c), (f))
#else
#define G_FPUTC(c, f) putc((c), (f))
#endif

#if defined(_USE_ICU)
#define G_FPUTS(s, f) u_fputs((s), (f))
#elif defined(_USE_WCHAR)
#define G_FPUTS(s, f) fputws((s), (f))
#else
#define G_FPUTS(s, f) fputs((s), (f))
#endif

#if defined(_USE_ICU)
#define G_FPRINTF u_fprintf
#else
#define G_FPRINTF fprintf
#endif

/* 
 * Emulate stdio direct I/O functions
 */

/* G_FREAD: G_CHAR* x int x G_FILE -> int */
/* Notice that G_FREAD only takes no objectsize arg, unlike stdio fread */
#if defined(_USE_ICU)
#define G_FREAD(array, numobj, file) u_file_read(array, numobj, file)
#else
#define G_FREAD(array, numobj, file) fread(array, sizeof(char), numobj, file)
#endif

/* 
 * Emulate stdio file positioning functions
 */

/* G_FSEEK: G_FILE* x long x int -> int */
#if defined(_USE_ICU)
#define G_FSEEK(fp, offset, origin) fseek(u_fgetfile(fp), offset, origin) 
#else
#define G_FSEEK(fp, offset, origin) fseek(fp, offset, origin) 
#endif

#if defined(_USE_ICU)
#define G_FTELL(fp) ftell(u_fgetfile(fp))
#else
#define G_FTELL(fp) ftell(fp)
#endif

/* 
 * Emulate stdio error functions
 */

#if defined(_USE_WCHAR)
 #if defined(_WIN32)
  #define G_PERROR(message) _wperror(message)
 #else
  #define G_PERROR(message) perror(message_string(message))
 #endif
#else
#define G_PERROR(message) perror(message)
#endif

/* G_FEOF: G_FILE* -> int */
#if defined(_USE_ICU)
#define G_FEOF(fp) feof(u_fgetfile(fp))
#else
#define G_FEOF(fp) feof(fp)
#endif

#endif /* IO_MACROS_H */
