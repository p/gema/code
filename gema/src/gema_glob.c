/* generalized macro processor */

/* $Id $ */

/*********************************************************************
  This file is part of "gema", the general-purpose macro translator,
  written by David N. Gray <dgray@acm.org> in 1994 and 1995.
  You may do whatever you like with this, so long as you retain
  an acknowledgment of the original source.
 *********************************************************************/

#include "gema_glob.h"

#ifndef GEMA_USAGE

#define GEMA_USAGE 	  "Syntax: gema [ -p <patterns> ] [ -f <pattern-file> ] [ <in-file> <out-file> ]\n"\
                      "Copies standard input to standard output, performing\n"\
	                  "text substitutions specified by a series of patterns\n"\
                      "specified in -p options or in files specified by "\
                      "-f options.\n"
#endif


COStream output_stream;
COStream stdout_stream;
CIStream stdin_stream;

boolean keep_going = FALSE;
boolean binary = FALSE;
Exit_States exit_status = EXS_OK;

void usage(void) {
  fprintf(stderr, GEMA_USAGE);
  pattern_help( stderr );
}

static struct switches {
  const char* name;
  boolean * var;
} switch_table[] =
  { { "line", &line_mode },
    { "b", &binary },
    { "k", &keep_going },
    { "match", &discard_unmatched },
    { "i", &case_insensitive },
    { "w", &ignore_whitespace },
    { "t", &token_mode },
    { "arglen", &MAX_ARG_LEN },
#ifndef NDEBUG
    { "debug", &debug_switch },
#endif
#ifdef TRACE
    { "trace", &trace_switch },
#endif
#if G_CHAR_BYTES > 1
    { "bom", &bom_switch }, /* write Byte Order Mark to Unicode output file? */
#endif
    { NULL, NULL } };

int*
find_switch(const char* arg) {
  /* given a switch name, return a pointer to the value. */
  struct switches *p;
  for ( p = &switch_table[0] ; p->name != NULL ; p++ )
    if ( stricmp(arg, p->name)==0 )
      return p->var;
  return NULL;
}

static struct parms {
  const char* name;
  G_SCHAR** var;
  SetOfChar* varset;
  Encoding* enc;
} parm_table[] =
  { { "idchars", &idchars, &charset_for_letter('I'), NULL },
    { "filechars", &filechars, &charset_for_letter('F'), NULL },
    { "backup", &backup_suffix, NULL, NULL },
#if G_CHAR_BYTES > 1
    { "inenc",  NULL, NULL, &input_encoding },
    { "outenc", NULL, NULL, &output_encoding },
#endif
    { NULL, NULL, NULL, NULL } };

boolean
set_parm(const char* name, const G_SCHAR* value) {
  struct parms *p;
  for ( p = &parm_table[0] ; p->name != NULL ; p++ )
    if ( stricmp(name, p->name)==0 ) {
      SetOfChar* sp = p->varset;
      G_SCHAR** vp = p->var;
      if ( vp != NULL ) { /* store as a string */
#if 0 /* don't bother with this to avoid copying the initial value. */
        if ( *vp != NULL )
          free(*vp);
#endif
        *vp = g_str_dup(value);
      } /* end store as a string */
      if ( sp != NULL ) { /* store as a character set */
        if ( *sp != NULL )
          free_char_set(*sp);
        /* For compatibility with version 1, here hyphen is just an
           ordinary character, not a range. */
        *sp = make_char_set(value, /* hyphen_is_range = */ FALSE,
                            /* terminator = */ FALSE, /* endp = */ NULL);
      } /* end store as a character set */
#if G_CHAR_BYTES > 1
      if ( p->enc != NULL ) { /* store as name of a file encoding */
        char valstr[40];
        Encoding e;
        wide_to_utf8(value, valstr, sizeof(valstr));
        e = find_encoding(valstr);
        if ( e != NULL )
          *p->enc = e;
        else {
          input_error(input_stream, EXS_UNDEF,
                       "Undefined file encoding \"%.49s\"\n", valstr );
          fputs("  Valid values are: ", stderr);
          show_encodings();
          fputc('\n', stderr);
          if ( ! keep_going )
            exit(exit_status);
        }
      } /* end file encoding */
#endif      
      return TRUE;
    } /* end name matches */
  return FALSE;
}

static SetOfChar* find_char_set(const G_CHAR* name)
{
  /* Name is limited to just a single letter. */
  if ( name[0] > 'z' || name[1] != G_NULL_CHAR )
    return NULL;
  else {
    char name_letter = toupper((char)name[0]);
    if ( name_letter < 'A' || name_letter > 'Z' )
      return NULL;
    return &charset_for_letter(name_letter);
  }
}

boolean
set_char_set(const G_CHAR* name, const G_CHAR* string) {
  SetOfChar* sp = find_char_set(name);
  if ( sp == NULL )
    return FALSE; /* invalid name */
  if ( *sp != NULL )
    free_char_set(*sp);
  *sp = make_char_set(string, /* hyphen_is_range = */ TRUE,
		      /* terminator = */ FALSE, /* endp = */ NULL);
  return TRUE;
}

/* Callback function needed because of information hiding -- the file that
   knows the internals of character sets doesn't know about streams. */
static void show_range(const struct char_range* rp, void* arg)
{
  COStream out = (COStream)arg;
  G_XCHAR first = rp->first;
  G_XCHAR last  = rp->last;
  if ( first == '-' ) {
    cos_putss(out, "---");
    if ( last <= first )
      return;
    else first = first + 1;
  }
  cos_putch(out, first);
  if ( last > first ) {
    if ( last == '-' )
      cos_putch(out, '-');
    else {
      int diff = last - first;
      if ( diff > 1 ) {
        if ( diff == 2 )
          cos_putch(out, first+1);
        else cos_putch(out, '-');
      }
    }
    cos_putch(out, last);
  }
}

boolean
get_char_set(const G_CHAR* name, void* outstream) {
  SetOfChar* sp = find_char_set(name);
  if ( sp == NULL )
    return FALSE; /* invalid name */
  show_char_set(*sp, show_range, outstream);
  return TRUE;
}

#ifdef MSDOS
#ifdef __TURBOC__
#include <dir.h>
#endif
#ifdef _WIN32
#include <io.h>
#else
#include <dos.h>
#endif
#endif /* MSDOS */

void
expand_wildcard ( const G_CHAR* file_spec, COStream out ) {
/*  Expand wild card file name on MS-DOS or Windows.
    (On Unix, this is not needed because expansion is done by the shell.)
 */
#ifdef MSDOS						       
#if defined(_FIND_T_DEFINED)  /* Microsoft C on MS-DOS */
  struct _find_t fblk;
  if ( _dos_findfirst( file_spec, _A_NORMAL|_A_ARCH|_A_RDONLY, &fblk )
       == 0 ) {
    /* first match found */
    do {
      merge_pathnames( out, FALSE, file_spec, fblk.name, NULL );
      cos_putch( out, '\n' );
    }
    while ( _dos_findnext( &fblk ) == 0 );
  }
  else
#elif defined(_WIN32)  /* Microsoft C/C++ on Windows NT */
  intptr_t handle;
 #if defined(_USE_WCHAR)
  struct _wfinddata_t fblk;
  handle = _wfindfirst( file_spec, &fblk );
 #else
  struct _finddata_t fblk;
  handle = _findfirst( file_spec, &fblk );
 #endif  
  if ( handle != -1 ) {
    /* first match found */
    do {
      if ( !(fblk.attrib & _A_SUBDIR) ) {		   
	merge_pathnames( out, FALSE, file_spec, fblk.name, NULL );
	cos_putch( out, '\n' );
      }
    }
    while (
 #if defined(_USE_WCHAR)
	   _wfindnext( handle, &fblk ) == 0
 #else
	   _findnext( handle, &fblk ) == 0
 #endif
	    );
    _findclose( handle );
  }
  else
#elif defined(__TURBOC__)  /* Borland Turbo C */
  struct ffblk fblk;
  if ( findfirst( file_spec, &fblk, FA_ARCH|FA_RDONLY ) == 0 ) {
    /* first match found */
    do {
      merge_pathnames( out, FALSE, file_spec, fblk.ff_name, NULL );
      cos_putch( out, '\n' );
    }
    while ( findnext( &fblk ) == 0 );
  }
  else
#endif /* Borland */
  if ( G_STRCHR(file_spec,'*') != NULL )
    fprintf( stderr, "No match for \"%s\"\n", message_string(file_spec) );
  else
#endif /* MSDOS */
  {
    cos_puts( out, file_spec );
    cos_putch( out, '\n' );
  } 
}
