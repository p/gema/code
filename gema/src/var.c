
/* variables */

/*********************************************************************
  This file is part of "gema", the general-purpose macro translator,
  written by David N. Gray <dgray@acm.org> in 1994 and 1995.
  Extended for Unicode support in 2023.
  You may do whatever you like with this, so long as you retain
  an acknowledgment of the original source.
 *********************************************************************/

/* suppress Microsoft warnings about "insecure" string functions */
#define _CRT_SECURE_NO_WARNINGS 1

#include <assert.h>

#include "bool.h"
#include "type_macros.h"
#include "ctype_macros.h"
#include <ctype.h>
#include "string_macros.h"

#include "var.h"
#include "util.h"
#include "cstream.h"  /* for input_error */
#include "main.h"  /* for EXS_MEM */

#if defined (_USE_ICU)
#include <unum.h>		/* used in incr_var */
#endif

#ifdef TEST_VAR
G_FILE * errFile;
#endif

struct var_struct {
  const G_SCHAR* name;
  G_SCHAR* value;
  struct var_struct* next;
  unsigned length;
};

varp first_bound_var = NULL;
static varp last_bound_var = NULL;

static varp first_global_var = NULL;

static varp find_var ( const G_SCHAR* name ) {
  varp x;
  for ( x = first_bound_var ; x != NULL ; x = x->next )
    if ( G_STRCMP(name, x->name) == 0 )
      return x;
  return NULL;
}

const G_SCHAR* set_var( const G_SCHAR* name, const G_SCHAR* value,
		       G_CHAR_COUNT len ) {
  varp x;

#if defined(TEST_VAR) && defined(DEBUG_OUTPUT)
#if defined (_USE_ICU)
      u_fprintf(errFile, "set_var(%U (%x), %U (%x), %i)\n",
		name, name, value, value, len);
#elif defined(_USE_STDIO)
      fprintf(errFile, "set_var(%s (%p), %s (%p), %i)\n",
	      name, name, value, value, len);
#else
      G_FPRINTF(errFile, "set_var(%s (%p), %s (%p), %i)\n",
		message_string(name), name, message_string(value), value, len);
#endif
#endif /* defined(TEST_VAR) && defined(DEBUG_OUTPUT) */

  x = find_var(name);
  if ( x != NULL ) {
    if ( len != x->length ) {
      free(x->value);
      x->value = g_str_dup_len(value,len);
    }
    else G_STRCPY(x->value, value);
  }
  else {
    x = (varp)allocate(sizeof(struct var_struct), MemoryVar);
    x->next = first_global_var;
    x->name = g_str_dup(name);
    x->value = g_str_dup_len(value,len);
    first_global_var = x;
    if ( last_bound_var != NULL ) {
      assert ( last_bound_var->next == x->next );
      last_bound_var->next = x;
    }
    else first_bound_var = x;
  }
  x->length = len;
  return x->value;
}

void bind_var( const G_SCHAR* name, const G_SCHAR* value, G_CHAR_COUNT len ) {
  varp x;
  x = (varp)allocate(sizeof(struct var_struct), MemoryVar);
  x->next = first_bound_var;
  x->name = g_str_dup(name);
  x->value = g_str_dup_len(value,len);
  first_bound_var = x;
  if ( last_bound_var == NULL )
    last_bound_var = x;
  x->length = len;
}

static void
delete_var( varp x ) {
      free(x->value);
      free((G_SCHAR*)x->name);
      free(x);
}

void unbind_var( const G_SCHAR* name ) {
  varp x, prev;
  prev = NULL;
  for ( x = first_bound_var ; x != first_global_var ; x = x->next ) {
    if ( G_STRCMP(name, x->name) == 0 ) {
      if ( x == last_bound_var )
	last_bound_var = prev;
      if ( prev == NULL )
	first_bound_var = x->next;
      else prev->next = x->next;
      delete_var(x);
      return;
    }
    prev = x;
  }
  input_error( input_stream, EXS_OK, "No binding for @unbind{%s}\n",
	       message_string(name));
}

const G_SCHAR* get_var( const G_SCHAR* name, boolean undefined_ok,
                       size_t* lenpt ) {
  varp x;
  x = find_var(name);
  if ( x != NULL ) {
    *lenpt = x->length;
    return x->value;
  }
  else if ( undefined_ok )
    return NULL;
  else {
    input_error( input_stream, EXS_UNDEF,
		 "Undefined variable: \"%s\"\n", message_string(name));
    *lenpt = G_STRLEN(name);
    return name;
  }
}

void incr_var( const G_SCHAR* name, int amount ){
  const G_SCHAR* val;
  size_t length;
  val = get_var(name,FALSE,&length);

#if defined(TEST_VAR) && defined(DEBUG_OUTPUT)
#if defined (_USE_ICU)
      u_fprintf(errFile, "incr_var(%U (%x), %i), init val %U (%x)\n",
		name, name, amount, val, val);
#elif defined(_USE_STDIO)
      fprintf(errFile, "incr_var(%s (%x), %i), init val %s (%x)\n",
	      name, name, amount, val, val);
#else
      G_FPRINTF(errFile, "incr_var(%s (%x), %i), init val %s (%x)\n",
		message_string(name), name, amount, message_string(val), val);
#endif
#endif /* defined(TEST_VAR) && defined(DEBUG_OUTPUT) */

  if ( val == name ) /* error */
    return;
  else if ( G_STRLEN(val) < 86 ) {
    G_SCHAR buf[90];
    long n;
    G_SCHAR* end;
    G_SCHAR* bp = buf;
    const G_SCHAR* start;
    for ( start = val ; ; start++ ) {
      G_SCHAR ch = *start;
      if ( G_IS(digit)(ch) || ch == '-' || ch == '+' ) { /* increment number */
#if defined (_USE_ICU)
	UErrorCode ecode = 0;
	UNumberFormat* nf;	/* May want this to be a global: open
				   only once */
	int32_t numlen = 0;
	int32_t posn = 0;

	nf = unum_open(UNUM_DEFAULT, NULL, &ecode);
	if (ecode) {
	  /* Exercising this code yeilds core dump! -- TAO */
	  input_error(input_stream, EXS_NUM,
		      "Can't increment variable \"%.99x\": %x\n",
		      name, errorName(ecode));
	} else {
	  n = unum_parse(nf, start, u_strlen(start), &posn, &ecode);
	  if (ecode) {
	    input_error(input_stream, EXS_NUM,
			"Can't increment variable \"%.99s\": %s\n",
			name, errorName(ecode));
	  } else {
	    n++;
	    end = start + posn;
	    numlen = unum_format(nf, n, bp, 90 - (bp - buf), 0, &ecode);
	    if (ecode) {
	      input_error(input_stream, EXS_NUM,
			  "Can't increment variable \"%.99s\": %s\n",
			  name, errorName(ecode));
	    } else {
	      /* need bounds checking on buf/bp array */
	      bp += numlen;
	      u_strncpy(bp, end, bp-buf);
	    }
	  }
	}
	unum_close(nf);
#elif defined(_USE_STDIO)
      	n = strtol( start, &end, 10 );
      	sprintf(bp,"%ld%s", n+amount, end);
#elif defined(_USE_WCHAR)
	{
	char nbuf[20];
	char* np;
      	n = G_STRTOL( start, &end, 10 );
      	sprintf(nbuf,"%ld", n+amount);
	for ( np = nbuf; *np != '\0' ; np++ )
	  *bp++ = *np;
	G_STRCPY(bp, end);
	}
#endif
	break;
      }
      else *bp++ = ch;
      if ( ch == '\0' ) { /* no number found */
	if ( bp > buf && G_IS(alpha)(bp[-2]) ) { /* increment alphabetic counter */
	  G_SCHAR tc, nc;
	  tc = bp[-2];
	  nc = (G_SCHAR)(tc + amount);
	  if ( G_IS(alpha)(nc) )
	    bp[-2] = nc;
	  else if ( amount == 1 && tolower(tc) == 'z' ) {
	      G_CHAR a = (G_CHAR)(tc - ('z'-'a'));
	      if ( bp == buf+2 || !G_IS(alpha)(bp[-3]) ) {
		bp[-2] = a;
		bp[-1] = a;
		bp[0] = '\0';
	      }
	      else {
		bp[-3] = bp[-3] + 1;
		bp[-2] = a;
	      }
	  }
	  else goto no_good;
	  break;
	}
	else goto no_good;
      }
    } /* end for */
   set_var(name,buf,G_STRLEN(buf));

#if defined(TEST_VAR) && defined(DEBUG_OUTPUT)
   val = get_var(name,FALSE,&length);
#if defined (_USE_ICU)
      u_fprintf(errFile, "incr_var(%U (%x), %i), init val %U (%x)\n",
		name, name, amount, val, val);
#elif defined(_USE_STDIO)
      fprintf(errFile, "incr_var(%s (%x), %i), init val %s (%x)\n",
	      name, name, amount, val, val);
#else
      G_FPRINTF(errFile, "incr_var(%s (%x), %i), init val %s (%x)\n",
		message_string(name), name, amount, message_string(val), val);
#endif
#endif /* defined(TEST_VAR) && defined(DEBUG_OUTPUT) */
   return;
  } /* end defined value not too long */
no_good:
  input_error( input_stream, EXS_NUM,
#if defined(_USE_ICU)
		"Can't increment variable \"%U\" with value \"%U\"\n",
#else
		"Can't increment variable \"%.99s\" with value \"%.99s\"\n",
#endif
	       message_string(name), message_string(val));
}

const G_SCHAR* append_var( const G_SCHAR* name, const G_SCHAR* value,
			  G_CHAR_COUNT val_len ) {
  varp x;
  x = find_var(name);
  if ( x == NULL )
    return set_var( name, value, val_len );
  else {
    G_CHAR_COUNT old_len;
    G_CHAR_COUNT new_len;
    void *tmpptr;
    old_len = x->length;
    new_len = old_len + val_len;
    tmpptr = realloc(x->value, (new_len + 1)*sizeof(G_SCHAR));
    /* RD: avoid warning from MS Visual Studio */
    if (tmpptr == NULL ) {
      fprintf(stderr, "Out of memory for variable \"%s\"; aborting.\n",
              message_string(name));
      exit((int)EXS_MEM);
    }
    x->value = tmpptr;
    G_STRCPY( x->value + old_len, value );
    x->length = new_len;
    return x->value;
  }
}

/* crude first approximation at an "undo" facility: */

void prune_vars(varp old) {
  varp x;
  while ( first_bound_var != old &&
	  first_bound_var != first_global_var ) {
    x = first_bound_var;
    first_bound_var = x->next;
    if ( x == last_bound_var )
      last_bound_var = NULL;
    delete_var(x);
  }
}

#if 0
/* beginning of more comprehensive mechanism, not yet used: */
static VarMark* oldest_mark = NULL;
static VarMark* newest_mark = NULL;

void mark_vars(VarMark* m) {
  m->first_var = first_bound_var;
  m->previous_mark = newest_mark;
  if ( oldest_mark == NULL )
    oldest_mark = m;
  newest_mark = m;
}

void commit_vars(VarMark* m) {
  assert( m == newest_mark );
  newest_mark = m->previous_mark;
  if ( m == oldest_mark )
    oldest_mark = NULL;
}

void restore_vars(VarMark* m) {
  varp x;
  assert( m == newest_mark );
  while ( first_bound_var != m->first_var &&
	  first_bound_var != first_global_var ) {
    x = first_bound_var;
    first_bound_var = x->next;
    delete_var(x);
  }
  newest_mark = m->previous_mark;
  if ( m == oldest_mark )
    oldest_mark = NULL;
}
#endif


#ifdef TEST_VAR

/* otherwise defined in gema.c */
COStream output_stream;

boolean keep_going = FALSE;
boolean binary = FALSE;
Exit_States exit_status = EXS_OK;

/* otherwise defined in match.c */
CIStream input_stream = NULL;

typedef struct vartest {
  G_SCHAR *name;
  G_SCHAR *value;
} varTest;

int
main(int argc, char ** argv)
{
  int rc = 0;
  int i = 0;

  G_CHAR testname0[] = {'n', 'u', 'm', '0', '\0'};
  G_CHAR testname1[] = {'n', 'u', 'm', '1', '\0'};
  G_CHAR testname2[] = {'n', 'u', 'm', '2', '\0'};
  G_CHAR testname3[] = {'n', 'u', 'm', '3', '\0'};
  G_CHAR testname4[] = {'n', 'u', 'm', '4', '\0'};
  G_CHAR testname5[] = {'s', 't', 'r', '0', '\0'};
  G_CHAR testname6[] = {'s', 't', 'r', '1', '\0'};
  G_CHAR testname7[] = {'s', 't', 'r', '2', '\0'};
  G_CHAR testname8[] = {'s', 't', 'r', '3', '\0'};
  G_CHAR testname9[] = {'s', 't', 'r', '4', '\0'};

  G_CHAR teststr0[] = {'1', '7', '5', '\0'};
  G_CHAR teststr1[] = {'1', '7', '6', '\0'};
  G_CHAR teststr2[] = {'1', '7', '7', '\0'};
  G_CHAR teststr3[] = {'1', '7', '8', '\0'};
  G_CHAR teststr4[] = {'0', '7', '3', '9', '\0'};
  G_CHAR teststr5[] = {'f', 'o', 'o', '\0'};
  G_CHAR teststr6[] = {'f', 'o', 'p', '\0'};
  G_CHAR teststr7[] = {'9', '9', 'f', 'o', 'o', '\0'};
  G_CHAR teststr8[] = {'1', '0', '0', 'f', 'o', 'o', '\0'};
  G_CHAR teststr9[] = {'1', '2', '3', 'E', 'f', 'g', '\0'};

  varTest var_test[] = {
    { testname0, teststr0},
    { testname1, teststr1},
    { testname2, teststr2},
    { testname3, teststr3},
    { testname4, teststr4},
    { testname5, teststr5},
    { testname6, teststr6},
    { testname7, teststr7},
    { testname8, teststr8},
    { testname9, teststr9},
    { NULL, NULL },
  };

  G_CHAR_COUNT vlen;

  errFile = G_FINIT(stderr, NULL, NULL);

  if ( get_var(testname0, TRUE, &vlen) )
      G_FPRINTF(errFile, "Muffed get_var test with no values set\n");

  for (i = 0; var_test[i].name != NULL; i++) {
    G_CHAR * vp;
    varTest v = var_test[i];
    vp = set_var(v.name, v.value, G_STRLEN(v.value));
    if (G_STRCMP(vp, v.value) != 0) {
      rc++;
#if defined (_USE_ICU)
      u_fprintf(errFile, "Failed set_var test %d: (ptrs %x!=%x) %U != %U\n",
		i, vp, v.value, vp, v.value);
#elif defined(_USE_STDIO)
      fprintf(errFile, "Muffed set_var test %d: %p != %p\n", i, vp, v.value);
#else
      G_FPRINTF(errFile, "Muffed set_var test %d: %p != %p\n", i, vp, v.value);
#endif
    }
  }

#define VLEN 30
  {
    const G_CHAR * var0;
    const G_CHAR * var1;
    G_CHAR_COUNT vlen0;
    G_CHAR_COUNT vlen1;

    incr_var(testname0, 1);
    var0 = get_var(testname0, TRUE, &vlen0);
    var1 = get_var(testname1, TRUE, &vlen1);
    if (vlen0 != vlen1 || G_STRNCMP(var0, var1, vlen0)) {
#if defined (_USE_ICU)
      u_fprintf(errFile, "Muffed incr_var test w/ numbers\n");
#elif defined(_USE_STDIO)
      fprintf(errFile, "Muffed incr_var test w/ numbers\n");
#else
      G_FPRINTF(errFile, "Muffed incr_var test w/ numbers\n");
#endif
    }

    incr_var(testname5, 1);
    var0 = get_var(testname5, TRUE, &vlen0);
    var1 = get_var(testname6, TRUE, &vlen1);
    if (vlen0 != vlen1 || G_STRNCMP(var0, var1, vlen0))
      printf("Muffed incr_var test w/ strings\n");

    incr_var(testname7, 1);
    var0 = get_var(testname7, TRUE, &vlen0);
    var1 = get_var(testname8, TRUE, &vlen1);
    if (vlen0 != vlen1 || G_STRNCMP(var0, var1, vlen0))
      printf("Muffed incr_var test w/ strings\n");
  }

  exit(rc);
}

#endif /* TEST_VAR */
