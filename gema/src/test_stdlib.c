#include "bool.h"
#include "type_macros.h"
#include "io_macros.h"
#include "stdlib_macros.h"

#include "var.h"
#include "util.h"
#include "cstream.h"  /* for input_error */
#include "main.h"  /* for EXS_MEM */

/* otherwise defined in gema.c */
COStream output_stream;

boolean keep_going = FALSE;
boolean binary = FALSE;
Exit_States exit_status = EXS_OK;

/* otherwise defined in match.c */
CIStream input_stream = NULL;

typedef struct strtoltest {
  G_CHAR *string;
  int base;
  long result;
} strtolTest;

int
main(int argc, char ** argv)
{
  int rc = 0;
  int i = 0;

  G_CHAR teststr0[] = {'0', '\0'};
  G_CHAR teststr1[] = {'\0'};
  G_CHAR teststr2[] = {'\0'};
  G_CHAR teststr3[] = {'1', '7', '5', '\0'};
  G_CHAR teststr4[] = {'0', 'x', '1', '7', 'a', 'B', '\0'};
  G_CHAR teststr5[] = {'\0'};
  G_CHAR teststr6[] = {'0', '7', '3', '9', '\0'};
  G_CHAR teststr7[] = {'6', '5', '\0'};
  G_CHAR teststr8[] = {'0', 'x', '2', '3', 'd', 'C', '\0'};
  G_CHAR teststr9[] = {'1', '2', '3', 'E', 'f', 'g', '\0'};

  strtolTest strltol_test[] = {
    { teststr0, 0, 0 },
    { teststr1, 10, 0 },
    { teststr2, 0, 0 },
    { teststr3, 10, 175 },
    { teststr4, 0 , 0x17aB },
    { teststr5, 0, 0 },
    { teststr6, 0, 073 },
    { teststr7, 7, (6*7)+5 },
    { teststr8, 16, 0x23dC },
    { teststr9, 16, 0x123EF },
    { NULL, 0, 0 },
  };
    
  for (i = 0; strltol_test[i].string != NULL; i++) {
    strtolTest test = strltol_test[i];
    int result = G_STRTOL(test.string, NULL, test.base);
    if (result != test.result) {
      rc++;
      printf("Muffed test %d: %d (hex %x)\n", i, result, result);
    }
  }

  exit(rc);
}
