#ifndef CTYPE_MACROS_H
#define CTYPE_MACROS_H

#if defined(_USE_ICU)
#define G_IS(test) u_is ## test
#elif defined(_USE_STDIO)
#include <ctype.h>
#define G_IS(test) is ## test
#elif defined(_USE_WCHAR)
#include <wchar.h>
#define G_IS(test) isw ## test
#else
#error "Must define one of _USE_ICU, _USE_STDIO, or _USE_WCHAR"
#endif

#if defined(_USE_ICU)
#define G_TOUPPER(c) u_toupper(c)
#define G_TOLOWER(c) u_tolower(c)
#elif defined(_USE_STDIO)
#define G_TOUPPER(c) toupper(c)
#define G_TOLOWER(c) tolower(c)
#elif defined(_USE_WCHAR)
#include <wctype.h>
#define G_TOUPPER(c) towupper(c)
#define G_TOLOWER(c) towlower(c)
#endif

#endif /* CTYPE_MACROS_H */
