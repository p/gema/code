
#ifndef UTIL_H
#define UTIL_H

#include <stdlib.h>

#include "bool.h"

#include "type_macros.h"

#ifndef MSDOS
#if defined(__MSDOS__) || defined(_MSDOS) || defined(_WIN32)
#define MSDOS 1
#endif
#endif

typedef enum {
  MemoryPatterns, MemoryStream, MemoryInputBuf, MemoryOutputBuf,
  MemoryVar, MemoryPath, MemoryRegexp, MemoryDispatch, MemorySet
} Memory_Kinds;

#ifdef __linux__
/* On Linux, change the name of this to avoid some sort of compiler or
   linker confusion when optimized. */
#define allocate gema_mem_alloc
#endif

/* allocate memory space; does not return unless succesful */
void* allocate( size_t size, Memory_Kinds what );

/* 8-bit only, good for paths and such */
char* str_dup( const char* x ); /* return new copy of string */
char* str_dup_len( const char* x, int len );

/* String functions that accomodate 8-bit or 16-bit internal chars */
G_SCHAR* g_str_dup_len( const G_SCHAR* x, G_CHAR_COUNT len );
G_SCHAR* g_str_dup( const G_SCHAR* x );

/* directory delimiter character */
#if defined(MSDOS)
#define DirDelim '\\'
#elif defined(MACOS)
#define DirDelim ':'
#else /* assume Unix */
#define DirDelim '/'
#endif

const G_SCHAR*
pathname_name_and_type(const G_SCHAR* path);

const G_SCHAR*
pathname_type(const G_SCHAR* path);

char*
pathname_merge_directory( const G_SCHAR* path, const G_SCHAR* dir );

int
is_absolute_pathname(const G_SCHAR* path);

const G_CHAR*
relative_pathname(const G_CHAR* relative_to, const G_CHAR* file_path);

const G_SCHAR*
canonicalize_path(const G_CHAR* path);

/* set of characters */
typedef struct char_set* SetOfChar;
SetOfChar make_char_set(const G_SCHAR* str, boolean hyphen_is_range,
			G_SCHAR terminator, G_SCHAR** endp);
boolean char_is_in_set(const SetOfChar set, G_XCHAR_OR_EOF ch);
G_XCHAR char_to_other_set(G_XCHAR ch, const SetOfChar from_set,
                          const SetOfChar to_set);
void free_char_set(SetOfChar set);
struct char_range
{
  G_XCHAR first; /* first Unicode character in range */
  G_XCHAR last;  /* last  Unicode character in range */
};
SetOfChar char_set_from_ranges(struct char_range* ranges, int num);

void show_char_set(SetOfChar set,
		   void show_range(const struct char_range* rp, void* arg),
		   void* arg);

G_XCHAR g_toupper(G_XCHAR ch);
G_XCHAR g_tolower(G_XCHAR ch);

#ifdef MSDOS
#define HAS_STRICMP
#ifdef _WIN32
#define stricmp _stricmp
#endif
#endif

#ifndef HAS_STRICMP
/* unless already provided in the compiler's library */
int stricmp (const char* s1, const char* s2);
#endif

/* returns a string suitable for including in a message to stderr */
#if defined(_USE_STDIO)
#define message_string(str) str
#else
const char* message_string(const G_CHAR* ws);

char* visualize_char(char* outptr, G_XCHAR ch);

int one_wide_from_utf8( /* In: */  const unsigned char* start,
                        /* Out: */ const unsigned char** endptr);
char* wide_to_utf8(const G_CHAR* in, char* out, size_t outlength);
G_CHAR* utf8_to_wide(const char* in, G_CHAR* out, size_t outlength);

/* construct UTF-16 high surrogate */
#define HISUR(c) (0xD800 + (((c)-0x10000)>>10))
/* construct UTF-16 low surrogate */
#define LOSUR(c) (0xDC00 + ((c) & 0x3FF))
/* is an int value a UTF-16 high surrogate? */
#define IS_HI_SUR(c) (((c) & 0xFFFFFC00) == 0xD800)
/* is an int value a UTF-16 low surrogate? */
#define IS_LO_SUR(c) (((c) & 0xFFFFFC00) == 0xDC00)
/* construct an extended Unicode character from a surrogate pair */
#define COMBINE_SUR(high,low) ((((high)&0x3FF)<<10|((low)&0x3FF))+0x10000)


#endif /* end not _USE_STDIO */

#if G_CHAR_BYTES != 2
#define NEXT_XCHAR(ptr) *ptr++
#define PUT_XCHAR(ptr,c) *ptr++ = c
#define string_offset(str,num) ((str) + (num))
#else
G_XCHAR fetch_next_xchar(const G_CHAR** lpp);
#define NEXT_XCHAR(ptr) fetch_next_xchar(&ptr)
void put_next_xchar(G_CHAR** pp, G_XCHAR ch);
#define PUT_XCHAR(ptr,c) put_next_xchar(&ptr,c)
const G_CHAR* string_offset(const G_CHAR* str, int num);
#endif

#endif

