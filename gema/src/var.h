#ifndef VAR_H
#define VAR_H

#include <stddef.h>

const G_SCHAR* set_var( const G_SCHAR* name, const G_SCHAR* value,
		       G_CHAR_COUNT len );

const G_SCHAR* get_var( const G_SCHAR* name, boolean undefined_ok,
		       size_t* lenpt );

void incr_var( const G_SCHAR* name, int amount );

const G_SCHAR* append_var( const G_SCHAR* name, const G_SCHAR* value,
			  G_CHAR_COUNT len );

void bind_var( const G_SCHAR* name, const G_SCHAR* value, G_CHAR_COUNT len );

void unbind_var( const G_SCHAR* name );

typedef struct var_mark_struct{
  struct var_struct * first_var;
  struct var_mark_struct * previous_mark;
} VarMark;

void mark_vars(VarMark* m);
void commit_vars(VarMark* m);
void restore_vars(VarMark* m);

typedef struct var_struct * varp;

extern varp first_bound_var;
void prune_vars(varp old);

#endif







