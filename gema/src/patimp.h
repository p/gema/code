
/* pattern implementation internal declarations */

/*********************************************************************
  This file is part of "gema", the general-purpose macro translator,
  written by David N. Gray <dgray@acm.org> in 1994 and 1995.
  You may do whatever you like with this, so long as you retain
  an acknowledgment of the original source.
 *********************************************************************/

#include "pattern.h"

/* special characters used internally: */
  /* in both templates and actions: */
#define PT_END '\0'			/* end of template or action */
#define PT_QUOTE '\1'			/* use next character literally */
#define PT_SPACE '\2'			/* match at least one white space */
#define PT_LINE '\3'			/* match beginning or end of line */

  /* pattern matching operations in templates: */

#define PT_MATCH_ANY '\4'		/* wild card match argument */
#define PT_RECUR '\5'			/* recursively translated argument */
#define PT_SKIP_WHITE_SPACE '\6'	/* skip optional white space */
#define PT_SPECIAL_ARG	'\7'		/* predefined recognition domains */
	/* note: skip over codes 08..0D for whitspace control */
#define PT_WORD_DELIM ((char)0x0E)	/* word delimiter */
#define PT_ID_DELIM ((char)0x0F)	/* identifier delimiter */
#define PT_AUX ((char)0x10)		/* extended opcode PTX_... */
#define PT_REGEXP ((char)0x11)		/* argument as regular expression */
#define PT_MATCH_ONE ((char)0x12)	/* match a single character */
#if 0
#define PT_ARG_DELIM ((char)0x13)	/* command line argument delimiter */
#endif

  /* operations in actions: */
#define PT_PUT_ARG ((char)0x14)		/* output argument value */
	/* skip over ((char)0x15) for EBCDIC NewLine */
#define PT_DOMAIN ((char)0x16)	/* translate argument in another domain */
#define PT_SEPARATOR ((char)0x17)	/* separates arguments */
#define PT_OP ((char)0x18)		/* operator prefix, followed by OP_... */
#define PT_VAR1 ((char)0x19)		/* value of variable with 1 letter name */
#define PT_MATCHED_TEXT ((char)0x1A)	/* output all the matched text */
	/* note: skip over ((char)0x1B) for Escape character */

#define PT_ONE_OPT ((char)0x1C)	/* optimized "?" argument */

  /* special flag used during undefinition: */
#define PT_UNDEF ((char)0x1F)		/* undefine the pattern */

  /* extended template opcodes following PT_AUX: */
#define PTX_INIT '\1'		/* beginning-of-domain processing */
#define PTX_FINAL '\2'		/* end-of-domain processing */
#define PTX_BEGIN_FILE '\3'	/* beginning-of-file processing */
#define PTX_END_FILE '\4'	/* end-of-file processing */
#define PTX_ONE_LINE '\5'	/* set line mode for this template */
#define PTX_NO_CASE '\6'	/* case insensitive mode for this template */
#define PTX_POSITION '\7'	/* mark ending position of input stream */
#define PTX_NO_GOAL ((char)0x08)/* don't use rest as argument terminator */
#define PTX_JOIN ((char)0x09)	/* concatenate (override token mode) */

#ifdef LUA
#define PTX_MARK ((char)0x70)
#define PTX_MRK0 ((char)0x70)
#define PTX_MRK1 ((char)0x71)
#define PTX_MRK2 ((char)0x72)
#define PTX_MRK3 ((char)0x73)
#define PTX_MRK4 ((char)0x74)
#define PTX_MRK5 ((char)0x75)
#define PTX_MRK6 ((char)0x76)
#define PTX_MRK7 ((char)0x77)
#define PTX_MRK8 ((char)0x78)
#define PTX_MRK9 ((char)0x79)

extern char fake_nl;

#define PTX_COND ((char)0x60)
#define PTX_CND0 ((char)0x60)
#define PTX_CND1 ((char)0x61)
#define PTX_CND2 ((char)0x62)
#define PTX_CND3 ((char)0x63)
#define PTX_CND4 ((char)0x64)
#define PTX_CND5 ((char)0x65)
#define PTX_CND6 ((char)0x66)
#define PTX_CND7 ((char)0x67)

#define PTX_NCOND ((char)0x68)
#define PTX_NCND0 ((char)0x68)
#define PTX_NCND1 ((char)0x69)
#define PTX_NCND2 ((char)0x6A)
#define PTX_NCND3 ((char)0x6B)
#define PTX_NCND4 ((char)0x6C)
#define PTX_NCND5 ((char)0x6D)
#define PTX_NCND6 ((char)0x6E)
#define PTX_NCND7 ((char)0x6F)

extern unsigned char cond;

#endif

enum Operators {
 OP_NONE, /* skip 0 */
	/* variables: */
 OP_VAR,		/* value of variable */
 OP_SET,		/* set variable */
 OP_INCR,		/* increment variable */
 OP_DECR,		/* decrement variable */
 OP_VAR_DFLT,		/* variable with default if not defined */
 OP_BIND,		/* bind a variable */
 OP_UNBIND,		/* unbind a variable */
 OP_APPEND,		/* append to variable */

	/* match termination: */
 OP_EXIT,		/* end translation */
 OP_FAIL,		/* translation fails */
 OP_END_OR_FAIL,	/* end if any match, else fail */

	/* string operations: */
 OP_QUOTE,		/* quote characters for OP_DEFINE */
 OP_EQUOTE,		/* escaped quote for human readability */
 OP_LEFT,		/* left justify */
 OP_RIGHT,		/* right justify */
 OP_CENTER,		/* center string in field */
 OP_FILL_LEFT,		/* left justify in background string */
 OP_FILL_RIGHT,		/* right justify in background string */
 OP_FILL_CENTER,	/* center string in background string */
 OP_STR_CMP,		/* string comparison, case sensitive */
 OP_STRI_CMP,		/* string comparison, case insensitive */
 OP_SUBST,		/* substitution using temporary pattern */
 OP_REVERSE,		/* reverse string */
 OP_LENGTH,		/* length of string */
 OP_UPCASE,		/* convert to upper case */
 OP_DOWNCASE,		/* convert to lower case */
 OP_SUBSTRING,		/* substring */

	/* characters: */
 OP_CHARINT,		/* numeric code of character argument */
 OP_INTCHAR,		/* construct character with given code */

	/* files: */
 OP_ERR,		/* write message to stderr */
 OP_OUT,		/* write directly to current output file */
 OP_READ,		/* read from another file */
 OP_FILE,		/* name of input file */
 OP_PATH,		/* pathname of input file */
 OP_LINE,		/* current line number in input file */
 OP_COL,		/* current column number in input file */
 OP_WRITE,		/* write to alternate file */
 OP_CLOSE,		/* close alternate output file */
 OP_MODTIME,		/* modification time of input file */
 OP_OUTFILE,		/* name of output file */
 OP_OUTCOL,		/* current column number in output file */
 OP_MERGEPATH,		/* merge pathnames, dir of 1st, name of 2nd */
 OP_COMBINEPATH,	/* args are directory, name, and type */
 OP_RELPATH,		/* relative pathname (2nd arg in dir of 1st) */
 OP_PROBE,		/* test whether pathname is defined */
 OP_EXP_WILD,		/* expand wild card file name */
 OP_ENCODING,		/* input file encoding */

	/* output formatting: */
 OP_TAB,		/* skip to particular output column */
 OP_WRAP,		/* output argument after newline if needed */
 OP_SET_WRAP,		/* set wrap column and indent string */

	/* miscelaneous: */
 OP_DEFINE,		/* define patterns */
 OP_UNDEFINE,		/* undefine patterns */
 OP_DATE,		/* current date */
 OP_TIME,		/* current time */
 OP_DATIME,		/* current date and time */
 OP_GETENV,		/* get environment variable */
 OP_GETENV_DEFAULT, 	/* get env var with default value */
 OP_SHELL,		/* execute a shell command */
 OP_EXIT_STATUS,	/* set program termination code */
 OP_SET_SWITCH,		/* set numeric program option */
 OP_GET_SWITCH,		/* get value of numeric program option */
 OP_SET_PARM,		/* set program parameter with string value */
 OP_CHAR_SET,		/* define a set of characters */
 OP_GET_SET,		/* get a set of characters */
 OP_SYNTAX,		/* change syntax type of characters */
 OP_DEFAULT_SYNTAX,	/* restore default syntax table */
 OP_LOCALE,		/* set internationalization locale */
 OP_ABORT,		/* stop immediately */
 OP_HELP,		/* display command usage message */
 OP_REPEAT,		/* repeated execution */
 OP_VERSION,		/* display program version */

	/* arithmetic: */
 OP_ADD,		/* addition */
 OP_SUB,		/* subtraction */
 OP_MUL,		/* multiplication */
 OP_DIV,		/* division */
 OP_MOD,		/* modulus */
 OP_AND,		/* bitwise and */
 OP_OR,			/* bitwise or */
 OP_NOT,		/* bitwise inverse */
 OP_NUM_CMP,		/* numeric comparison */
 OP_RADIX,		/* radix conversion */

#ifdef LUA
 OP_LUA,
 OP_COL_MRK,
 OP_LINE_MRK,
 OP_NEWLINE,
 OP_RULES,
 OP_GETRULES,
#endif    

 OP_last_op
};


boolean is_operator(G_XCHAR_OR_EOF x);
boolean isident( G_XCHAR_OR_EOF ch ); /* is the character an identifier constituent? */
boolean g_is_space(G_XCHAR_OR_EOF ch);
boolean needs_word_delim(G_XCHAR_OR_EOF ch); /* needs space to separate from word? */

/* maximum number of arguments in a template: */
#define MAX_ARG_NUM 20

struct pattern_struct {
  const G_CHAR* pattern;
  const G_CHAR* action;
  struct pattern_struct * next;
  boolean is_copy;
};

#define DISPATCH_SIZE 0x60

#if G_CHAR_BYTES == 1
#define dispatch_index(ch) (((unsigned)toupper(ch)) % DISPATCH_SIZE)
#else
 #if 'a' != '\x61'
 #error "Unicode support is incompatible with a non-ASCII base character set"
 #endif
/* avoid applying toupper to values beyond its defined range */
#define dispatch_index(ch) ((((ch & ~0x1F) == 0x60) ? (unsigned)toupper(ch) : (unsigned)ch) % DISPATCH_SIZE)
#endif

struct patterns_struct {
  struct patterns_struct** dispatch;
  struct pattern_struct* head;
  struct pattern_struct* tail;
};

typedef struct pattern_struct * Pattern;

struct domain_struct {
  struct patterns_struct patterns;
  struct pattern_struct* init_and_final_patterns;
  const G_SCHAR* name;
  Domain inherits;
};

/* Maximum number of domains.  This may be modified as needed. */
#ifndef MAX_DOMAINS
#define MAX_DOMAINS 250
#endif

extern int ndomains;

extern Domain domains[MAX_DOMAINS];
#define domain_name(n) domains[n]->name
#if MAX_DOMAINS >= 256 && G_CHAR_BYTES == 1
#define SINGLE_CHAR_DOMAIN_NUM FALSE /* need two G_CHAR to hold domain number */
#else
#define SINGLE_CHAR_DOMAIN_NUM TRUE /* domain number fits into one G_GHAR */
#endif
void delete_domain(int n);

extern enum Translation_Status
  { Translate_Complete, Translate_Continue,Translate_Exited, Translate_Failed }
  translation_status;

const G_CHAR*
do_action( const G_CHAR* action, CIStream* args, COStream out);

extern Pattern current_rule;

extern G_XCHAR_OR_EOF arg_char; /* value of optimized "?" argument */

extern unsigned char fnnargs[OP_last_op];

void quoted_copy( CIStream in, COStream out );
void escaped_copy( CIStream in, COStream out );

int intern_regexp( G_CHAR* exp, CIStream in );

const G_CHAR*
regex_match(int regex_num, const G_CHAR* text, boolean start_of_line);

const G_SCHAR* regex_string(int regex_num);

unsigned
get_template_element( const G_CHAR** ap, boolean for_goal );
/* the following macros concern the result value of the above function */
#if G_CHAR_BYTES == 1  /* 8-bit characters, int could be 16 bits */
#define UPOP(op) (op << 8)
#define ISOP(x) (x & 0xFF00)
#define ENDOP 0x4000
#else  /* wide characters supported, int must be at least 32 bits */
#define UPOP(op) (op << 24)
#define ISOP(x) (x & 0xFF000000)
#define ENDOP        0x40000000
#endif
