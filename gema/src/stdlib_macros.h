#ifndef STDLIB_MACROS_H
#define STDLIB_MACROS_H

#if defined(_USE_STDIO)
#include <stdlib.h>
#endif

/* A few stdlib.h replacements */
/* Note the horrible abuse of the preprocessor */

#if defined(_USE_ICU)

static long u_strtol(const UChar* s, UChar** endp, int base) {
  UChar * start;
  UChar * next;
  int mybase;
  long result = 0;

  start = s;
  if (base > 0) {
    mybase = base;
    if (base==16 && start[0]=='0' && (start[1]=='x' || start[1]=='x'))
      start+=2;
  } else {
    if (s[0] == '0') {
      if (s[1] == 'x' || s[1] == 'X') {
	mybase = 16;
	start += 2;
      } else {
	mybase = 8;
	start += 1;
      }
    } else
      mybase = 10;
  }
  /* ASCII-centric if mybase != 10 */
  for (next = start; *next != 0; next++) {
    UChar c = *next;
    int val;
    if (mybase==10) {
      if (!u_isdigit(c))
	break;
      val = u_charDigitValue(c);
    } else if (mybase<10) {
      if ( c<'0' || ( c >= '0'+mybase ) )
	break;
      val = c - '0';
    } else {
      if ( c>='0' && c<= '9' )
	val = c - '0';
      else if ( c>='a' && ( c < ('a' + mybase - 10) ) )
	val =  c - 'a' + 10;
      else if ( c>='A' && ( c < ('A' + mybase - 10) ) )
	val =  c - 'A' + 10;
      else
	break;
    }
    result = (result * mybase) + val;
  }
  if (endp != NULL)
    * endp = next;
  return result;
}
#define G_STRTOL(s, endp, base) u_strtol(s, endp, base)

#elif defined(_USE_STDIO)
#define G_STRTOL(s, endp, base) strtol(s, endp, base)
#endif

#endif /* STDLIB_MACROS_H */
