
/* suppress Microsoft warnings about "insecure" string functions */
#define _CRT_SECURE_NO_WARNINGS 1

#include <stdlib.h>
#include <limits.h>
#include "type_macros.h"
#include "io_macros.h"
#include "string_macros.h"
#include "ctype_macros.h"
#include "util.h"
#include "main.h"

void* allocate( size_t size, Memory_Kinds what ) {
  /* note: the `what' argument is not used yet, but may be used someday to
     collect statistics on memory usage. */
  void* result;
  result = malloc(size);
  if ( result == NULL ) {
      fprintf(stderr, "Out of memory; aborting.\n");
      exit((int)EXS_MEM);
    }
  return result;
}

char* str_dup_len( const char* x, int len ) {
  char* r;
  r = allocate( (size_t)len + 1, MemoryVar);
  memcpy(r,x,len);
  r[len] = '\0';
  return r;
}

char* str_dup( const char* x ) {
  if ( x == NULL )
    return NULL;
  return str_dup_len(x, strlen(x));
}

G_SCHAR* g_str_dup_len( const G_SCHAR* x, G_CHAR_COUNT len ) {
  G_SCHAR* r;

  r = allocate( (len+1)*sizeof(G_SCHAR), MemoryVar);
  memcpy(r,x,len*sizeof(G_SCHAR));
  r[len] = G_NULL_CHAR;
  return r;
}

G_SCHAR* g_str_dup( const G_SCHAR* x ) {
  if ( x == NULL )
    return NULL;
  return g_str_dup_len(x, G_STRLEN(x));
}

const G_SCHAR*
pathname_name_and_type(const G_SCHAR* path) {
  const G_SCHAR* d;
#ifdef _WIN32
  /* For Windows, to avoid unexpected results, we need to allow use of either
     forward slash or back slash as a directory delimiter. */
  const G_SCHAR* p ;
  d = NULL;
  for ( p = path ; ; p++ ) {
    G_SCHAR c = *p;
    if ( c == '\0' )
      break;
    if ( c == '\\' || c == '/' )
      d = p;
  }
#else
  d = G_STRRCHR(path,DirDelim);
#endif
  if ( d == NULL )
    return path;
  else return d+1;
}

const G_SCHAR*
pathname_type(const G_SCHAR* path) {
  const G_SCHAR* d = G_STRRCHR(path,'.');
  if ( d == NULL )
    return NULL;
  else return d+1;
}

int
is_absolute_pathname(const G_SCHAR* path) {
  const G_SCHAR* p = path;
#ifdef MSDOS
  if ( p[1] == ':' )
    p += 2;  /* strip off drive letter */
  if ( p[0] == '/' )
    return TRUE;
#endif
  return p[0] == DirDelim;
}

const G_CHAR*
relative_pathname(const G_CHAR* relative_to, const G_CHAR* file_path) {
  /* If the two file pathnames are in the same directory, then return
     just the name and type portion of the second pathname.
     Otherwise return the whole second argument. */
  const G_CHAR* file_name;
  const G_CHAR* rel_name;
  int dir_len;
  file_name = pathname_name_and_type(file_path);
  rel_name = pathname_name_and_type(relative_to);
  dir_len = rel_name - relative_to;
  if ( dir_len == ( file_name - file_path ) &&
       G_STRNCMP(relative_to, file_path, dir_len) == 0 )
    return file_name;
  else return file_path;
}

const G_SCHAR*
canonicalize_path(const G_CHAR* inpath) {
  const G_SCHAR* path = inpath;
  if ( path[0] == '.' &&
       ( path[1] == DirDelim
#ifdef _WIN32
         || path[1] == '/'
#endif
        ) )
    return path+2;      /*   "./foo" -> "foo"   */
  else return path;
}

#ifdef TEST_UTIL
/* create a pathname by taking the directory from the first argument,
   the name from the second, and the extension from the third;
   returns char* to newly-allocated memory which caller must free. */
/* Return string allocation will waste a few bytes for simplicity. */
G_SCHAR*
merge_pathnames( boolean just_dir,
		 const G_SCHAR* dpath, const G_SCHAR* npath, const G_SCHAR* tpath ) {
  const G_SCHAR* nname = NULL;
  const G_SCHAR* ntype = NULL;
  G_SCHAR* p;
  G_SCHAR* path;
  int dlen = 0;
  int nlen = 0;
  int tlen = 0;
  int dirdelim_len = 0;
  int typedelim_len = 0;

  if ( tpath != NULL && tpath[0] != '\0' ) {
    ntype = pathname_type(tpath);
    if ( ntype == NULL )
      ntype = tpath;
    tlen = strlen(ntype);
  }

  if ( is_absolute_pathname(npath) || dpath==NULL || dpath[0]=='\0' ) {
    nname = npath;
    dlen = 0;
  }
  else {
    nname = pathname_name_and_type(npath);
    if ( just_dir ) {
      dlen = strlen(dpath);
      if (dpath[dlen-1] != DirDelim)
	dirdelim_len = 1;
    }
    else {
      G_SCHAR * tmp = pathname_name_and_type(dpath);
      dlen = tmp - dpath;
    }
  }

#ifdef MS_DOS
  if (tlen == 0) 
    nlen = strlen(nname);
  else {
    G_SCHAR * ntype = pathname_type(npath);
    if ( ntype == NULL ) {
      nlen = strlen(nname);
      typedelim_len = 1;
    }
    else
      nlen = ntype - nname;
  }
#else
  nlen = strlen(nname);
  typedelim_len = 1;
#endif

#ifdef DEBUG_UTIL
  printf("ntype == %s, tlen = %d\n", ntype, tlen);
  fflush(stdout);
#endif /* DEBUG_UTIL */
  {
    int plen = dlen + dirdelim_len + nlen + typedelim_len + tlen + 1;
    path = (G_SCHAR*) allocate(plen*sizeof(G_SCHAR), MemoryPath);
  }

  p = path;
  if (dlen) {
    strncpy(p, dpath, dlen);
    p += dlen;
    if ( dirdelim_len )
      *p++ = DirDelim;
  }
  strncpy(p, nname, nlen);
  p += nlen;
  if (tlen) {
#ifdef DEBUG_UTIL
    printf("typedelim_len = %d, ntype == %s, tlen = %d\n",
	   typedelim_len, ntype, tlen);
    fflush(stdout);
#endif /* DEBUG_UTIL */
    if ( typedelim_len )
      *p++ = '.';
    strncpy(p, ntype, tlen);
    p += tlen;
  }
  *p++ = '\0';

  return path;
}
#endif

#ifndef HAS_STRICMP
#include <ctype.h>

int stricmp (const char* s1, const char* s2){
  /* case-insensitive string comparison 
     for platforms that do not provide stricmp
   */
  int c1, c2;
  const unsigned char* u1 = (const unsigned char*)s1;
  const unsigned char* u2 = (const unsigned char*)s2;
  for ( ; ; ) {
    c1 = *u1++;
    c2 = *u2++;
    if ( c1 != c2 ) {
      c1 = toupper(c1);
      c2 = toupper(c2);
      if ( c1 != c2 )
	return c1 - c2;
    }
    if ( c1 == '\0' )
      return 0;
  }
}

#endif /* end HAS_STRICMP */

int g_cmpi (const G_CHAR* s1, const G_CHAR* s2){
  /* case-insensitive string comparison,
     taking into account user-defined letters */
  G_XCHAR c1, c2;
  const G_CHAR* p1 = s1;
  const G_CHAR* p2 = s2;
  do {
    c1 = NEXT_XCHAR(p1);
    c2 = NEXT_XCHAR(p2);
    if ( c1 != c2 ) {
      G_XCHAR c3 = g_toupper(c1);
      G_XCHAR c4 = g_toupper(c2);
      if ( c3 != c4 )
        return c3 - c4;
    }
  } while ( c1 != G_NULL_CHAR );
  return 0;
}

#if G_CHAR_BYTES > 1 /* using wide characters */

/* Convert a wide string to an 8-bit string by using UTF-8 encoding. */
/* Returns the end point in the output buffer for appending. */
char* wide_to_utf8(const G_CHAR* in, char* out, size_t outlength)
{
  const G_CHAR* inptr;
  char* outptr = out;
  const char* outlimit = out + outlength - 5;
  for ( inptr = in ; outptr <= outlimit ; ) {
    G_XCHAR wch = *inptr++;
    if ( wch == G_NULL_CHAR )
      break;
    if ( wch <= 0x7F )
      *outptr++ = (char)wch;
    else if ( wch <= 0x07FF ) {
      /* 110xxxxx 10xxxxxx */
      *outptr++ = (char)(0xC0 + (wch >> 6));
      *outptr++ = (char)(0x80 + (wch & 0x3F));
    }
    else if ( wch <= 0xFFFF  ) {
      if ( IS_HI_SUR(wch) && IS_LO_SUR(*inptr) ) {
        wch = COMBINE_SUR(wch,*inptr++);
        goto extended;
      }
      /* 1110xxxx 10xxxxxx 10xxxxxx */
      *outptr++ = (char)(0xE0 + (wch >> 12));
      *outptr++ = (char)(0x80 + ((wch >> 6) & 0x3F));
      *outptr++ = (char)(0x80 + (wch & 0x3F));
    }
    else if ( wch <= 0x1FFFFF ) { /* although maximum Unicode is 0x10FFFF */
  extended:
      /* 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx */
      *outptr++ = (char)(0xF0 + (wch >> 18));
      *outptr++ = (char)(0x80 + ((wch >> 12) & 0x3F));
      *outptr++ = (char)(0x80 + ((wch >> 6) & 0x3F));
      *outptr++ = (char)(0x80 + (wch & 0x3F));
    }
    else /* this case shouldn't be possible */
      fprintf(stderr, "Invalid character for converting to UTF-8: 0x%X\n",
              (int)wch);
  } /* end for */
  *outptr = '\0';
  return outptr;
}

/* Return one wide character converted from the beginning of a
   non-empty UTF-8 string.  
   *endptr is set to point to the next character in the input. */
int one_wide_from_utf8( /* In: */  const unsigned char* start,
                        /* Out: */ const unsigned char** endptr)
{
  int wch;
  const unsigned char* inptr = start;
  unsigned int ch1 = *inptr++;
  unsigned int ch2 = *inptr;
  unsigned int ch3;
  if ( (ch1 < 0xC2) || ((ch2 & 0xC0) != 0x80) ) {
    /* Not UTF-8, assume ISO-Latin-1*/
    wch = ch1;
  }
  else if ((ch1 & 0xE0) == 0xC0) {
    /* 110xxxxx 10xxxxxx */
    inptr++;
    wch = ((ch1 & 0x1F) << 6) | (ch2 & 0x3F);
  }
  else if ( (inptr[1] & 0xC0) != 0x80 )
    wch = ch1; /* not valid UTF-8, assume Latin-1 */
  else if ((ch1 & 0xF0) == 0xE0) {
    /* 1110xxxx 10xxxxxx 10xxxxxx */
    inptr++;
    ch3 = *inptr++;
    wch = ((ch1 & 0x0F) << 12) | ((ch2 & 0x3F) << 6) | (ch3 & 0x3F);
  }
  else if ( ((ch1 & 0xF8) == 0xF0) && (inptr[2] & 0xC0) == 0x80 ) {
    /* 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx */
    inptr++;
    ch3 = *inptr++;
    wch = ((ch1 & 7) << 18) | ((ch2 & 0x3F) << 12) |
      ((ch3 & 0x3F) << 6) | (*inptr++ & 0x3F);
  }
  else {
    /* Invalid for UTF-8 input, assume ISO-Latin-1*/
    wch = ch1;
  }
  *endptr = inptr;
  return wch;
}

/* Convert a nul-terminated string of 8-bit characters to a wide string,
   converting from UTF-8 encoding where found, or assuming ISO-Latin-1 for
   non-ASCII characters which do not form a valid UTF-8 encoding. */
/* Returns the end point in the output buffer for appending. */
G_CHAR* utf8_to_wide(const char* in, G_CHAR* out, size_t outlength)
{
  const unsigned char* inptr;
  G_CHAR* outptr = out;
  /* minus 3 to reserve room for a surrogate pair and nul */
  const G_CHAR* outlimit = out + outlength - 3;
  for ( inptr = (const unsigned char*)in ; outptr <= outlimit ; ) {
    unsigned int wch;
    unsigned int ch = *inptr;
    if ( ch == '\0' )
      break; /* reached nul termination */
    if ( ch < 0xC0 ) { /* can't be UTF-8; shortcut most common case */
      inptr++;
      wch = ch;
    }
    else {
      wch = one_wide_from_utf8(inptr, &inptr);
#if G_CHAR_BYTES < 3
      if ( wch > 0xFFFF ) {
        /* generate surrogate pair for extended character */
        *outptr++ = (G_CHAR)HISUR(wch);
        wch = LOSUR(wch);
      }
#endif
    }
    *outptr++ = (G_CHAR)wch;
  } /* end for */
  *outptr = G_NULL_CHAR;
  return outptr;
}

boolean stderr_ascii(); /* is the stderr stream limited to ASCII characters? */

/* Convert a wide string to a char string for console display. */
const char* message_string(const G_CHAR* ws)
{
  static char buf[1600]; /* circular buffer */
  static char* nextp = buf;
  char* bp = nextp;
  char* startp;
  char* endp = buf + sizeof(buf);
  if ( (endp - bp) < 520 ) {
    /* wrap around to beginning of buffer */
    bp = buf;
  }
  startp = bp;
#ifdef _WIN32
  if ( stderr_ascii() ) {  /* use escape sequence of ASCII characters */
    const G_CHAR* wptr;
    char* limit = startp + 510;
    for ( wptr = ws ; bp < limit ; ) {
      G_XCHAR wc = NEXT_XCHAR(wptr);
      if ( wc == G_NULL_CHAR )
	break;
      if ( wc <= 0xFF )
        *bp++ = wc;
      else {
	/* Do like cos_writechar_ascii_only */
        bp += sprintf(bp, "\\u{%04X}", (unsigned int)wc);
      }
    } /* end for */
    *bp = '\0';
  }
  else
#endif
    /* use UTF-8 encoding */
    bp = wide_to_utf8(ws, bp, 520);
  if ( (bp - startp) > 508 ) { /* too long; truncating */
    strcpy(bp, "...");
    bp += 4;
  }
  nextp = (char*)(((size_t)bp + 7) & ~(size_t)7); /* next word address */
  return startp;
}

#if G_CHAR_BYTES == 2
/* fetch the next extended Unicode character from a wide string */
G_XCHAR fetch_next_xchar(const G_CHAR** pp)
{
  const G_CHAR* p = *pp;
  G_XCHAR xc = *p++;
  if ( IS_HI_SUR(xc) && IS_LO_SUR(*p) )
    xc = COMBINE_SUR(xc, *p++);
  *pp = p;
  return xc;
}

/* append an extended Unicode character to a wide string */
void put_next_xchar(G_CHAR** pp, G_XCHAR ch)
{
  G_CHAR* p = *pp;
  if ( ch > 0xFFFF ) {
    *p++ = HISUR(ch);
    ch = LOSUR(ch);
  }
  *p++ = (G_CHAR)ch;
  *pp = p;
}

/* return the portion of the string after skipping num characters */
const G_CHAR* string_offset(const G_CHAR* str, int num){
  int remaining;
  const G_CHAR* ptr = str;
  for ( remaining = num ; remaining > 0 ; remaining-- ) {
    if ( IS_HI_SUR(ptr[0]) && IS_LO_SUR(ptr[1]) )
      ptr++; /* surrogate pair counts as one character */
    ptr++;
  }
  return ptr;
}
#endif /* end G_CHAR_BYTES == 2 */

#endif /* end using wide characters */


/* ====  set of characters ==== */

/* A set of characters is implemented using binary search on an ordered
   array of character ranges.  This implementation was chosen in order
   to efficiently support arbitrary Unicode characters without making
   any limiting assumptions. */

#include <assert.h>

struct char_set
{
  unsigned nranges;
  struct char_range ranges[1]; /* and more elements dynamically allocated */
};

/* Construct the internal representation of a set of characters from
   a string of characters to be included.  If hyphen_is_range is true,
   a hyphen may be used to indicate a range of characters.
   For example, "1-6" means the same as "123456". */
struct char_set* make_char_set(const G_SCHAR* str,
                               boolean hyphen_is_range,
                               G_SCHAR terminator,
                               G_SCHAR** endp)
{
  struct char_set* set;
  const G_SCHAR* sp;
  struct char_range* buffer;
  struct char_range* array;
  unsigned nbuf = 0;
  unsigned ax = 0;
  unsigned n;
  size_t slen;

  slen = G_STRLEN(str);
  if ( slen == 0 ) {  /* empty set */
    return NULL;
  }
  buffer = (struct char_range*)
    allocate(sizeof(struct char_range) * (slen+2), MemorySet);

  /* Convert the character string into an array of character ranges */
  
  for ( sp = str ; *sp != G_NULL_CHAR ; ) {
    G_XCHAR ch = *sp;
    if ( ch == terminator && sp > str ) {
      sp++; /* point to next character after terminating ']' */
      break;
    }
#if G_CHAR_BYTES == 2
    if ( IS_HI_SUR(ch) && IS_LO_SUR(sp[1]) )
      ch = COMBINE_SUR(ch,*++sp);
#endif
    buffer[nbuf].first = ch;
    if ( sp[1] == '-' && hyphen_is_range && sp[2] != terminator
         && (sp[2] != '-' || ch == '-') ) { /* range */
      G_XCHAR ch2 = sp[2];
#if G_CHAR_BYTES == 2
      if ( IS_HI_SUR(ch2) && IS_LO_SUR(sp[3]) ) {
        ch2 = COMBINE_SUR(ch2,sp[3]);
        sp++;
      }
#endif
      buffer[nbuf].last = ch2;
      if ( ch2 < ch ) {
        buffer[nbuf].first = ch2;
        buffer[nbuf].last = ch;
      }
      sp = &sp[3];
    }
    else { /* single character */
      if ( nbuf > 0 && buffer[nbuf-1].last == ch-1 )
        /* combine the common case of sequential characters to avoid 
           doing it in the n-squared sort */
        nbuf = nbuf - 1;
      buffer[nbuf].last = ch;
      sp++;
    }
    nbuf++;
  } /* end for sp */
  if ( endp != NULL )
    *endp = (G_SCHAR*)sp; /* points to terminaing null or after ']' */
  assert(nbuf>0);

  /* allocate maximum space needed */

  set = (struct char_set*)
    allocate(sizeof(struct char_set) + sizeof(struct char_range)*(nbuf-1),
             MemorySet);

  /* sort the ranges into numeric order */

  array = set->ranges;
  for ( n = 0 ; n < nbuf ; n++ ) {
    unsigned min = INT_MAX;
    unsigned min_index = nbuf;
    unsigned i;
    for ( i = 0 ; i < nbuf ; i++ ) {
      unsigned ifirst = buffer[i].first;
      if ( ifirst < min
#if G_CHAR_BYTES == 1
           && ifirst <= buffer[i].last /* not FF and 00 for processed entry */
#endif
           ) { /* smallest seen so far */
        min = ifirst;
        min_index = i;
      }
    }
    if ( ax > 0 &&
         buffer[min_index].first <= (array[ax-1].last + 1) ) {
      /* combine adjacent or overlapping ranges */
      if ( buffer[min_index].last > array[ax-1].last )
        /* extend the preceding range */
        array[ax-1].last = buffer[min_index].last;
      /* else the new range is redundant */
    }
    else {
      array[ax++] = buffer[min_index];
    }
#if G_CHAR_BYTES == 1
    buffer[min_index].first = '\xFF'; /* don't do this one again */
    buffer[min_index].last  = '\x00';
#else
    buffer[min_index].first = INT_MAX; /* don't do this one again */
#endif
  } /* end for n */
  free(buffer);
  
  set->nranges = ax;
  if ( ax < nbuf ) {
    /* reduce to the size actually used after optimization */
    set = realloc(set, sizeof(struct char_set) + 
                  sizeof(struct char_range)*(ax-1));
  }
  return set;
}

void free_char_set(SetOfChar set)
{
  if ( set != NULL )
    free(set);
}

struct char_set* char_set_from_ranges(struct char_range* ranges, int num)
{
  struct char_set* set;
  if ( ranges == NULL || num <= 0 ) { /* empty set */
    return NULL;
  }
  set = (struct char_set*)
    allocate(sizeof(struct char_set) + sizeof(struct char_range)*(num-1),
             MemorySet);
  set->nranges = num;
  memcpy(set->ranges, ranges, sizeof(struct char_range) * num);
  return set;
}

/* Is the given character in the given set? */
/* This uses a binary search of an ordered array of character ranges. */
boolean char_is_in_set(const SetOfChar set, G_XCHAR_OR_EOF ch)
{
  unsigned lowx, highx, midx; /* low, high, and middle indexes */
  if ( set == NULL || set->nranges <= 0 ) /* empty set */
    return FALSE;
  lowx = 0;
  highx = set->nranges - 1;
  if ( ch < set->ranges[lowx].first || ch > set->ranges[highx].last )
    return FALSE;
  while ( highx > lowx ) {
    midx = (lowx + highx) >> 1;
    if ( ch < set->ranges[midx].first ) {
      highx = midx - 1;
      if ( ch > set->ranges[highx].last )
        return FALSE;
    }
    else if ( ch > set->ranges[midx].last ) {
      lowx = midx + 1;
      if ( ch < set->ranges[lowx].first )
        return FALSE;
    }
    else return TRUE; /* found the character in the midx range */
  } /* end while */
  /* When only one range remaining, the character must be 
     within it or we would have returned false already. */
  assert( ch >= set->ranges[lowx].first && ch <= set->ranges[highx].last );
  return TRUE;
}

/* Map a character from one set to another.
   If the two sets have the same number of elements, and the given character
   is in the first set, return the corresponding character from the 
   second sorted set.  Otherwise, returns the null character. */
G_XCHAR char_to_other_set(G_XCHAR ch, const SetOfChar from_set,
                          const SetOfChar to_set)
{
  unsigned index, numranges;
  if ( to_set == NULL || from_set == NULL )
    return G_NULL_CHAR;
  numranges = from_set->nranges;
  if ( to_set->nranges != numranges )
    return G_NULL_CHAR;
  if ( ch > from_set->ranges[numranges-1].last )
    return G_NULL_CHAR;
  /* Not bothering with a binary search here since these sets are 
     unlikely to have more than a couple of ranges. */
  for ( index = 0 ; index < numranges ; index++ ) {
    const struct char_range *from_range, *to_range;
    from_range = &from_set->ranges[index];
    if ( ch < from_range->first )
      return G_NULL_CHAR;
    if ( ch <= from_range->last ) {
      /* The character is in the "from" set. */
      assert( ch >= from_range->first && ch <= from_range->last );
      to_range = &to_set->ranges[index];
      if ( (  to_range->last -   to_range->first) !=
           (from_range->last - from_range->first) )
        return G_NULL_CHAR;
      /* A corresponding character exists in the "to" set; return it. */
      return to_range->first + (ch - from_range->first);
    }
  } /* end for index */
  /* shouldn't get here because we checked the end of the last range before */
  return G_NULL_CHAR;
}

void show_char_set(SetOfChar set,
		   void show_range(const struct char_range* rp, void* arg),
		   void* arg)
{
  if ( set != NULL ) {
    unsigned i;
    for ( i = 0 ; i < set->nranges ; i++ ) {
      show_range(&set->ranges[i], arg);
    }
  }
}


#ifdef TEST_UTIL

static int passed = 0;
static int failed = 0;

static void test(const char* include, const char* exclude)
{
  int i;
  SetOfChar set = make_char_set(include, TRUE, '\0', NULL);
  /* printf("Set \"%s\" has %d ranges\n", include, set->nranges); */
  for ( i = 0; include[i] != '\0' ; i++ ) {
    unsigned char ci = include[i];
    if ( ci == '-' )
      ci = (include[i-1] + include[i+1]) / 2; /* middle of range */
    if ( char_is_in_set(set, ci) )
      passed++;
    else {
      printf("Failed '%c' not in \"%s\"\n", ci, include);
      failed++;
    }
  }
  for ( i = 0; exclude[i] != '\0' ; i++ ) {
    if ( !char_is_in_set(set, exclude[i]) )
      passed++;
    else {
      printf("Failed '%c' is in \"%s\"\n", exclude[i], include);
      failed++;
    }
  }
  free_char_set(set);
}

static void test_SetOfChar()
{
  test("aeiou", "bcdfghjklmnpqrstvwxyz");
  test("bcdfghjklmnpqrstvwxyz", "aeiou");
  test("0-9", " /:A");
  test("9876543210", " /:A");
  test("~!@#$%^&*()_+{}?><", "aAzZ09 \n");
  test("a-zA-Z\xC0-\xFF", "9@_[]`{}\xBF");
  test("9-0", " /:A");

  printf("Passed %d tests; failed %d\n", passed, failed);
}

typedef struct mp_args {
  boolean just_dir;
  char * dir;
  char * name;
  char * type;
} Args;

int main(char** argv, int argc) {
  char * done = "done";
  Args array[] = {
    { TRUE, "/foo", "/etc/motd", ".BAK"},
    { TRUE, "/foo/", "motd", ".BAK"},
    { TRUE, "/foo/", "motd", "BAK"},
    { TRUE, "/foo/", "motd.foo", ".BAK"},
    { TRUE, "/foo/", "motd.foo", "BAK"},
    { TRUE, "/foo", "motd.foo", NULL},
    { FALSE, "/foo/", "motd.foo", ".BAK"},
    { FALSE, "/foo", "motd.foo", NULL},
    { TRUE, "/foo/", "/etc/bargle.funk", NULL},
    { TRUE, NULL, "/etc/motd.FCS", NULL},
    { TRUE, done, done, done},
  };
  Args *args = NULL;

  int i;
  char * result;

  for ( args = array; args->dir != done; args++) {
    printf("merge_pathnames(%d, \"%s\", \"%s\", \"%s\") =\n",
           args->just_dir, args->dir, args->name, args->type);
    fflush(stdout);
    result = merge_pathnames(args->just_dir, args->dir,
                             args->name, args->type);
    printf("\t%s\n", result);
    fflush(stdout);
    free(result);
  }

  test_SetOfChar();
  exit (0);
}

#endif /* TEST_UTIL */
