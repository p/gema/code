#ifndef STRING_MACROS_H
#define STRING_MACROS_H

#include <string.h>
#if defined(_USE_ICU)
#include "uchar.h"
#include "ustring.h"
#endif

#if defined(_USE_STDIO)
#include <stdlib.h>
#endif

#if defined(_USE_ICU)
#define G_STRCPY(to, from) u_strcpy(to, from)
#elif defined(_USE_STDIO)
#define G_STRCPY(to, from) strcpy(to, from)
#elif defined(_USE_WCHAR)
#include <wchar.h>
#define G_STRCPY(to, from) wcscpy(to, from)
#endif

#if defined(_USE_ICU)
#define G_STRNCPY(to, from, n) u_strncpy(to, from, n)
#elif defined(_USE_STDIO)
#define G_STRNCPY(to, from, n) strncpy(to, from, n)
#elif defined(_USE_WCHAR)
#define G_STRNCPY(to, from, n) wcsncpy(to, from, n)
#endif

#if defined(_USE_ICU)
#define G_STRCAT(to, from) u_strcat(to, from)
#elif defined(_USE_STDIO)
#define G_STRCAT(to, from) strcat(to, from)
#elif defined(_USE_WCHAR)
#define G_STRCAT(to, from) wcscat(to, from)
#endif

#if defined(_USE_ICU)
#define G_STRNCAT(to, from, n) u_strncat(to, from, n)
#elif defined(_USE_STDIO)
#define G_STRNCAT(to, from, n) strncat(to, from, n)
#elif defined(_USE_WCHAR)
#define G_STRNCAT(to, from, n) wcsncat(to, from, n)
#endif

#if defined(_USE_ICU)
#define G_STRCMP(to, from) u_strcmp(to, from)
#elif defined(_USE_STDIO)
#define G_STRCMP(to, from) strcmp(to, from)
#elif defined(_USE_WCHAR)
#define G_STRCMP(to, from) wcscmp(to, from)
#endif

#if defined(_USE_ICU)
#define G_STRNCMP(to, from, n) u_strncmp(to, from, n)
#elif defined(_USE_STDIO)
#define G_STRNCMP(to, from, n) strncmp(to, from, n)
#elif defined(_USE_WCHAR)
#define G_STRNCMP(to, from, n) wcsncmp(to, from, n)
#endif

/* case insensitive string comparison */
#define G_STRICMP(to, from) g_cmpi(to, from)
int g_cmpi (const G_CHAR* s1, const G_CHAR* s2);

/* G_STRLEN: G_CHAR* => G_CHARCOUNT */
#if defined(_USE_ICU)
#define G_STRLEN(s) u_strlen(s)
#elif defined(_USE_STDIO)
#define G_STRLEN(s) strlen(s)
#elif defined(_USE_WCHAR)
#define G_STRLEN(s) wcslen(s)
#endif


#if defined(_USE_WCHAR)
#define G_STRTOL(string, end, base)  wcstol(string, end, base)
#define G_STRTOUL(string, end, base) wcstoul(string, end, base)
#define G_STRCHR(str, c)  wcschr(str, c)
#define G_STRRCHR(str, c) wcsrchr(str, c)
#else
#define G_STRTOL(string, end, base)  strtol(string, end, base)
#define G_STRTOUL(string, end, base) strtoul(string, end, base)
#define G_STRCHR(str, c)  strchr(str, c)
#define G_STRRCHR(str, c) strrchr(str, c)
#endif

#endif /* STRING_MACROS_H */
