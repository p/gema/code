

/*      Version identification string

	The prefix "@(#)" is searched for by the Unix "what" command.
	The characters following that will also be displayed by the 
	"-version" option or the "@version" function.

	If you make any changes to this program at all, please be
	sure to update this string also, so that your modified version
	can be distinguished from the original version.
 */

#ifndef GEMA_DATE
#define GEMA_DATE "Mar 9, 2024"
#endif

#ifndef LUA
#if defined(_USE_STDIO) /* 8-bit configuration */
const char what_string [] = "@(#)gema 1.6 " GEMA_DATE;
#else /* Unicode configuration */
const char what_string [] = "@(#)gema 2.0 " GEMA_DATE;
#endif

const char author [] = "@(#) David N. Gray <DGray@acm.org>";
#else
const char what_string [] = "@(#)gel 1.3 " GEMA_DATE;

const char author [] = "@(#) David N. Gray <DGray@acm.org>\n"\
                       "     Remo Dentato <rdentato@users.sourceforge.net>";
#endif
