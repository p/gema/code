
/* pattern matching */

/*********************************************************************
  This file is part of "gema", the general-purpose macro translator,
  written by David N. Gray <dgray@acm.org> in 1994 and 1995.
  Extended for Unicode support in 2023.
  You may do whatever you like with this, so long as you retain
  an acknowledgment of the original source.
 *********************************************************************/

#include "pattern.h"
#include "util.h"
#include "patimp.h"
#include "var.h"	/* for get_var */
#include "ctype_macros.h" /* for isalnum, isspace, etc. */
#include <ctype.h>   /* for toupper */
#include <string.h>
#include <assert.h>

boolean case_insensitive = FALSE;
boolean ignore_whitespace = FALSE;
#if !defined(_USE_ICU)
int max_ctype_char = 0x7F;
#endif

int MAX_ARG_LEN = 4096;

G_XCHAR_OR_EOF arg_char;

CIStream input_stream = NULL; /* used only for error message location */

enum Translation_Status translation_status;

SetOfChar character_sets['Z'-'A'+1];

#if defined(_USE_WCHAR)
G_SCHAR* idchars = L"_";
G_SCHAR* filechars = L"./-_~#@%+="
#if defined(MSDOS)
		L":\\"
#endif
#else
char* idchars = "_";
char* filechars = "./-_~#@%+="
#if defined(MSDOS)
		":\\"
#endif
#endif

#if defined(MACOS)
  "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89"
  "\x8a\x8b\x8c\x8d\x8e\x8f\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9a\x9b"
  "\x9c\x9d\x9e\x9f\xa0\xa1\xa2\xa3\xa4\xa5\xa6\xa7\xa8\xa9\xaa\xab\xac\xad"
  "\xae\xaf\xb0\xb1\xb2\xb3\xb4\xb5\xb6\xb7\xb8\xb9\xba\xbb\xbc\xbd\xbe\xbf"
  "\xc0\xc1\xc2\xc3\xc4\xc5\xc6\xc7\xc8\xc9\xca\xcb\xcc\xcd\xce\xcf\xd0\xd1"
  "\xd2\xd3\xd4\xd5\xd6\xd7\xd8\xd9\xda\xdb\xdc\xdd\xde\xdf\xe0\xe1\xe2\xe3"
  "\xe4\xe5\xe6\xe7\xe8\xe9\xea\xeb\xec\xed\xee\xef\xf0\xf1\xf2\xf3\xf4\xf5"
  "\xf6\xf7\xf8\xf9\xfa\xfb\xfc\xfd\xfe\xff"
#endif
		;

boolean
isident( G_XCHAR_OR_EOF ch ) { /* is the character an identifier constituent? */
  if ( ch == G_EOF )
    return FALSE;
  if ( char_is_in_set(charset_for_letter('I'), ch) )
    return TRUE;
  if ( ch > max_ctype_char )
    return FALSE;
  return G_IS(alnum)(ch);
}

boolean
g_is_space( G_XCHAR_OR_EOF ch ) { /* is this a white space character? */
  SetOfChar cset;
  if ( ch == G_EOF )
    return FALSE;
  if ( ch <= max_ctype_char && G_IS(space)(ch) )
    return TRUE;
  cset = charset_for_letter('S');
  if ( (cset != NULL) && char_is_in_set(cset, ch) )
    return TRUE;
  return FALSE;
}

boolean
needs_word_delim( G_XCHAR_OR_EOF ch ) { /* needs space to separate from word? */
  if ( ch == G_EOF )
    return FALSE;
#ifdef WCHAR_MAX
  if ( ch <= WCHAR_MAX )
#endif
    if ( G_IS(alnum)(ch) )
      return TRUE;
  if ( char_is_in_set(charset_for_letter('J'), ch) ||
       char_is_in_set(charset_for_letter('K'), ch) )
    return TRUE;
  if ( ch >= 0xC0 && ch <= 0xFF ) /* Latin-1 letters */
    return TRUE;
  return FALSE;
}

struct goal_state {
  const G_CHAR* template;
  CIStream* args;
  int options;
};

#if defined(_QC) || defined(_MSC_VER) /* Microsoft C or Quick C */
#pragma check_stack(on)
#endif

static CIStream
match_regexp ( CIStream s, int regex_num ) {
      G_XCHAR_OR_EOF lc;
      const G_CHAR* end_match;
      MarkBuf begin;
      G_CHAR buf[1024];
      G_CHAR* tp;
      G_XCHAR_OR_EOF tc;

      lc = cis_prevch(s);
      cis_mark(s, &begin);
      for ( tp = buf ; tp < buf+1023 ; ) {
	tc = cis_getch(s);
	if ( tc == G_EOF )
	  break;
	PUT_XCHAR(tp, tc);
	if ( tc == '\n' )
	  break;
      }
      *tp = '\0';
      end_match = regex_match( regex_num, buf, (lc == '\n' || lc == G_EOF ));
      cis_restore(s, &begin);
      if ( end_match == NULL )
	return NULL;
      else {
	size_t match_length;
        for ( tp = buf ; tp < end_match ; tp++ )
          {
#if G_CHAR_BYTES == 2
            G_XCHAR_OR_EOF tch = cis_getch(s);
            if ( IS_HI_SUR(*tp) && tch > 0xFFFF )
              tp++; /* don't count the low surrogate */
#else
            (void)cis_getch(s);
#endif
          }
        match_length = end_match - buf;
        buf[match_length] = G_NULL_CHAR;
        return make_string_input_stream(buf, match_length, TRUE);
      }
}

static unsigned
get_goal_char( const G_CHAR* ps ) {
  /* returns goal character, or ENDOP if no goal, or a value that
     satisfies ISOP for an operator. */
  return get_template_element(&ps,TRUE);
}

/* option bits for try_pattern */
#define MatchSwallow 1
#define MatchLine 2
#define MatchNoCase 4
#define MatchArgDelim 8

struct mark_struct {
  CIStream in;
  boolean marked;
  MarkBuf start;
};

static G_XCHAR_OR_EOF
getch_marked(struct mark_struct* ps) {
  if ( !ps->marked ) {
    cis_mark(ps->in, &ps->start);
    ps->marked = TRUE;
  }
  return cis_getch(ps->in);
}

#if defined(TRACE) || (G_CHAR_BYTES > 1)
#define SHOW_BUF_SIZE 16 /* buffer size for show_char */

char*
visualize_char(char* outptr, G_XCHAR ch) {
  char b2;
  switch(ch){
  case '\n': b2 = 'n'; break;
#if '\r' != '\n'
  case '\r': b2 = 'r'; break;
#endif
  case '\t': b2 = 't'; break;
  case '\f': b2 = 'f'; break;
  case '\b': b2 = 'b'; break;
  case '\\':
  case '\"':
  case '\'': b2 = (char)ch ; break;
  default: 
    if ( ch < 8 ) {
      b2 = (char)('0' + ch);
    }
#if G_CHAR_BYTES > 1
    else if ( ch > 0xFF ) {
      return outptr + sprintf(outptr, "\\u%04X", ch);
    }
#endif
    else if ( (ch & 0x7F) < 0x20 ) {
      return outptr + sprintf(outptr, "\\x%02X", ch);
    }
    else { /* the normal, most common case */
      *outptr++ = (unsigned char)ch;
      return outptr;
    }
  } /* end switch ch */
  *outptr++ = '\\';
  *outptr++ = b2;
  return outptr;
}
#endif

#ifdef TRACE
/* Compile with -DTRACE to enable use of the -trace option */
boolean trace_switch = FALSE;

#include <stdarg.h>
#include <limits.h>
int trace_indent = 0;
struct trace_enter_struct trace_enter = {INT_MAX,0,0,0,-1};

/* return a printable representation of the character ch */
static const char*
show_char(G_XCHAR_OR_EOF ch, char* buf) {
  if ( ch == EOF || ch == G_EOF )
    return "EOF";
  if ( ch < 0 ||
#if G_CHAR_BYTES == 1
       ch > 0xFF
#else
       ch > 0x1FFFFF
#endif
       )  /* shouldn't happen */
    sprintf(buf, "%d", ch);
  else {
    char* ptr = buf;
    *ptr++ = '\'';
    ptr = visualize_char(ptr, ch);
    *ptr++ = '\'';
    *ptr   = '\0';
  }
  return buf;
}

/* write to buf a printable representation of the string str */
static const char*
show_string2(const G_SCHAR* str, size_t strlength, char* buf) {
  size_t i;
  char* ptr;
  ptr = buf;
  for ( i = 0 ; i < strlength ; i++ ) {
    G_XCHAR ch = str[i];
#if G_CHAR_BYTES == 2
    if ( IS_HI_SUR(ch) && IS_LO_SUR(str[i+1]) )
      ch = COMBINE_SUR(ch, str[++i]);
#endif
    ptr = visualize_char(ptr, ch);
    if ( (i >= 40) && ((i+25) < strlength) ) {
      /* The string is too long, so show the first 40 characters and last 20. */
      strcpy(ptr, "...");
      show_string2(string_offset(str, strlength - 20), 20, ptr+3);
      return buf;
    }
  }
  *ptr   = '\0';
  return buf;
}

#if 0 /* not yet needed */
static const char*
show_string(const G_SCHAR* str, char* buf)
{
  return show_string2(str, G_STRLEN(str), buf);
}
#endif

const char*
show_arg_string(CIStream arg, char* buf)
{
  /* Note that an argument string can contain NUL bytes, so
     we can't rely on null termination. */
  return show_string2((const G_SCHAR*)cis_whole_string(arg), cis_length(arg), buf);
}

typedef enum { FAIL, OK } trace_kinds;

static void
trace( struct mark_struct * mp, trace_kinds kind, const char* format, ... ) {
  char charbuf[SHOW_BUF_SIZE];
  va_list args;
  va_start(args,format);
  if ( trace_switch ) {
    int n;
    fflush(stdout);
    if ( trace_indent > trace_enter.level ) {
      fprintf( stderr, "%12ld,%2d ", trace_enter.line, trace_enter.column);
      for ( n = trace_enter.level ; n > 0 ; n-- )
	fputc(' ',stderr);
      fprintf( stderr, "Try <%s> at %s\n",
	       message_string(domains[trace_enter.domain]->name),
	       show_char(trace_enter.ch, charbuf) );
    }
    trace_enter.level = INT_MAX;
    if ( mp->in != input_stream )
      fprintf( stderr, "%16s", "");
    else {
      if ( mp->marked )
	fprintf( stderr, "%4ld,%2d:", mp->start.line, mp->start.column );
      else fprintf( stderr, "%8s","");
      fprintf( stderr, "%4ld,%2d ", cis_line(mp->in), cis_column(mp->in));
    }
    for ( n = trace_indent ; n > 0 ; n-- )
      fputc(' ',stderr);
    if ( kind == FAIL ) fprintf( stderr, "Failed ");
    vfprintf(stderr, format, args);
    if ( kind == FAIL ) fprintf( stderr, " at %s\n",
				 show_char(cis_peek(mp->in), charbuf) );
    fflush(stderr);
  }
  va_end(args);
}

#define TRACE_FAILURE(string) \
  if(trace_switch && !(local_options & MatchArgDelim))	\
    trace(&marker,FAIL,string)

static const char trace_match_format[] = "Matched %s as \"%s\"\n";

#define TRACE_MATCH(description, value) \
  if ( trace_switch ) { \
    char trace_match_buf[250]; \
    trace(&marker, OK, trace_match_format, description, \
          show_arg_string(value, trace_match_buf));     \
  }

#else
#define TRACE_FAILURE(string)
#define TRACE_MATCH(description, value)
#endif

static int
match_to_stream(struct mark_struct* ps, CIStream arg, int local_options) {
  G_XCHAR_OR_EOF ac, kc;
  CIStream in;

  in = ps->in;
  while ( ( ac = cis_getch(arg) ) != G_EOF ) {
loop1:
    kc = cis_peek(in);
    if ( ac == kc ||
	 ( (local_options & MatchNoCase) && g_toupper(ac) == g_toupper(kc) ) )
      (void)getch_marked(ps);
    else if ( ignore_whitespace && g_is_space(kc) &&
	      !( kc == '\n' && (local_options & MatchLine) ) &&
	      cis_prevch(arg) == G_EOF && ps->marked ) {
      (void)cis_getch(in);
      goto loop1;
    }
    else return FALSE;
  }
  return TRUE;
}

boolean
try_pattern( CIStream in, const G_CHAR* patstring, CIStream* next_arg,
	     CIStream* all_args, int options, Goal goal ) {
  const G_CHAR* ps;
  G_XCHAR_OR_EOF ic;
  int this_op, last_op;
  struct mark_struct marker;
  MarkBuf end_position;
  boolean end_position_marked;
  boolean match;
  int local_options;

  local_options = options;
  marker.marked = FALSE;
  marker.in = in;
  end_position_marked = FALSE;
  this_op = G_EOF;

  for ( ps = patstring ; ; ps++ ) {
    last_op = this_op;
    this_op = *ps;
    switch(this_op){
    case PT_END:
      goto success;
    case PT_MATCH_ANY: { /* "*" argument */
      COStream outbuf;
      int limit;
      Goal use_goal;
      if ( next_arg == NULL ) /* matching only up to first argument */
	goto success;
      assert( next_arg[0] == NULL );
      next_arg[1] = NULL;
      if ( ps[1] == PT_END ) {
        if ( goal == NULL && !cis_is_file(in) ) {
          /* Optimize special case where the argument just swallows all the
             rest of the input stream.  No need to impose a limit here. */
          *next_arg++ = copy_read_input_stream(in);
          TRACE_MATCH("*", next_arg[-1]);
          goto success;
        }
        use_goal = goal;
      }
      else
        use_goal = NULL;
      outbuf = make_buffer_output_stream();
      ic = 0; /* just to avoid gcc warning */
      for ( limit = MAX_ARG_LEN ; limit > 0 ; limit-- ) {
      	ic = cis_peek(in);
	if ( ic == G_EOF ) {
	  if ( ps[1] == PT_END || ps[1] == PT_AUX ) {
	    *next_arg++ = convert_output_to_input(outbuf);
	    TRACE_MATCH("*", next_arg[-1]);
	    goto continue_match;
	  }
	  else break;
	}
	if ( use_goal != NULL ) {
	  assert( ! (use_goal->options & MatchSwallow) );
	  if ( try_pattern( in, use_goal->template, NULL, use_goal->args,
			    (use_goal->options | MatchArgDelim), goal ) &&
	       use_goal->template[0] != PT_END ) {
	    next_arg[0] = convert_output_to_input(outbuf);
	    TRACE_MATCH("*", next_arg[0]);
	    goto success;
	  }
	}
	else
	if ( try_pattern( in, ps+1, next_arg+1, all_args,
			  (local_options | (MatchArgDelim|MatchSwallow)),
			  goal ) &&
	     /* (the simpler test is being done second only because it is
		 the much less common case.) */
	     ps[1] != PT_END ) {
	  next_arg[0] = convert_output_to_input(outbuf);
	  TRACE_MATCH("*", next_arg[0]);
	  goto success;
	}
	if ( ic == '\n' && (local_options & MatchLine) ) {
	  TRACE_FAILURE( "*" );
	  goto failed_any;
	}
	else {
	  G_XCHAR_OR_EOF xc;
	  xc = getch_marked(&marker);
#ifndef NDEBUG
	  if ( xc != ic )
	    input_error(in, EXS_INPUT, __FILE__
	        " line %d: ic = '%c', xc = '%c'\n",
	         __LINE__, ic, xc );
#endif

#if (!defined(MSDOS)) && ('\r' != '\n')
	  /* on Unix, skip the CR in MSDOS CR,LF sequence */
	  if ( !(xc == '\r' && cis_peek_short(in) == '\n') )
#endif
	  cos_putch(outbuf, xc );
	}
      } /* end for limit */
#ifdef TRACE
      if ( trace_switch ) {
        if ( ic == G_EOF )
          trace(&marker, FAIL, "*");
        else trace(&marker, FAIL, "* at limit of arglen=%d", MAX_ARG_LEN);
      }
#endif
    failed_any:
      cos_close(outbuf);
      goto failure;
    } /* end PT_MATCH_ANY */

    case PT_MATCH_ONE: {  /* "?" argument (general case) */
      G_CHAR str[4];
      if ( next_arg == NULL ) /* matching only up to first argument */
	goto success;
      ic = getch_marked(&marker);
      if ( ic == G_EOF || ( ic == '\n' && (local_options & MatchLine) ) )
	goto failure;
      str[0] = ic;
      str[1] = '\0';
#if G_CHAR_BYTES == 2
      if ( ic > 0xFFFF ) {
        str[0] = HISUR(ic);
        str[1] = LOSUR(ic);
        str[2] = '\0';
      }
#endif
      *next_arg++ = make_string_input_stream(str, 1, TRUE);
      *next_arg = NULL;
      TRACE_MATCH("?", next_arg[-1]);
      break;
    } /* end PT_MATCH_ONE */

    case PT_ONE_OPT: {  /* "?" argument (optimized special case) */
      assert( next_arg != NULL );
      arg_char = cis_getch(in);
      if ( arg_char == G_EOF )
        goto failure;
#ifdef TRACE
      if ( trace_switch ) {
        char buf[SHOW_BUF_SIZE];
        trace(&marker, OK, "Matched ? as %s\n", show_char(arg_char, buf));
      }
#endif
      break;
    } /* end PT_ONE_OPT */

    case PT_RECUR: { /* argument recursively translated */
      COStream outbuf;
      int domain;
      struct goal_state goal_info;
      Goal new_goal;
      if ( next_arg == NULL ) /* matching only up to first argument */
	goto success;
      assert( next_arg[0] == NULL );
      outbuf = make_buffer_output_stream();
#if SINGLE_CHAR_DOMAIN_NUM
      domain = *++ps - 1;
#else
      /* Get domain index as 14 bit little endian number */
      domain = ((unsigned char)*++ps)&0x7f;
      domain = ((((unsigned char)*++ps)&0x7f)<<7) | domain;
#endif
      if ( !marker.marked ) {
	cis_mark(in,&marker.start);
	marker.marked = TRUE;
      }
#ifdef TRACE
      if ( trace_switch ) {
	++trace_indent;
	if ( trace_indent < trace_enter.level ) {
	  trace_enter.level = trace_indent;
	  trace_enter.line = cis_line(in);
	  trace_enter.column = cis_column(in);
	  trace_enter.ch = cis_peek(in);
	  trace_enter.domain = domain;
	}
      }
#endif
      if ( ps[1] == PT_END )
	new_goal = goal;
      else {
	goal_info.template = ps+1;
	goal_info.args = all_args;
	goal_info.options = local_options & ~ MatchSwallow;
	new_goal = &goal_info;
      }
      if ( translate ( in, domains[domain], outbuf, new_goal ) ) {
	*next_arg++ = convert_output_to_input( outbuf );
	*next_arg = NULL;
#ifdef TRACE
	if ( trace_switch ) {
	  char tracebuf[250];
	  trace( &marker, OK, "Matched <%s> as \"%s\"\n",
		 message_string(domains[domain]->name),
		 show_arg_string(next_arg[-1], tracebuf) );
	  --trace_indent;
	}
#endif
      }
      else {
#ifdef TRACE
	if ( trace_switch ) {
	  trace( &marker, FAIL, "<%s>", message_string(domains[domain]->name) );
	  --trace_indent;
	}
#endif
	cos_close(outbuf);
	goto failure;
      }
      break;
    } /* end PT_RECUR */

    case PT_SPECIAL_ARG: {
      COStream outbuf;
      int kind;
      int parms;
      int num_wanted;
      int num_found;
      boolean optional;
      boolean inverse;
      unsigned goal_char, alt_goal_char;
      struct goal_state terminator_data;
      Goal terminator;
      static int warned_kind = 0;

      num_found = 0;
      parms = *++ps;
      optional = parms & 0x40;
      inverse = parms & 0x80;
      kind = ('A'-1) + (parms & 0x3F);
      num_wanted = (*++ps) - 1;
      goal_char = get_goal_char(ps+1);
      if ( goal_char == ENDOP && goal != NULL ) {
	goal_char = get_goal_char(goal->template);
	terminator = goal;
      }
      else {
	terminator_data.template = ps+1;
	terminator_data.args = all_args;
	terminator_data.options = local_options;
	terminator = &terminator_data;
      }
      if ( num_wanted >= 0xFE )
	num_wanted = -1;
      else if ( !optional )
	goal_char = ENDOP;
      alt_goal_char = goal_char;
      if ( (terminator->options & MatchNoCase) && !ISOP(goal_char) ) {
	goal_char = g_tolower(goal_char);
	alt_goal_char = g_toupper(goal_char);
      }
      outbuf = next_arg != NULL ? make_buffer_output_stream() : NULL;
      for ( ; ; ) {
	boolean ok = FALSE;
	boolean user_defined = FALSE;
	ic = cis_peek(in);
	if ( ic != G_EOF ) {
#if G_CHAR_BYTES > 1
	  if ( ic > max_ctype_char ) {
	    /* beyond the meaningful range of the is... functions */
            SetOfChar cset = NULL;
            switch(kind) {
            case 'U': ok = TRUE; break;
            case 'G': cset = charset_for_letter('V'); break;
            case 'A':
            case 'W':
            case 'L':
              if ( char_is_in_set(charset_for_letter('J'), ic) )
                ok = TRUE;
              else 
                cset = charset_for_letter('K');
              break;
            default:
              user_defined = (strchr("BEHMQRZ", kind)!=0);
              break;
            } /* end switch kind */
            if ( !ok ) { /* not yet set; derive from cset */
              ok = char_is_in_set(cset, ic);
            }
        } /* end ic > max_ctype_char */
        else
#endif
	switch(kind) {
	case 'O': if ( ic > '7' ) {			/* octal digits */
		    ok = FALSE;
		    break;
		}
	  /* else fall-through */
	case 'D': ok = G_IS(digit)(ic); break;		/* digits */
	case 'W': if ( num_found > 0 &&			/* word */
		       ( ic == '\'' || ic == '-' ) ) {
		    ok = TRUE;
		    break;
		}
	  /* else fall-through */
	case 'L': ok = G_IS(alpha)(ic)  		/* letters */
		    || char_is_in_set(charset_for_letter('J'), ic)
		    || char_is_in_set(charset_for_letter('K'), ic);
		  break;
	case 'J': ok = G_IS(lower)(ic);			/* lower case */
		  break;
	case 'K': ok = G_IS(upper)(ic);			/* upper case */
		  break;
	case 'A': ok = G_IS(alnum)(ic)  		/* alphanumerics */
		    || char_is_in_set(charset_for_letter('J'), ic)
		    || char_is_in_set(charset_for_letter('K'), ic);
		  break;
	case 'I': ok = isident(ic); break;		/* identifier */
	case 'G': ok = G_IS(graph)(ic); break;		/* graphic char */
	case 'C': ok = G_IS(cntrl)(ic); break;		/* control char */
	case 'F': ok = G_IS(alnum)(ic); break;		/* file pathname */
	case 'S': ok = G_IS(space)(ic); break;		/* white space */
	case 'X': ok = G_IS(xdigit)(ic); break;		/* hex digits */
	case 'N': ok = G_IS(digit)(ic) || 			/* number */
			( ic=='.' && cos_prevch(outbuf) != '.' ) ||
			( num_found == 0 && ( ic=='-' || ic=='+' ) );
	      if ( !ok && num_found == 1 ) {
		int prevch = cos_prevch(outbuf);
		if ( prevch < 'A' && strchr("+-.", prevch) != NULL ) {
		  cos_close(outbuf);
		  goto failure;
		}
	      }
		break;
	case 'Y': ok = G_IS(punct)(ic) &&			/* punctuation */
			 !( G_IS(space)(ic) | isident(ic) );
		break;
	case 'P': ok = G_IS(print)(ic); break;		/* printing char */
#if G_CHAR_BYTES == 1  /* else see vranges */
	case 'V': /* fall-through for same as <T> for version 1 compatibility */
#endif
	case 'T': ok = (G_IS(print)(ic) | G_IS(space)(ic));	/* valid text */
		break;
	case 'U': ok = TRUE;			/* anything (except EOF) */
		break;
	default: user_defined = TRUE; break;
        } /* end switch(kind) */
        if ( !ok ) {
          SetOfChar cset;
          assert(kind >= 'A' && kind <= 'Z');
          cset = charset_for_letter(kind);
          if ( cset != NULL )
            ok = char_is_in_set(cset, ic);
          else if ( user_defined ) {
            /* no user-defined set and
               no built-in programatic implementation */
            if ( kind != warned_kind ) {
              warned_kind = kind;
              fprintf(stderr, "Undefined arg type: <%c>\n", kind);
            }
          }
        }
	if ( inverse )
	  ok = !ok;
	} /* end ic != G_EOF */
	if(ok) {
	  if ( ic == '\n' && (local_options & MatchLine) )
	    break;
	  if ( next_arg == NULL ) /* matching only up to first argument */
	    goto success;
	  if ( ( ((unsigned)ic) == goal_char ||
		 ((unsigned)ic) == alt_goal_char ||
	         ( goal_char == UPOP(PT_SPACE) && g_is_space(ic) &&
		   ( optional || g_is_space(cos_prevch(outbuf)) ) ) ) &&
	       try_pattern( in, terminator->template, NULL, terminator->args,
			    (terminator->options & MatchNoCase), goal ) )
	    /* would be valid constituent except that it appears in the
	       template as a terminator. */
	    break;
	  num_found++;
	  if ( num_found > num_wanted && num_wanted >= 0 ) {
	    if ( num_wanted == 0 )
	      cos_putch(outbuf, ic);
	    break;
	  }
	  cos_putch(outbuf, getch_marked(&marker));
	} /* end ok */
	else if ( ignore_whitespace &&
		  g_is_space(ic) && num_found == 0 &&
		  !( ic == '\n' && (local_options & MatchLine) ) &&
		  ( kind=='Y' || kind=='C' || !isident(cis_prevch(in)) ) &&
		  marker.marked /* not beginning of template */ ) {
	  (void)getch_marked(&marker);
	  continue;
	}
	else break;
      } /* end for */
      if ( ( num_found < num_wanted ||  /* not enough characters found */
	     num_found == 0		/* no valid characters found */
	   ) && !optional )
       {
#ifdef TRACE
	if ( trace_switch )
	  trace( &marker, FAIL, "<%s%c>", (inverse? "-" : ""), kind );
#endif
	cos_close(outbuf);
      	goto failure;
      }
      else {
	if ( next_arg == NULL )
	  /* matching only up to first argument; here for empty optional arg */
	  goto success;
	else {
	  *next_arg++ = convert_output_to_input(outbuf);
	  *next_arg = NULL;
#ifdef TRACE
	  if ( trace_switch ) {
	    char trace_buf[250];
	    trace ( &marker, OK, "Matched <%s%c> as \"%s\"\n",
		    (inverse? "-" : ""), (optional? tolower(kind) : kind),
		    show_arg_string(next_arg[-1], trace_buf) );
	  }
#endif
	}
	break;
      }
    } /* end PT_SPECIAL_ARG */

    case PT_REGEXP: {
       CIStream value;
       int regex_num;
       if ( next_arg == NULL ) /* matching only up to first argument */
	 goto success;
       if ( !marker.marked ) {
         /* At beginning of template, remember restart point for failure. */
         cis_mark(in,&marker.start);
         marker.marked = TRUE;
       }
       regex_num = *++ps - 1;
       value = match_regexp ( in, regex_num );
       if ( value == NULL ) {
#ifdef TRACE
         if(trace_switch && !(local_options & MatchArgDelim))
           trace(&marker, FAIL, "/%s/",
                 message_string(regex_string(regex_num)));
#endif
	 goto failure;
       }
#ifdef TRACE
       if ( trace_switch ) {
         char tracebuf[250];
         trace(&marker, OK, "Matched /%s/ as \"%s\"\n",
               message_string(regex_string(regex_num)),
               show_arg_string(value, tracebuf));
       }
#endif
       *next_arg++ = value;
       *next_arg = NULL;
       break;
    }

    case PT_PUT_ARG: { /* match against value of previous argument */
	CIStream arg;
	if ( all_args == NULL ) /* matching only up to first argument */
	  goto success;
	arg = all_args[ (*++ps) - 1 ];
	if ( arg == NULL ) {
	  if ( next_arg == NULL )
	    goto success;
	  else goto failure;
	}
	cis_rewind(arg);
	if ( match_to_stream(&marker, arg, local_options) ) {
	  TRACE_MATCH("$n", arg);
	  break;
	}
	else {
	  TRACE_FAILURE("$n");
	  goto failure;
	}
    }

    case PT_VAR1: {
	G_SCHAR vname[4];
	const G_SCHAR* value;
	size_t length;
	CIStream arg;
	boolean ok;
	vname[0] = *++ps;
	vname[1] = '\0';
#if G_CHAR_BYTES == 2
	if ( IS_HI_SUR(vname[0]) && IS_LO_SUR(ps[1]) ) {
	  vname[1] = *++ps;
	  vname[2] = '\0';
	}
#endif
    	value = get_var(vname, FALSE, &length);
	arg = make_string_input_stream(value, length, FALSE);
	ok = match_to_stream(&marker, arg, local_options);
	cis_close(arg);
	if ( ok )
	  break;
	else goto failure;
      }

    case PT_SPACE: /* at least one space required */
      if ( !g_is_space( cis_peek(in) ) ) {
	if ( g_is_space(cis_prevch(in)) &&
	     ( marker.marked || next_arg == all_args ) )
	  break;
	else {
	  TRACE_FAILURE( "\\S" );
	  goto failure;
	}
      }
      /* and fall through for optional additional space */
    case PT_SKIP_WHITE_SPACE: {  /* optional white space */
      G_XCHAR_OR_EOF x;
      for ( ; ; ) {
        x = cis_peek(in);
        if ( !g_is_space(x) )
          break;
        if ( x == '\n' ) {
          G_CHAR nc;
          nc = ps[1]; /* next character in template */
          if ( nc == PT_AUX && ps[2] == PTX_POSITION ) /* peek past \P */
            nc = ps[3];
          if ( (local_options & MatchLine) ||
               nc == PT_LINE || nc == '\n' ||
               last_op == PT_LINE || last_op == '\n' )
            break;
        } /* end x == '\n' */
        (void)getch_marked(&marker);
      } /* end for */
      break;
    }
    case PT_WORD_DELIM: { /* word delimiter */
      if ( ! needs_word_delim(cis_prevch(in)) )
	break;
      if ( ! needs_word_delim(cis_peek(in)) )
	break;
      TRACE_FAILURE( "\\X" );
      goto failure;
    }
    case PT_ID_DELIM: { /* identifier delimiter */
      if ( !isident( cis_prevch(in) ) )
	break;
      if ( !isident(cis_peek(in)) )
	break;
      TRACE_FAILURE( "\\I" );
      goto failure;
    }
#if 0 	/* changed my mind */
    case PT_ARG_DELIM: { /* command line argument delimiter */
      while ( cis_peek(in) == Arg_Delim )
	(void)getch_marked(&marker);
      ic = cis_prevch(in);
      if ( ic == Arg_Delim || ic == G_EOF || cis_peek(in) == G_EOF )
	break;
      else goto failure;
    }
#endif
   case PT_LINE: { /* at beginning or end of line */
      int np;
#ifdef LUA
      if (fake_nl) {
        fake_nl='\0';	        
	break;
      }
#endif
      ic = cis_prevch(in);
      if ( ic == '\n' || ic == G_EOF )
	break;
      np = ps[1];
      if ( np != PT_SKIP_WHITE_SPACE && np != PT_SPACE &&
   	   np != PT_LINE && !g_is_space(np) ) {
	ic = cis_peek(in);
	if ( ic == G_EOF )
	  break;
	if ( ic == '\n' ) {
	  if ( !is_operator(np) ) /* accept newline if not end of template */
	    (void)getch_marked(&marker);
	  break;
	}
      }
      TRACE_FAILURE( "\\N" );
      goto failure;
    }

   case PT_AUX: {
     G_CHAR ec;
     ec = *++ps;
     switch(ec) {
#if 0	/* superseded by separate beginning and end operators */
       case PTX_MATCH_EOF: { /* at beginning or end of file */
	if ( cis_is_file(in) ) {
	  if ( cis_prevch(in) == G_EOF )
	    break;
	  if ( cis_peek(in) == G_EOF )
	    break;
	}
	goto failure;
      }
#endif
      case PTX_ONE_LINE:
	local_options |= MatchLine;
	break;
      case PTX_NO_CASE:
	local_options |= MatchNoCase;
	break;
      case PTX_BEGIN_FILE:
	if ( !cis_is_file(in) )
	  goto failure;
	/* else fall-through */
      case PTX_INIT: /* beginning of input data */
	/* when this is at the beginning of a template, we wouldn't have
	   gotten here unless it was already known to be true. */
	if ( ps-1 == patstring || cis_prevch(in) == G_EOF )
	  break;
	else goto failure;
      case PTX_END_FILE:
	if ( !cis_is_file(in) )
	  goto failure;
	/* else fall-through */
      case PTX_FINAL: /* end of input data */
	/* when this is at the beginning of a template, we wouldn't have
	   gotten here unless it was already known to be true. */
	if ( ps-1 == patstring || cis_peek(in) == G_EOF )
	  break;
	else if ( goal != NULL && ps[0] == PTX_FINAL &&
		  try_pattern( in, goal->template, NULL, goal->args,
			       (goal->options & ~MatchSwallow), NULL) )
	  break;
    	else goto failure;
      case PTX_POSITION: /* leave input stream here after match */
	if ( ps[1] != PT_END ) {
	  if ( !marker.marked ) {
	    cis_mark(in,&marker.start);
	    marker.marked = TRUE;
	  }
	  cis_mark(in, &end_position);
	  end_position_marked = TRUE;
	}
	break;
      case PTX_NO_GOAL:
	if ( !(local_options & MatchSwallow) ) {
	  /* when doing look-ahead for argument delimiter */
	  assert( next_arg == NULL );
	  if ( marker.marked ) /* some text matched, consider it sufficient. */
	    goto success;
	  else goto failure; /* don't terminate the argument yet */
	}
	break;
      case PTX_JOIN:
	if ( ignore_whitespace ) {
	  ic = cis_peek(in);
	  if ( g_is_space(ic) && ic != ps[1] &&
	       ( !is_operator(ps[1]) || ps[1] == PT_SPECIAL_ARG ) ) {
	    TRACE_FAILURE( "\\J" );
	    goto failure; /* prevent the space from being ignored */
	  }
	}
     	break;
     	
#ifdef LUA
      case PTX_MRK0:
      case PTX_MRK1:
      case PTX_MRK2:
      case PTX_MRK3:
      case PTX_MRK4:
      case PTX_MRK5:
      case PTX_MRK6:
      case PTX_MRK7:
      case PTX_MRK8:
      case PTX_MRK9:
        { char buf[80];
          int l,c;
          l=cis_line(in); c=cis_column(in);
          if ( cis_prevch(in) == '\n' ) {l++; c=0;}
          buf[0]='.'; buf[1]='l';
          buf[2]='0' + (ec - PTX_MRK0);
          buf[3]='\0';
       	  set_var( buf,buf+4,sprintf(buf+4,"%d",l));
          buf[1]='c';
       	  set_var( buf,buf+4,sprintf(buf+4,"%d",c));
        }
        break;
        
      case PTX_CND0:
      case PTX_CND1:
      case PTX_CND2:
      case PTX_CND3:
      case PTX_CND4:
      case PTX_CND5:
      case PTX_CND6:
      case PTX_CND7:
        /*fprintf(stderr,"COND: %d,%d,%d\n",ec,cond, (1<<(ec-PTX_NCND0)) & cond);*/
        if (!((1<<(ec-PTX_CND0)) & cond))  goto failure;
        break;

      case PTX_NCND0:
      case PTX_NCND1:
      case PTX_NCND2:
      case PTX_NCND3:
      case PTX_NCND4:
      case PTX_NCND5:
      case PTX_NCND6:
      case PTX_NCND7:
        /*fprintf(stderr,"NCND: %d,%d,%d\n",ec,cond, (1<<(ec-PTX_NCND0)) & cond);*/
        if (((1<<(ec-PTX_NCND0)) & cond))  goto failure;
        break;

#endif

#ifndef NDEBUG
      default:
	input_error( in, EXS_FAIL, "Undefined aux op in template: %d\n", ec);
	break;
#endif
     } /* end switch ec */
     break;
   } /* end PT_AUX */

    case PT_QUOTE: /* take next character literally */
      ++ps;
      /* and fall-through */
    default: {
      G_XCHAR_OR_EOF pc;
      pc = *ps;
#if G_CHAR_BYTES == 2
      if ( IS_HI_SUR(pc) && IS_LO_SUR(ps[1]) )
        pc = COMBINE_SUR(pc,*++ps);
#endif
again:
      ic = cis_peek(in);
      if ( ic != pc &&
	   ( !(local_options & MatchNoCase) ||
	     ( g_toupper(ic) != g_toupper(pc) ) ) ) {
	if ( (ignore_whitespace
#if (!defined(MSDOS)) && ('\r' != '\n')
	      || (ic == '\r' && pc == '\n') /* allow CR,LF as end of line */
#endif
	      ) && g_is_space(ic) &&
	     ( !isident(pc) || !isident(cis_prevch(in)) ) &&
	     marker.marked /* don't skip before beginning of template */ &&
	     !( ic == '\n' && (local_options & MatchLine) ) ) {
	  (void)getch_marked(&marker);
	  goto again;
	}
#ifdef TRACE
	if ( trace_switch &&
	     (local_options & (MatchArgDelim|MatchSwallow))==MatchSwallow ) {
	  char buf[SHOW_BUF_SIZE];
	  trace( &marker, FAIL, show_char(pc, buf) );
	}
#endif
	goto failure;
      } /* end if ic != pc ... */
      else (void)getch_marked(&marker);
#ifdef LUA
			/* This looks wrong -- DNG 6/1/22 */
	      *((G_CHAR*)ps)=ic;
#endif
      } /* end default */
    } /* end switch this_op */
  continue_match: ;
  } /* end for pattern string */
 failure:
  match = FALSE;
  goto quit;
 success:
  match = TRUE;
#ifdef LUA
    fake_nl='\0';
#endif
  if ( !marker.marked && ( options & MatchSwallow ) &&
       next_arg == all_args &&
       translation_status == Translate_Complete && 
       patstring[0] != PT_ONE_OPT && patstring[0] != PT_MATCH_ANY )
    /* matched without advancing the input stream */
    translation_status = Translate_Continue;
 quit:
  if ( marker.marked ) {
    if ( match && ( options & MatchSwallow ) ) {
      if ( end_position_marked ) {
	if ( end_position.position == marker.start.position &&
	     next_arg == all_args &&
	     translation_status == Translate_Complete )
	  /* matched without advancing the input stream */
	  translation_status = Translate_Continue;
	cis_restore(in,&end_position);
      }
      /* else leave the input stream at the end of the matched text */
      cis_release(in,&marker.start);
    }
    else {
      if ( end_position_marked )
	cis_release(in,&end_position);
      cis_restore(in,&marker.start); /* restore input to previous position */
    }
  }
  return match;
}

static int global_options;
Pattern current_rule = NULL;

/* does the domain have a default rule "=@fail" or "=@terminate"? */
static boolean
default_fail( Domain domainpt )
{
  Pattern tailpat;
  tailpat = domainpt->patterns.tail;
  if ( tailpat == NULL ) {
    return FALSE;
  }
  else if ( tailpat->pattern[0] == PT_END ) { /* empty template */
    return tailpat->action[0] == PT_OP &&
      (tailpat->action[1] == OP_FAIL || tailpat->action[1] == OP_END_OR_FAIL);
  }
  else return FALSE;
}

static boolean
try_match( CIStream in, Pattern pat, COStream out, Goal goal )
{
  boolean result;
  varp varmark;
  CIStream args[MAX_ARG_NUM+1];

  args[0] = NULL;
  varmark = first_bound_var;
  if ( ! try_pattern( in, pat->pattern, &args[0], &args[0],
		      global_options, goal ) ) {
    if ( varmark != first_bound_var )
      prune_vars(varmark); /* undo variables bound within failed match */
    result = FALSE;
  }
  else {
    const G_CHAR* as;
    enum Translation_Status save = translation_status;
    translation_status = Translate_Complete;
    /* pattern matches, so perform the specified action. */
    current_rule = pat;

    as = do_action( pat->action, args, out );
    assert( pat->action == NULL || *as == PT_END );
    (void)as; /* RD: suppress warning */

    result = TRUE;
    if ( translation_status <= Translate_Continue )
      translation_status = save;
  }
  {
    CIStream * argp;
    for ( argp = &args[0] ; *argp != NULL ; argp++ )
      cis_close(*argp); /* de-allocate argument buffer */
  }
  return result;
}

static boolean
try_list( CIStream in, Patterns p, COStream out, Goal goal ) {
  Pattern pat;
  boolean result = FALSE;
  for ( pat = p->head ; pat != NULL ; pat = pat->next ) {
    if ( translation_status > Translate_Continue )
      return translation_status != Translate_Failed;
    if ( try_match( in, pat, out, goal ) ) {
      if ( translation_status != Translate_Continue )
	return TRUE;
      result = TRUE;
    }
  }
  return result;
}

static boolean
try_patterns( int ch, CIStream in, MarkBuf* start, Patterns p, COStream out,
	      Goal goal ) {
    MarkBuf mark;
    if ( p->dispatch != NULL ) {
      Patterns sub;
      sub = p->dispatch[ dispatch_index(ch) ];
      if ( sub != NULL ) {
	if ( sub->dispatch != NULL ) {
	  G_XCHAR_OR_EOF xc;
	  if ( start == NULL ) {
	    start = &mark;
	    cis_mark(in, start);
	  }
	  xc = cis_getch(in);
	  assert ( ch == xc );
    (void)xc; /* RD: suppress warning */
	  if ( try_patterns( cis_peek(in), in, start, sub, out, goal ) )
	    return TRUE;
	  else start = NULL;
	}
	else {
	  if ( start != NULL ) {
	    cis_restore(in, start);
	    start = NULL;
	  }
	  if ( try_list( in, sub, out, goal ) ) {
	    if ( translation_status == Translate_Continue )
	      translation_status = Translate_Complete;
	    else
	      return TRUE;
	  }
	}
      }
    } /* end p->dispatch */
    if ( start != NULL ) {
      assert ( start != &mark );
      cis_restore(in, start);
    }
    if ( try_list( in, p, out, goal ) ) {
      if ( translation_status == Translate_Continue )
	translation_status = Translate_Complete;
      else
	return TRUE;
    }
    return FALSE;
}

static int domains_checked = 1;

boolean translate ( CIStream in, Domain domainpt, COStream out,
		    Goal goal_info ) {
  G_XCHAR_OR_EOF ch, ch2;
  const G_CHAR* goal;
  unsigned goal_char, alt_goal_char;
  enum Translation_Status save_fail;
  CIStream save_input;
  boolean no_match = TRUE;
  boolean discard = FALSE;
  boolean beginning = TRUE;
  boolean local_line_mode = FALSE;

  save_input = input_stream;
  if ( save_input == NULL || cis_pathname(in) != NULL ||
       ( cis_is_file(in) && ! cis_is_file(save_input) ) )
    input_stream = in;
  for ( ; domains_checked < ndomains ; domains_checked++ ) {
    Domain dp = domains[domains_checked];
    if ( dp->patterns.head == NULL && dp->patterns.dispatch == NULL &&
  	 dp->name[0] != '\0' && dp->init_and_final_patterns == NULL )
      fprintf(stderr, "Domain name \"%s\" is referenced but not defined.\n",
		message_string(dp->name));
  }
  global_options = MatchSwallow;
  if ( line_mode )
    global_options |= MatchLine;
  if ( case_insensitive )
    global_options |= MatchNoCase;
  if ( goal_info == NULL ) {
    goal = NULL;
    goal_char = ENDOP;
    alt_goal_char = ENDOP; /* just to avoid warning from Gnu compiler */
  }
  else {
    goal = goal_info->template;
    goal_char = get_goal_char(goal);
    alt_goal_char = goal_char;
    if ( (goal_info->options & MatchNoCase) && !ISOP(goal_char) ) {
      goal_char = g_tolower(goal_char);
      alt_goal_char = g_toupper(goal_char);
    }
    if ( goal_info->options & MatchLine )
      local_line_mode = TRUE;
  }
  save_fail = translation_status;
  translation_status = Translate_Complete;
  if ( discard_unmatched && domainpt->name[0] == '\0' )
    discard = TRUE;
  {	/* do any initialization actions */
  Pattern pat;
  for ( pat = domainpt->init_and_final_patterns ; pat != NULL ; pat = pat->next ) {
    const G_CHAR* ps = pat->pattern;
    assert( ps[0] == PT_AUX );
    if ( ps[1] == PTX_INIT ||
	 ( ps[1] == PTX_BEGIN_FILE && cis_prevch(in) == G_EOF ) )
      if ( try_match( in, pat, out, goal_info ) ) {
	no_match = FALSE;
	if ( translation_status == Translate_Continue )
	  translation_status = Translate_Complete;
	else if ( translation_status == Translate_Complete )
	  break;
	else {
	  boolean result = translation_status != Translate_Failed;
	  translation_status = save_fail;
	  input_stream = save_input;
	  return result;
	}
      }
  }
  }

  for ( ; translation_status == Translate_Complete ; ) {
    Domain idp;
    ch = cis_peek(in);
    if ( ch == G_EOF ) {
      /* For a domain whose default rule is ``=@fail'',
	 an argument should not match an empty string just
	 because the end of file is found before starting. */
      if( !( beginning && default_fail(domainpt) ) ) {
	break;  /* done */
      }
    }
    else if ( goal_char != ENDOP ) {
      if ( ((unsigned)ch) == goal_char || ((unsigned)ch) == alt_goal_char ||
    	   (goal_char == UPOP(PT_SPACE) && g_is_space(ch) &&
	    !( beginning && ignore_whitespace ) ) ) {
	if ( ( goal_char == goal[0] &&
	       (goal[1] == PT_END || goal[1] == PT_MATCH_ANY ||
		goal[1] == PT_MATCH_ONE || goal[1] == PT_RECUR ) )
	     /* short-cut for single-character delimiter */
	     ) {
	  /* For a domain whose default rule is ``=@fail'',
	     an argument should not match an empty string just
	     because the terminator is found before starting. */
          if( !( beginning && default_fail(domainpt) ) ) {
            if ( domainpt->patterns.dispatch != NULL ) {
              Patterns sub;
              sub = domainpt->patterns.dispatch[ dispatch_index(ch) ];
              if ( sub != NULL ) {
                Pattern pat2;
                for ( pat2 = sub->head ; pat2 != NULL ; pat2 = pat2->next ) {
                  if ( translation_status > Translate_Continue )
                    break;
                  if ( is_operator(pat2->pattern[1]) &&
                       (pat2->pattern[1] != PT_QUOTE) )
                    break;
                  /* else found a longer literal string */
                  if ( compare_specificity(pat2->pattern, goal) <= 0 )
                    break;
                  /* else found a rule more specific than goal */
                  if ( try_match( in, pat2, out, goal_info ) ) {
                    /* found match on a domain rule more specific than goal */
                  if ( translation_status != Translate_Complete ) {
                    boolean result = translation_status != Translate_Failed;
                    translation_status = save_fail;
                    input_stream = save_input;
                    return result;
                  }
                  else goto next_char;
                  } /* end if try_match */
                } /* end for pat2 */
              } /* if sub */
            } /* end if dispatch != NULL */
            break;
          } /* end not default fail */
        } /* end matched single character goal */
        else if (   /* use general pattern matching for the goal */
	     try_pattern( in, goal, NULL, goal_info->args,
			  goal_info->options, NULL ) ) {
	    /* For a domain whose default rule is ``=@fail'',
	       an argument should not match an empty string just
	       because the terminator is found before starting. */
	  if( !( beginning && default_fail(domainpt) ) )
	    break;
	}
      }
    } /* end goal_char != ENDOP */
    beginning = FALSE;
    for ( idp = domainpt ; idp != NULL ; idp = idp->inherits )
      if ( try_patterns( ch, in, NULL, &idp->patterns, out, goal_info ) ) {
	/* match found */
	if ( translation_status != Translate_Complete ) {
	  boolean result = translation_status != Translate_Failed;
	  translation_status = save_fail;
	  input_stream = save_input;
	  return result;
	}
	else goto next_char;
      }
    if ( (ch == '\n') && local_line_mode ) {
      /* For recursive argument with -line option, fail at end of line. */
      translation_status = save_fail;
      input_stream = save_input;
      return FALSE;
    }
    ch2 = cis_getch(in);
#ifndef NDEBUG
    if( ch2 != ch )
      input_error(in, EXS_INPUT, __FILE__
      " line %d : ch2 = '%c', ch = '%c'\n", __LINE__, ch2, ch);
#endif
    if ( !discard )
      cos_putch(out, ch2);
next_char: ;
  }  /* end for */

  {		/* do any finalization actions */
  Pattern pat;
  for ( pat = domainpt->init_and_final_patterns ; pat != NULL ; pat = pat->next ) {
    const G_CHAR* ps = pat->pattern;
    if ( ps[1] == PTX_FINAL ||
	 ( ps[1] == PTX_END_FILE && cis_peek(in) == G_EOF) )
      if ( try_match( in, pat, out, goal_info ) )
	no_match = FALSE;
  }
  if ( no_match && cis_prevch(in) == G_EOF && cis_peek(in) == G_EOF ) {
    for ( pat = domainpt->patterns.head ; pat != NULL ; pat = pat->next ) {
      if ( pat->pattern[0] == PT_END ) {
	/* empty template matches empty input stream */
	current_rule = pat;
	do_action( pat->action, NULL, out );
	break;
      }
    }
  }
  }
  translation_status = save_fail;
  input_stream = save_input;
  return TRUE;  /* indicate success */
}

G_XCHAR g_toupper(G_XCHAR ch)
{
  G_XCHAR_OR_EOF upch;
  SetOfChar from_set, to_set;
#if G_CHAR_BYTES == 2
  if ( ch <= WCHAR_MAX )
#endif
    {
      upch = G_TOUPPER(ch);
      if ( upch != ch )
        return upch;
    }
  from_set = charset_for_letter('J');
  if ( from_set == NULL )
    return ch;
  to_set   = charset_for_letter('K');
  upch = char_to_other_set(ch, from_set, to_set);
  if ( upch != G_NULL_CHAR )
    return upch;
  else return ch;
}

G_XCHAR g_tolower(G_XCHAR ch)
{
  G_XCHAR_OR_EOF downch;
  SetOfChar from_set, to_set;
#if G_CHAR_BYTES == 2
  if ( ch <= WCHAR_MAX )
#endif
    {
      downch = G_TOLOWER(ch);
      if ( downch != ch )
        return downch;
    }
  from_set = charset_for_letter('K');
  if ( from_set == NULL )
    return ch;
  to_set   = charset_for_letter('J');
  downch = char_to_other_set(ch, from_set, to_set);
  if ( downch != G_NULL_CHAR )
    return downch;
  else return ch;
}
